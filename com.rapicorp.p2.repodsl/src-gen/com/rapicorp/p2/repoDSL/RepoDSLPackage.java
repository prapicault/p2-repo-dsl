/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.rapicorp.p2.repoDSL.RepoDSLFactory
 * @model kind="package"
 * @generated
 */
public interface RepoDSLPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "repoDSL";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.rapicorp.com/p2/RepoDSL";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "repoDSL";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  RepoDSLPackage eINSTANCE = com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl.init();

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.ModelImpl <em>Model</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.ModelImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getModel()
   * @generated
   */
  int MODEL = 0;

  /**
   * The feature id for the '<em><b>Repo Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__REPO_NAME = 0;

  /**
   * The feature id for the '<em><b>Associated Repos</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__ASSOCIATED_REPOS = 1;

  /**
   * The feature id for the '<em><b>Categories</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__CATEGORIES = 2;

  /**
   * The feature id for the '<em><b>Non Categorized</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL__NON_CATEGORIZED = 3;

  /**
   * The number of structural features of the '<em>Model</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int MODEL_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.RepoNameImpl <em>Repo Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.RepoNameImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getRepoName()
   * @generated
   */
  int REPO_NAME = 1;

  /**
   * The feature id for the '<em><b>User Readable Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPO_NAME__USER_READABLE_NAME = 0;

  /**
   * The number of structural features of the '<em>Repo Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REPO_NAME_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.ReferencedRepoImpl <em>Referenced Repo</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.ReferencedRepoImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getReferencedRepo()
   * @generated
   */
  int REFERENCED_REPO = 2;

  /**
   * The feature id for the '<em><b>Associated Repo URL</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_REPO__ASSOCIATED_REPO_URL = 0;

  /**
   * The feature id for the '<em><b>Nick Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_REPO__NICK_NAME = 1;

  /**
   * The feature id for the '<em><b>Enablement</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_REPO__ENABLEMENT = 2;

  /**
   * The feature id for the '<em><b>Visibility</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_REPO__VISIBILITY = 3;

  /**
   * The number of structural features of the '<em>Referenced Repo</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int REFERENCED_REPO_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.UncategorizedContentImpl <em>Uncategorized Content</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.UncategorizedContentImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getUncategorizedContent()
   * @generated
   */
  int UNCATEGORIZED_CONTENT = 3;

  /**
   * The feature id for the '<em><b>Entries</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNCATEGORIZED_CONTENT__ENTRIES = 0;

  /**
   * The number of structural features of the '<em>Uncategorized Content</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UNCATEGORIZED_CONTENT_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.CategoryDefImpl <em>Category Def</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.CategoryDefImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getCategoryDef()
   * @generated
   */
  int CATEGORY_DEF = 4;

  /**
   * The number of structural features of the '<em>Category Def</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY_DEF_FEATURE_COUNT = 0;

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.IuImpl <em>Iu</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.IuImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getIu()
   * @generated
   */
  int IU = 5;

  /**
   * The feature id for the '<em><b>Iu Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IU__IU_ID = 0;

  /**
   * The feature id for the '<em><b>Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IU__RANGE = 1;

  /**
   * The number of structural features of the '<em>Iu</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IU_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.VersionRangeImpl <em>Version Range</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.VersionRangeImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getVersionRange()
   * @generated
   */
  int VERSION_RANGE = 6;

  /**
   * The feature id for the '<em><b>Lower Range</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VERSION_RANGE__LOWER_RANGE = 0;

  /**
   * The feature id for the '<em><b>Upper Range</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VERSION_RANGE__UPPER_RANGE = 1;

  /**
   * The number of structural features of the '<em>Version Range</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int VERSION_RANGE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.FeatureImpl <em>Feature</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.FeatureImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getFeature()
   * @generated
   */
  int FEATURE = 7;

  /**
   * The feature id for the '<em><b>Feature Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FEATURE__FEATURE_ID = 0;

  /**
   * The number of structural features of the '<em>Feature</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FEATURE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link com.rapicorp.p2.repoDSL.impl.CategoryNameImpl <em>Category Name</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see com.rapicorp.p2.repoDSL.impl.CategoryNameImpl
   * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getCategoryName()
   * @generated
   */
  int CATEGORY_NAME = 8;

  /**
   * The feature id for the '<em><b>Entries</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY_NAME__ENTRIES = CATEGORY_DEF_FEATURE_COUNT + 0;

  /**
   * The feature id for the '<em><b>Category Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY_NAME__CATEGORY_NAME = CATEGORY_DEF_FEATURE_COUNT + 1;

  /**
   * The number of structural features of the '<em>Category Name</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CATEGORY_NAME_FEATURE_COUNT = CATEGORY_DEF_FEATURE_COUNT + 2;


  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.Model <em>Model</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Model</em>'.
   * @see com.rapicorp.p2.repoDSL.Model
   * @generated
   */
  EClass getModel();

  /**
   * Returns the meta object for the containment reference '{@link com.rapicorp.p2.repoDSL.Model#getRepoName <em>Repo Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Repo Name</em>'.
   * @see com.rapicorp.p2.repoDSL.Model#getRepoName()
   * @see #getModel()
   * @generated
   */
  EReference getModel_RepoName();

  /**
   * Returns the meta object for the containment reference list '{@link com.rapicorp.p2.repoDSL.Model#getAssociatedRepos <em>Associated Repos</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Associated Repos</em>'.
   * @see com.rapicorp.p2.repoDSL.Model#getAssociatedRepos()
   * @see #getModel()
   * @generated
   */
  EReference getModel_AssociatedRepos();

  /**
   * Returns the meta object for the containment reference list '{@link com.rapicorp.p2.repoDSL.Model#getCategories <em>Categories</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Categories</em>'.
   * @see com.rapicorp.p2.repoDSL.Model#getCategories()
   * @see #getModel()
   * @generated
   */
  EReference getModel_Categories();

  /**
   * Returns the meta object for the containment reference '{@link com.rapicorp.p2.repoDSL.Model#getNonCategorized <em>Non Categorized</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Non Categorized</em>'.
   * @see com.rapicorp.p2.repoDSL.Model#getNonCategorized()
   * @see #getModel()
   * @generated
   */
  EReference getModel_NonCategorized();

  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.RepoName <em>Repo Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Repo Name</em>'.
   * @see com.rapicorp.p2.repoDSL.RepoName
   * @generated
   */
  EClass getRepoName();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.RepoName#getUserReadableName <em>User Readable Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>User Readable Name</em>'.
   * @see com.rapicorp.p2.repoDSL.RepoName#getUserReadableName()
   * @see #getRepoName()
   * @generated
   */
  EAttribute getRepoName_UserReadableName();

  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.ReferencedRepo <em>Referenced Repo</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Referenced Repo</em>'.
   * @see com.rapicorp.p2.repoDSL.ReferencedRepo
   * @generated
   */
  EClass getReferencedRepo();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getAssociatedRepoURL <em>Associated Repo URL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Associated Repo URL</em>'.
   * @see com.rapicorp.p2.repoDSL.ReferencedRepo#getAssociatedRepoURL()
   * @see #getReferencedRepo()
   * @generated
   */
  EAttribute getReferencedRepo_AssociatedRepoURL();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getNickName <em>Nick Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Nick Name</em>'.
   * @see com.rapicorp.p2.repoDSL.ReferencedRepo#getNickName()
   * @see #getReferencedRepo()
   * @generated
   */
  EAttribute getReferencedRepo_NickName();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getEnablement <em>Enablement</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Enablement</em>'.
   * @see com.rapicorp.p2.repoDSL.ReferencedRepo#getEnablement()
   * @see #getReferencedRepo()
   * @generated
   */
  EAttribute getReferencedRepo_Enablement();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getVisibility <em>Visibility</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Visibility</em>'.
   * @see com.rapicorp.p2.repoDSL.ReferencedRepo#getVisibility()
   * @see #getReferencedRepo()
   * @generated
   */
  EAttribute getReferencedRepo_Visibility();

  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.UncategorizedContent <em>Uncategorized Content</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Uncategorized Content</em>'.
   * @see com.rapicorp.p2.repoDSL.UncategorizedContent
   * @generated
   */
  EClass getUncategorizedContent();

  /**
   * Returns the meta object for the containment reference list '{@link com.rapicorp.p2.repoDSL.UncategorizedContent#getEntries <em>Entries</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Entries</em>'.
   * @see com.rapicorp.p2.repoDSL.UncategorizedContent#getEntries()
   * @see #getUncategorizedContent()
   * @generated
   */
  EReference getUncategorizedContent_Entries();

  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.CategoryDef <em>Category Def</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Category Def</em>'.
   * @see com.rapicorp.p2.repoDSL.CategoryDef
   * @generated
   */
  EClass getCategoryDef();

  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.Iu <em>Iu</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Iu</em>'.
   * @see com.rapicorp.p2.repoDSL.Iu
   * @generated
   */
  EClass getIu();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.Iu#getIuId <em>Iu Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Iu Id</em>'.
   * @see com.rapicorp.p2.repoDSL.Iu#getIuId()
   * @see #getIu()
   * @generated
   */
  EAttribute getIu_IuId();

  /**
   * Returns the meta object for the containment reference '{@link com.rapicorp.p2.repoDSL.Iu#getRange <em>Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Range</em>'.
   * @see com.rapicorp.p2.repoDSL.Iu#getRange()
   * @see #getIu()
   * @generated
   */
  EReference getIu_Range();

  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.VersionRange <em>Version Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Version Range</em>'.
   * @see com.rapicorp.p2.repoDSL.VersionRange
   * @generated
   */
  EClass getVersionRange();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.VersionRange#getLowerRange <em>Lower Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Lower Range</em>'.
   * @see com.rapicorp.p2.repoDSL.VersionRange#getLowerRange()
   * @see #getVersionRange()
   * @generated
   */
  EAttribute getVersionRange_LowerRange();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.VersionRange#getUpperRange <em>Upper Range</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Upper Range</em>'.
   * @see com.rapicorp.p2.repoDSL.VersionRange#getUpperRange()
   * @see #getVersionRange()
   * @generated
   */
  EAttribute getVersionRange_UpperRange();

  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.Feature <em>Feature</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Feature</em>'.
   * @see com.rapicorp.p2.repoDSL.Feature
   * @generated
   */
  EClass getFeature();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.Feature#getFeatureId <em>Feature Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Feature Id</em>'.
   * @see com.rapicorp.p2.repoDSL.Feature#getFeatureId()
   * @see #getFeature()
   * @generated
   */
  EAttribute getFeature_FeatureId();

  /**
   * Returns the meta object for class '{@link com.rapicorp.p2.repoDSL.CategoryName <em>Category Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Category Name</em>'.
   * @see com.rapicorp.p2.repoDSL.CategoryName
   * @generated
   */
  EClass getCategoryName();

  /**
   * Returns the meta object for the containment reference list '{@link com.rapicorp.p2.repoDSL.CategoryName#getEntries <em>Entries</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Entries</em>'.
   * @see com.rapicorp.p2.repoDSL.CategoryName#getEntries()
   * @see #getCategoryName()
   * @generated
   */
  EReference getCategoryName_Entries();

  /**
   * Returns the meta object for the attribute '{@link com.rapicorp.p2.repoDSL.CategoryName#getCategoryName <em>Category Name</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Category Name</em>'.
   * @see com.rapicorp.p2.repoDSL.CategoryName#getCategoryName()
   * @see #getCategoryName()
   * @generated
   */
  EAttribute getCategoryName_CategoryName();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  RepoDSLFactory getRepoDSLFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.ModelImpl <em>Model</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.ModelImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getModel()
     * @generated
     */
    EClass MODEL = eINSTANCE.getModel();

    /**
     * The meta object literal for the '<em><b>Repo Name</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__REPO_NAME = eINSTANCE.getModel_RepoName();

    /**
     * The meta object literal for the '<em><b>Associated Repos</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__ASSOCIATED_REPOS = eINSTANCE.getModel_AssociatedRepos();

    /**
     * The meta object literal for the '<em><b>Categories</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__CATEGORIES = eINSTANCE.getModel_Categories();

    /**
     * The meta object literal for the '<em><b>Non Categorized</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference MODEL__NON_CATEGORIZED = eINSTANCE.getModel_NonCategorized();

    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.RepoNameImpl <em>Repo Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.RepoNameImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getRepoName()
     * @generated
     */
    EClass REPO_NAME = eINSTANCE.getRepoName();

    /**
     * The meta object literal for the '<em><b>User Readable Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REPO_NAME__USER_READABLE_NAME = eINSTANCE.getRepoName_UserReadableName();

    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.ReferencedRepoImpl <em>Referenced Repo</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.ReferencedRepoImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getReferencedRepo()
     * @generated
     */
    EClass REFERENCED_REPO = eINSTANCE.getReferencedRepo();

    /**
     * The meta object literal for the '<em><b>Associated Repo URL</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REFERENCED_REPO__ASSOCIATED_REPO_URL = eINSTANCE.getReferencedRepo_AssociatedRepoURL();

    /**
     * The meta object literal for the '<em><b>Nick Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REFERENCED_REPO__NICK_NAME = eINSTANCE.getReferencedRepo_NickName();

    /**
     * The meta object literal for the '<em><b>Enablement</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REFERENCED_REPO__ENABLEMENT = eINSTANCE.getReferencedRepo_Enablement();

    /**
     * The meta object literal for the '<em><b>Visibility</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute REFERENCED_REPO__VISIBILITY = eINSTANCE.getReferencedRepo_Visibility();

    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.UncategorizedContentImpl <em>Uncategorized Content</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.UncategorizedContentImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getUncategorizedContent()
     * @generated
     */
    EClass UNCATEGORIZED_CONTENT = eINSTANCE.getUncategorizedContent();

    /**
     * The meta object literal for the '<em><b>Entries</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UNCATEGORIZED_CONTENT__ENTRIES = eINSTANCE.getUncategorizedContent_Entries();

    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.CategoryDefImpl <em>Category Def</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.CategoryDefImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getCategoryDef()
     * @generated
     */
    EClass CATEGORY_DEF = eINSTANCE.getCategoryDef();

    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.IuImpl <em>Iu</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.IuImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getIu()
     * @generated
     */
    EClass IU = eINSTANCE.getIu();

    /**
     * The meta object literal for the '<em><b>Iu Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IU__IU_ID = eINSTANCE.getIu_IuId();

    /**
     * The meta object literal for the '<em><b>Range</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference IU__RANGE = eINSTANCE.getIu_Range();

    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.VersionRangeImpl <em>Version Range</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.VersionRangeImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getVersionRange()
     * @generated
     */
    EClass VERSION_RANGE = eINSTANCE.getVersionRange();

    /**
     * The meta object literal for the '<em><b>Lower Range</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VERSION_RANGE__LOWER_RANGE = eINSTANCE.getVersionRange_LowerRange();

    /**
     * The meta object literal for the '<em><b>Upper Range</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute VERSION_RANGE__UPPER_RANGE = eINSTANCE.getVersionRange_UpperRange();

    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.FeatureImpl <em>Feature</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.FeatureImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getFeature()
     * @generated
     */
    EClass FEATURE = eINSTANCE.getFeature();

    /**
     * The meta object literal for the '<em><b>Feature Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FEATURE__FEATURE_ID = eINSTANCE.getFeature_FeatureId();

    /**
     * The meta object literal for the '{@link com.rapicorp.p2.repoDSL.impl.CategoryNameImpl <em>Category Name</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see com.rapicorp.p2.repoDSL.impl.CategoryNameImpl
     * @see com.rapicorp.p2.repoDSL.impl.RepoDSLPackageImpl#getCategoryName()
     * @generated
     */
    EClass CATEGORY_NAME = eINSTANCE.getCategoryName();

    /**
     * The meta object literal for the '<em><b>Entries</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CATEGORY_NAME__ENTRIES = eINSTANCE.getCategoryName_Entries();

    /**
     * The meta object literal for the '<em><b>Category Name</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CATEGORY_NAME__CATEGORY_NAME = eINSTANCE.getCategoryName_CategoryName();

  }

} //RepoDSLPackage
