/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Uncategorized Content</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.UncategorizedContent#getEntries <em>Entries</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getUncategorizedContent()
 * @model
 * @generated
 */
public interface UncategorizedContent extends EObject
{
  /**
   * Returns the value of the '<em><b>Entries</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Entries</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Entries</em>' containment reference list.
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getUncategorizedContent_Entries()
   * @model containment="true"
   * @generated
   */
  EList<EObject> getEntries();

} // UncategorizedContent
