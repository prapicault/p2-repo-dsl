/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Version Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.VersionRange#getLowerRange <em>Lower Range</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.VersionRange#getUpperRange <em>Upper Range</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getVersionRange()
 * @model
 * @generated
 */
public interface VersionRange extends EObject
{
  /**
   * Returns the value of the '<em><b>Lower Range</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Lower Range</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lower Range</em>' attribute.
   * @see #setLowerRange(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getVersionRange_LowerRange()
   * @model
   * @generated
   */
  String getLowerRange();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.VersionRange#getLowerRange <em>Lower Range</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lower Range</em>' attribute.
   * @see #getLowerRange()
   * @generated
   */
  void setLowerRange(String value);

  /**
   * Returns the value of the '<em><b>Upper Range</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Upper Range</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Upper Range</em>' attribute.
   * @see #setUpperRange(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getVersionRange_UpperRange()
   * @model
   * @generated
   */
  String getUpperRange();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.VersionRange#getUpperRange <em>Upper Range</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Upper Range</em>' attribute.
   * @see #getUpperRange()
   * @generated
   */
  void setUpperRange(String value);

} // VersionRange
