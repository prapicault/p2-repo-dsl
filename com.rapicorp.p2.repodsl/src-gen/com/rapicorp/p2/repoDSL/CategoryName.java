/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.CategoryName#getEntries <em>Entries</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.CategoryName#getCategoryName <em>Category Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getCategoryName()
 * @model
 * @generated
 */
public interface CategoryName extends CategoryDef
{
  /**
   * Returns the value of the '<em><b>Entries</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Entries</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Entries</em>' containment reference list.
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getCategoryName_Entries()
   * @model containment="true"
   * @generated
   */
  EList<EObject> getEntries();

  /**
   * Returns the value of the '<em><b>Category Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Category Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Category Name</em>' attribute.
   * @see #setCategoryName(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getCategoryName_CategoryName()
   * @model
   * @generated
   */
  String getCategoryName();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.CategoryName#getCategoryName <em>Category Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Category Name</em>' attribute.
   * @see #getCategoryName()
   * @generated
   */
  void setCategoryName(String value);

} // CategoryName
