/**
 */
package com.rapicorp.p2.repoDSL.impl;

import com.rapicorp.p2.repoDSL.RepoDSLPackage;
import com.rapicorp.p2.repoDSL.RepoName;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Repo Name</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.RepoNameImpl#getUserReadableName <em>User Readable Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RepoNameImpl extends MinimalEObjectImpl.Container implements RepoName
{
  /**
   * The default value of the '{@link #getUserReadableName() <em>User Readable Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUserReadableName()
   * @generated
   * @ordered
   */
  protected static final String USER_READABLE_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUserReadableName() <em>User Readable Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUserReadableName()
   * @generated
   * @ordered
   */
  protected String userReadableName = USER_READABLE_NAME_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected RepoNameImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return RepoDSLPackage.Literals.REPO_NAME;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUserReadableName()
  {
    return userReadableName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUserReadableName(String newUserReadableName)
  {
    String oldUserReadableName = userReadableName;
    userReadableName = newUserReadableName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.REPO_NAME__USER_READABLE_NAME, oldUserReadableName, userReadableName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case RepoDSLPackage.REPO_NAME__USER_READABLE_NAME:
        return getUserReadableName();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case RepoDSLPackage.REPO_NAME__USER_READABLE_NAME:
        setUserReadableName((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case RepoDSLPackage.REPO_NAME__USER_READABLE_NAME:
        setUserReadableName(USER_READABLE_NAME_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case RepoDSLPackage.REPO_NAME__USER_READABLE_NAME:
        return USER_READABLE_NAME_EDEFAULT == null ? userReadableName != null : !USER_READABLE_NAME_EDEFAULT.equals(userReadableName);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (userReadableName: ");
    result.append(userReadableName);
    result.append(')');
    return result.toString();
  }

} //RepoNameImpl
