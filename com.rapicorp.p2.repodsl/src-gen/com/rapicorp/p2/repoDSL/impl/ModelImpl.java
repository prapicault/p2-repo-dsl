/**
 */
package com.rapicorp.p2.repoDSL.impl;

import com.rapicorp.p2.repoDSL.CategoryDef;
import com.rapicorp.p2.repoDSL.Model;
import com.rapicorp.p2.repoDSL.ReferencedRepo;
import com.rapicorp.p2.repoDSL.RepoDSLPackage;
import com.rapicorp.p2.repoDSL.RepoName;
import com.rapicorp.p2.repoDSL.UncategorizedContent;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.ModelImpl#getRepoName <em>Repo Name</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.ModelImpl#getAssociatedRepos <em>Associated Repos</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.ModelImpl#getCategories <em>Categories</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.ModelImpl#getNonCategorized <em>Non Categorized</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model
{
  /**
   * The cached value of the '{@link #getRepoName() <em>Repo Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRepoName()
   * @generated
   * @ordered
   */
  protected RepoName repoName;

  /**
   * The cached value of the '{@link #getAssociatedRepos() <em>Associated Repos</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssociatedRepos()
   * @generated
   * @ordered
   */
  protected EList<ReferencedRepo> associatedRepos;

  /**
   * The cached value of the '{@link #getCategories() <em>Categories</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCategories()
   * @generated
   * @ordered
   */
  protected EList<CategoryDef> categories;

  /**
   * The cached value of the '{@link #getNonCategorized() <em>Non Categorized</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNonCategorized()
   * @generated
   * @ordered
   */
  protected UncategorizedContent nonCategorized;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return RepoDSLPackage.Literals.MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepoName getRepoName()
  {
    return repoName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetRepoName(RepoName newRepoName, NotificationChain msgs)
  {
    RepoName oldRepoName = repoName;
    repoName = newRepoName;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RepoDSLPackage.MODEL__REPO_NAME, oldRepoName, newRepoName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setRepoName(RepoName newRepoName)
  {
    if (newRepoName != repoName)
    {
      NotificationChain msgs = null;
      if (repoName != null)
        msgs = ((InternalEObject)repoName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RepoDSLPackage.MODEL__REPO_NAME, null, msgs);
      if (newRepoName != null)
        msgs = ((InternalEObject)newRepoName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RepoDSLPackage.MODEL__REPO_NAME, null, msgs);
      msgs = basicSetRepoName(newRepoName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.MODEL__REPO_NAME, newRepoName, newRepoName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<ReferencedRepo> getAssociatedRepos()
  {
    if (associatedRepos == null)
    {
      associatedRepos = new EObjectContainmentEList<ReferencedRepo>(ReferencedRepo.class, this, RepoDSLPackage.MODEL__ASSOCIATED_REPOS);
    }
    return associatedRepos;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EList<CategoryDef> getCategories()
  {
    if (categories == null)
    {
      categories = new EObjectContainmentEList<CategoryDef>(CategoryDef.class, this, RepoDSLPackage.MODEL__CATEGORIES);
    }
    return categories;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UncategorizedContent getNonCategorized()
  {
    return nonCategorized;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetNonCategorized(UncategorizedContent newNonCategorized, NotificationChain msgs)
  {
    UncategorizedContent oldNonCategorized = nonCategorized;
    nonCategorized = newNonCategorized;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, RepoDSLPackage.MODEL__NON_CATEGORIZED, oldNonCategorized, newNonCategorized);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNonCategorized(UncategorizedContent newNonCategorized)
  {
    if (newNonCategorized != nonCategorized)
    {
      NotificationChain msgs = null;
      if (nonCategorized != null)
        msgs = ((InternalEObject)nonCategorized).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - RepoDSLPackage.MODEL__NON_CATEGORIZED, null, msgs);
      if (newNonCategorized != null)
        msgs = ((InternalEObject)newNonCategorized).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - RepoDSLPackage.MODEL__NON_CATEGORIZED, null, msgs);
      msgs = basicSetNonCategorized(newNonCategorized, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.MODEL__NON_CATEGORIZED, newNonCategorized, newNonCategorized));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case RepoDSLPackage.MODEL__REPO_NAME:
        return basicSetRepoName(null, msgs);
      case RepoDSLPackage.MODEL__ASSOCIATED_REPOS:
        return ((InternalEList<?>)getAssociatedRepos()).basicRemove(otherEnd, msgs);
      case RepoDSLPackage.MODEL__CATEGORIES:
        return ((InternalEList<?>)getCategories()).basicRemove(otherEnd, msgs);
      case RepoDSLPackage.MODEL__NON_CATEGORIZED:
        return basicSetNonCategorized(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case RepoDSLPackage.MODEL__REPO_NAME:
        return getRepoName();
      case RepoDSLPackage.MODEL__ASSOCIATED_REPOS:
        return getAssociatedRepos();
      case RepoDSLPackage.MODEL__CATEGORIES:
        return getCategories();
      case RepoDSLPackage.MODEL__NON_CATEGORIZED:
        return getNonCategorized();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case RepoDSLPackage.MODEL__REPO_NAME:
        setRepoName((RepoName)newValue);
        return;
      case RepoDSLPackage.MODEL__ASSOCIATED_REPOS:
        getAssociatedRepos().clear();
        getAssociatedRepos().addAll((Collection<? extends ReferencedRepo>)newValue);
        return;
      case RepoDSLPackage.MODEL__CATEGORIES:
        getCategories().clear();
        getCategories().addAll((Collection<? extends CategoryDef>)newValue);
        return;
      case RepoDSLPackage.MODEL__NON_CATEGORIZED:
        setNonCategorized((UncategorizedContent)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case RepoDSLPackage.MODEL__REPO_NAME:
        setRepoName((RepoName)null);
        return;
      case RepoDSLPackage.MODEL__ASSOCIATED_REPOS:
        getAssociatedRepos().clear();
        return;
      case RepoDSLPackage.MODEL__CATEGORIES:
        getCategories().clear();
        return;
      case RepoDSLPackage.MODEL__NON_CATEGORIZED:
        setNonCategorized((UncategorizedContent)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case RepoDSLPackage.MODEL__REPO_NAME:
        return repoName != null;
      case RepoDSLPackage.MODEL__ASSOCIATED_REPOS:
        return associatedRepos != null && !associatedRepos.isEmpty();
      case RepoDSLPackage.MODEL__CATEGORIES:
        return categories != null && !categories.isEmpty();
      case RepoDSLPackage.MODEL__NON_CATEGORIZED:
        return nonCategorized != null;
    }
    return super.eIsSet(featureID);
  }

} //ModelImpl
