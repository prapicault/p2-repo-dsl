/**
 */
package com.rapicorp.p2.repoDSL.impl;

import com.rapicorp.p2.repoDSL.RepoDSLPackage;
import com.rapicorp.p2.repoDSL.VersionRange;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Version Range</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.VersionRangeImpl#getLowerRange <em>Lower Range</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.VersionRangeImpl#getUpperRange <em>Upper Range</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class VersionRangeImpl extends MinimalEObjectImpl.Container implements VersionRange
{
  /**
   * The default value of the '{@link #getLowerRange() <em>Lower Range</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLowerRange()
   * @generated
   * @ordered
   */
  protected static final String LOWER_RANGE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLowerRange() <em>Lower Range</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLowerRange()
   * @generated
   * @ordered
   */
  protected String lowerRange = LOWER_RANGE_EDEFAULT;

  /**
   * The default value of the '{@link #getUpperRange() <em>Upper Range</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUpperRange()
   * @generated
   * @ordered
   */
  protected static final String UPPER_RANGE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUpperRange() <em>Upper Range</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUpperRange()
   * @generated
   * @ordered
   */
  protected String upperRange = UPPER_RANGE_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected VersionRangeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return RepoDSLPackage.Literals.VERSION_RANGE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLowerRange()
  {
    return lowerRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLowerRange(String newLowerRange)
  {
    String oldLowerRange = lowerRange;
    lowerRange = newLowerRange;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.VERSION_RANGE__LOWER_RANGE, oldLowerRange, lowerRange));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUpperRange()
  {
    return upperRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUpperRange(String newUpperRange)
  {
    String oldUpperRange = upperRange;
    upperRange = newUpperRange;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.VERSION_RANGE__UPPER_RANGE, oldUpperRange, upperRange));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case RepoDSLPackage.VERSION_RANGE__LOWER_RANGE:
        return getLowerRange();
      case RepoDSLPackage.VERSION_RANGE__UPPER_RANGE:
        return getUpperRange();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case RepoDSLPackage.VERSION_RANGE__LOWER_RANGE:
        setLowerRange((String)newValue);
        return;
      case RepoDSLPackage.VERSION_RANGE__UPPER_RANGE:
        setUpperRange((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case RepoDSLPackage.VERSION_RANGE__LOWER_RANGE:
        setLowerRange(LOWER_RANGE_EDEFAULT);
        return;
      case RepoDSLPackage.VERSION_RANGE__UPPER_RANGE:
        setUpperRange(UPPER_RANGE_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case RepoDSLPackage.VERSION_RANGE__LOWER_RANGE:
        return LOWER_RANGE_EDEFAULT == null ? lowerRange != null : !LOWER_RANGE_EDEFAULT.equals(lowerRange);
      case RepoDSLPackage.VERSION_RANGE__UPPER_RANGE:
        return UPPER_RANGE_EDEFAULT == null ? upperRange != null : !UPPER_RANGE_EDEFAULT.equals(upperRange);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (lowerRange: ");
    result.append(lowerRange);
    result.append(", upperRange: ");
    result.append(upperRange);
    result.append(')');
    return result.toString();
  }

} //VersionRangeImpl
