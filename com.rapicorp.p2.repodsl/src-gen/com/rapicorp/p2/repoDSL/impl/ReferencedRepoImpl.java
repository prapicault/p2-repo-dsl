/**
 */
package com.rapicorp.p2.repoDSL.impl;

import com.rapicorp.p2.repoDSL.ReferencedRepo;
import com.rapicorp.p2.repoDSL.RepoDSLPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Referenced Repo</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.ReferencedRepoImpl#getAssociatedRepoURL <em>Associated Repo URL</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.ReferencedRepoImpl#getNickName <em>Nick Name</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.ReferencedRepoImpl#getEnablement <em>Enablement</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.impl.ReferencedRepoImpl#getVisibility <em>Visibility</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ReferencedRepoImpl extends MinimalEObjectImpl.Container implements ReferencedRepo
{
  /**
   * The default value of the '{@link #getAssociatedRepoURL() <em>Associated Repo URL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssociatedRepoURL()
   * @generated
   * @ordered
   */
  protected static final String ASSOCIATED_REPO_URL_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAssociatedRepoURL() <em>Associated Repo URL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAssociatedRepoURL()
   * @generated
   * @ordered
   */
  protected String associatedRepoURL = ASSOCIATED_REPO_URL_EDEFAULT;

  /**
   * The default value of the '{@link #getNickName() <em>Nick Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNickName()
   * @generated
   * @ordered
   */
  protected static final String NICK_NAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNickName() <em>Nick Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNickName()
   * @generated
   * @ordered
   */
  protected String nickName = NICK_NAME_EDEFAULT;

  /**
   * The default value of the '{@link #getEnablement() <em>Enablement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEnablement()
   * @generated
   * @ordered
   */
  protected static final String ENABLEMENT_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEnablement() <em>Enablement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEnablement()
   * @generated
   * @ordered
   */
  protected String enablement = ENABLEMENT_EDEFAULT;

  /**
   * The default value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVisibility()
   * @generated
   * @ordered
   */
  protected static final String VISIBILITY_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getVisibility() <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getVisibility()
   * @generated
   * @ordered
   */
  protected String visibility = VISIBILITY_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ReferencedRepoImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return RepoDSLPackage.Literals.REFERENCED_REPO;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getAssociatedRepoURL()
  {
    return associatedRepoURL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAssociatedRepoURL(String newAssociatedRepoURL)
  {
    String oldAssociatedRepoURL = associatedRepoURL;
    associatedRepoURL = newAssociatedRepoURL;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.REFERENCED_REPO__ASSOCIATED_REPO_URL, oldAssociatedRepoURL, associatedRepoURL));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getNickName()
  {
    return nickName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNickName(String newNickName)
  {
    String oldNickName = nickName;
    nickName = newNickName;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.REFERENCED_REPO__NICK_NAME, oldNickName, nickName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getEnablement()
  {
    return enablement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEnablement(String newEnablement)
  {
    String oldEnablement = enablement;
    enablement = newEnablement;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.REFERENCED_REPO__ENABLEMENT, oldEnablement, enablement));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getVisibility()
  {
    return visibility;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setVisibility(String newVisibility)
  {
    String oldVisibility = visibility;
    visibility = newVisibility;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, RepoDSLPackage.REFERENCED_REPO__VISIBILITY, oldVisibility, visibility));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case RepoDSLPackage.REFERENCED_REPO__ASSOCIATED_REPO_URL:
        return getAssociatedRepoURL();
      case RepoDSLPackage.REFERENCED_REPO__NICK_NAME:
        return getNickName();
      case RepoDSLPackage.REFERENCED_REPO__ENABLEMENT:
        return getEnablement();
      case RepoDSLPackage.REFERENCED_REPO__VISIBILITY:
        return getVisibility();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case RepoDSLPackage.REFERENCED_REPO__ASSOCIATED_REPO_URL:
        setAssociatedRepoURL((String)newValue);
        return;
      case RepoDSLPackage.REFERENCED_REPO__NICK_NAME:
        setNickName((String)newValue);
        return;
      case RepoDSLPackage.REFERENCED_REPO__ENABLEMENT:
        setEnablement((String)newValue);
        return;
      case RepoDSLPackage.REFERENCED_REPO__VISIBILITY:
        setVisibility((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case RepoDSLPackage.REFERENCED_REPO__ASSOCIATED_REPO_URL:
        setAssociatedRepoURL(ASSOCIATED_REPO_URL_EDEFAULT);
        return;
      case RepoDSLPackage.REFERENCED_REPO__NICK_NAME:
        setNickName(NICK_NAME_EDEFAULT);
        return;
      case RepoDSLPackage.REFERENCED_REPO__ENABLEMENT:
        setEnablement(ENABLEMENT_EDEFAULT);
        return;
      case RepoDSLPackage.REFERENCED_REPO__VISIBILITY:
        setVisibility(VISIBILITY_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case RepoDSLPackage.REFERENCED_REPO__ASSOCIATED_REPO_URL:
        return ASSOCIATED_REPO_URL_EDEFAULT == null ? associatedRepoURL != null : !ASSOCIATED_REPO_URL_EDEFAULT.equals(associatedRepoURL);
      case RepoDSLPackage.REFERENCED_REPO__NICK_NAME:
        return NICK_NAME_EDEFAULT == null ? nickName != null : !NICK_NAME_EDEFAULT.equals(nickName);
      case RepoDSLPackage.REFERENCED_REPO__ENABLEMENT:
        return ENABLEMENT_EDEFAULT == null ? enablement != null : !ENABLEMENT_EDEFAULT.equals(enablement);
      case RepoDSLPackage.REFERENCED_REPO__VISIBILITY:
        return VISIBILITY_EDEFAULT == null ? visibility != null : !VISIBILITY_EDEFAULT.equals(visibility);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (associatedRepoURL: ");
    result.append(associatedRepoURL);
    result.append(", nickName: ");
    result.append(nickName);
    result.append(", enablement: ");
    result.append(enablement);
    result.append(", visibility: ");
    result.append(visibility);
    result.append(')');
    return result.toString();
  }

} //ReferencedRepoImpl
