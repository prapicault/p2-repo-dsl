/**
 */
package com.rapicorp.p2.repoDSL.impl;

import com.rapicorp.p2.repoDSL.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RepoDSLFactoryImpl extends EFactoryImpl implements RepoDSLFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static RepoDSLFactory init()
  {
    try
    {
      RepoDSLFactory theRepoDSLFactory = (RepoDSLFactory)EPackage.Registry.INSTANCE.getEFactory(RepoDSLPackage.eNS_URI);
      if (theRepoDSLFactory != null)
      {
        return theRepoDSLFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new RepoDSLFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepoDSLFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case RepoDSLPackage.MODEL: return createModel();
      case RepoDSLPackage.REPO_NAME: return createRepoName();
      case RepoDSLPackage.REFERENCED_REPO: return createReferencedRepo();
      case RepoDSLPackage.UNCATEGORIZED_CONTENT: return createUncategorizedContent();
      case RepoDSLPackage.CATEGORY_DEF: return createCategoryDef();
      case RepoDSLPackage.IU: return createIu();
      case RepoDSLPackage.VERSION_RANGE: return createVersionRange();
      case RepoDSLPackage.FEATURE: return createFeature();
      case RepoDSLPackage.CATEGORY_NAME: return createCategoryName();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepoName createRepoName()
  {
    RepoNameImpl repoName = new RepoNameImpl();
    return repoName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ReferencedRepo createReferencedRepo()
  {
    ReferencedRepoImpl referencedRepo = new ReferencedRepoImpl();
    return referencedRepo;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public UncategorizedContent createUncategorizedContent()
  {
    UncategorizedContentImpl uncategorizedContent = new UncategorizedContentImpl();
    return uncategorizedContent;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CategoryDef createCategoryDef()
  {
    CategoryDefImpl categoryDef = new CategoryDefImpl();
    return categoryDef;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Iu createIu()
  {
    IuImpl iu = new IuImpl();
    return iu;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public VersionRange createVersionRange()
  {
    VersionRangeImpl versionRange = new VersionRangeImpl();
    return versionRange;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Feature createFeature()
  {
    FeatureImpl feature = new FeatureImpl();
    return feature;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CategoryName createCategoryName()
  {
    CategoryNameImpl categoryName = new CategoryNameImpl();
    return categoryName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RepoDSLPackage getRepoDSLPackage()
  {
    return (RepoDSLPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static RepoDSLPackage getPackage()
  {
    return RepoDSLPackage.eINSTANCE;
  }

} //RepoDSLFactoryImpl
