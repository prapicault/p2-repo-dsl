/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Referenced Repo</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getAssociatedRepoURL <em>Associated Repo URL</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getNickName <em>Nick Name</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getEnablement <em>Enablement</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getVisibility <em>Visibility</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getReferencedRepo()
 * @model
 * @generated
 */
public interface ReferencedRepo extends EObject
{
  /**
   * Returns the value of the '<em><b>Associated Repo URL</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Associated Repo URL</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Associated Repo URL</em>' attribute.
   * @see #setAssociatedRepoURL(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getReferencedRepo_AssociatedRepoURL()
   * @model
   * @generated
   */
  String getAssociatedRepoURL();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getAssociatedRepoURL <em>Associated Repo URL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Associated Repo URL</em>' attribute.
   * @see #getAssociatedRepoURL()
   * @generated
   */
  void setAssociatedRepoURL(String value);

  /**
   * Returns the value of the '<em><b>Nick Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Nick Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Nick Name</em>' attribute.
   * @see #setNickName(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getReferencedRepo_NickName()
   * @model
   * @generated
   */
  String getNickName();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getNickName <em>Nick Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Nick Name</em>' attribute.
   * @see #getNickName()
   * @generated
   */
  void setNickName(String value);

  /**
   * Returns the value of the '<em><b>Enablement</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Enablement</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Enablement</em>' attribute.
   * @see #setEnablement(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getReferencedRepo_Enablement()
   * @model
   * @generated
   */
  String getEnablement();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getEnablement <em>Enablement</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Enablement</em>' attribute.
   * @see #getEnablement()
   * @generated
   */
  void setEnablement(String value);

  /**
   * Returns the value of the '<em><b>Visibility</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Visibility</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Visibility</em>' attribute.
   * @see #setVisibility(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getReferencedRepo_Visibility()
   * @model
   * @generated
   */
  String getVisibility();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.ReferencedRepo#getVisibility <em>Visibility</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Visibility</em>' attribute.
   * @see #getVisibility()
   * @generated
   */
  void setVisibility(String value);

} // ReferencedRepo
