/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Category Def</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getCategoryDef()
 * @model
 * @generated
 */
public interface CategoryDef extends EObject
{
} // CategoryDef
