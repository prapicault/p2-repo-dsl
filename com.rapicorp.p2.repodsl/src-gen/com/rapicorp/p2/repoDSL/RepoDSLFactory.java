/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage
 * @generated
 */
public interface RepoDSLFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  RepoDSLFactory eINSTANCE = com.rapicorp.p2.repoDSL.impl.RepoDSLFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Model</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Model</em>'.
   * @generated
   */
  Model createModel();

  /**
   * Returns a new object of class '<em>Repo Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Repo Name</em>'.
   * @generated
   */
  RepoName createRepoName();

  /**
   * Returns a new object of class '<em>Referenced Repo</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Referenced Repo</em>'.
   * @generated
   */
  ReferencedRepo createReferencedRepo();

  /**
   * Returns a new object of class '<em>Uncategorized Content</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Uncategorized Content</em>'.
   * @generated
   */
  UncategorizedContent createUncategorizedContent();

  /**
   * Returns a new object of class '<em>Category Def</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Category Def</em>'.
   * @generated
   */
  CategoryDef createCategoryDef();

  /**
   * Returns a new object of class '<em>Iu</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Iu</em>'.
   * @generated
   */
  Iu createIu();

  /**
   * Returns a new object of class '<em>Version Range</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Version Range</em>'.
   * @generated
   */
  VersionRange createVersionRange();

  /**
   * Returns a new object of class '<em>Feature</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Feature</em>'.
   * @generated
   */
  Feature createFeature();

  /**
   * Returns a new object of class '<em>Category Name</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Category Name</em>'.
   * @generated
   */
  CategoryName createCategoryName();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  RepoDSLPackage getRepoDSLPackage();

} //RepoDSLFactory
