/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Iu</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.Iu#getIuId <em>Iu Id</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.Iu#getRange <em>Range</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getIu()
 * @model
 * @generated
 */
public interface Iu extends EObject
{
  /**
   * Returns the value of the '<em><b>Iu Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Iu Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Iu Id</em>' attribute.
   * @see #setIuId(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getIu_IuId()
   * @model
   * @generated
   */
  String getIuId();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.Iu#getIuId <em>Iu Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Iu Id</em>' attribute.
   * @see #getIuId()
   * @generated
   */
  void setIuId(String value);

  /**
   * Returns the value of the '<em><b>Range</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Range</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Range</em>' containment reference.
   * @see #setRange(VersionRange)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getIu_Range()
   * @model containment="true"
   * @generated
   */
  VersionRange getRange();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.Iu#getRange <em>Range</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Range</em>' containment reference.
   * @see #getRange()
   * @generated
   */
  void setRange(VersionRange value);

} // Iu
