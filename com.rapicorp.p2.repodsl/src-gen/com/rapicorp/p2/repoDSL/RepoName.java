/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Repo Name</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.RepoName#getUserReadableName <em>User Readable Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getRepoName()
 * @model
 * @generated
 */
public interface RepoName extends EObject
{
  /**
   * Returns the value of the '<em><b>User Readable Name</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>User Readable Name</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>User Readable Name</em>' attribute.
   * @see #setUserReadableName(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getRepoName_UserReadableName()
   * @model
   * @generated
   */
  String getUserReadableName();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.RepoName#getUserReadableName <em>User Readable Name</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>User Readable Name</em>' attribute.
   * @see #getUserReadableName()
   * @generated
   */
  void setUserReadableName(String value);

} // RepoName
