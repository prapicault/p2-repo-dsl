/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.Model#getRepoName <em>Repo Name</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.Model#getAssociatedRepos <em>Associated Repos</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.Model#getCategories <em>Categories</em>}</li>
 *   <li>{@link com.rapicorp.p2.repoDSL.Model#getNonCategorized <em>Non Categorized</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends EObject
{
  /**
   * Returns the value of the '<em><b>Repo Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Repo Name</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Repo Name</em>' containment reference.
   * @see #setRepoName(RepoName)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getModel_RepoName()
   * @model containment="true"
   * @generated
   */
  RepoName getRepoName();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.Model#getRepoName <em>Repo Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Repo Name</em>' containment reference.
   * @see #getRepoName()
   * @generated
   */
  void setRepoName(RepoName value);

  /**
   * Returns the value of the '<em><b>Associated Repos</b></em>' containment reference list.
   * The list contents are of type {@link com.rapicorp.p2.repoDSL.ReferencedRepo}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Associated Repos</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Associated Repos</em>' containment reference list.
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getModel_AssociatedRepos()
   * @model containment="true"
   * @generated
   */
  EList<ReferencedRepo> getAssociatedRepos();

  /**
   * Returns the value of the '<em><b>Categories</b></em>' containment reference list.
   * The list contents are of type {@link com.rapicorp.p2.repoDSL.CategoryDef}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Categories</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Categories</em>' containment reference list.
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getModel_Categories()
   * @model containment="true"
   * @generated
   */
  EList<CategoryDef> getCategories();

  /**
   * Returns the value of the '<em><b>Non Categorized</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Non Categorized</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Non Categorized</em>' containment reference.
   * @see #setNonCategorized(UncategorizedContent)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getModel_NonCategorized()
   * @model containment="true"
   * @generated
   */
  UncategorizedContent getNonCategorized();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.Model#getNonCategorized <em>Non Categorized</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Non Categorized</em>' containment reference.
   * @see #getNonCategorized()
   * @generated
   */
  void setNonCategorized(UncategorizedContent value);

} // Model
