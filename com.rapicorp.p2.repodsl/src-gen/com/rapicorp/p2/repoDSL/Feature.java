/**
 */
package com.rapicorp.p2.repoDSL;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.rapicorp.p2.repoDSL.Feature#getFeatureId <em>Feature Id</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getFeature()
 * @model
 * @generated
 */
public interface Feature extends EObject
{
  /**
   * Returns the value of the '<em><b>Feature Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Feature Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Feature Id</em>' attribute.
   * @see #setFeatureId(String)
   * @see com.rapicorp.p2.repoDSL.RepoDSLPackage#getFeature_FeatureId()
   * @model
   * @generated
   */
  String getFeatureId();

  /**
   * Sets the value of the '{@link com.rapicorp.p2.repoDSL.Feature#getFeatureId <em>Feature Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Feature Id</em>' attribute.
   * @see #getFeatureId()
   * @generated
   */
  void setFeatureId(String value);

} // Feature
