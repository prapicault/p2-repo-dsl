package com.rapicorp.p2.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.rapicorp.p2.services.RepoDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRepoDSLParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'repoName:'", "'referenceRepo:'", "'as'", "'enabled'", "'disabled'", "'hidden'", "'visible'", "'uncategorizedContent:'", "'end'", "'iu:'", "','", "'['", "']'", "'feature:'", "'category:'"
    };
    public static final int RULE_ID=5;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalRepoDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRepoDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRepoDSLParser.tokenNames; }
    public String getGrammarFileName() { return "../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g"; }



     	private RepoDSLGrammarAccess grammarAccess;
     	
        public InternalRepoDSLParser(TokenStream input, RepoDSLGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Model";	
       	}
       	
       	@Override
       	protected RepoDSLGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleModel"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:67:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:68:2: (iv_ruleModel= ruleModel EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:69:2: iv_ruleModel= ruleModel EOF
            {
             newCompositeNode(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel75);
            iv_ruleModel=ruleModel();

            state._fsp--;

             current =iv_ruleModel; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:76:1: ruleModel returns [EObject current=null] : ( ( (lv_repoName_0_0= ruleRepoName ) )? ( (lv_associatedRepos_1_0= ruleReferencedRepo ) )* ( (lv_categories_2_0= ruleCategoryDef ) )* ( (lv_nonCategorized_3_0= ruleUncategorizedContent ) )? ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_repoName_0_0 = null;

        EObject lv_associatedRepos_1_0 = null;

        EObject lv_categories_2_0 = null;

        EObject lv_nonCategorized_3_0 = null;


         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:79:28: ( ( ( (lv_repoName_0_0= ruleRepoName ) )? ( (lv_associatedRepos_1_0= ruleReferencedRepo ) )* ( (lv_categories_2_0= ruleCategoryDef ) )* ( (lv_nonCategorized_3_0= ruleUncategorizedContent ) )? ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:80:1: ( ( (lv_repoName_0_0= ruleRepoName ) )? ( (lv_associatedRepos_1_0= ruleReferencedRepo ) )* ( (lv_categories_2_0= ruleCategoryDef ) )* ( (lv_nonCategorized_3_0= ruleUncategorizedContent ) )? )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:80:1: ( ( (lv_repoName_0_0= ruleRepoName ) )? ( (lv_associatedRepos_1_0= ruleReferencedRepo ) )* ( (lv_categories_2_0= ruleCategoryDef ) )* ( (lv_nonCategorized_3_0= ruleUncategorizedContent ) )? )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:80:2: ( (lv_repoName_0_0= ruleRepoName ) )? ( (lv_associatedRepos_1_0= ruleReferencedRepo ) )* ( (lv_categories_2_0= ruleCategoryDef ) )* ( (lv_nonCategorized_3_0= ruleUncategorizedContent ) )?
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:80:2: ( (lv_repoName_0_0= ruleRepoName ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:81:1: (lv_repoName_0_0= ruleRepoName )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:81:1: (lv_repoName_0_0= ruleRepoName )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:82:3: lv_repoName_0_0= ruleRepoName
                    {
                     
                    	        newCompositeNode(grammarAccess.getModelAccess().getRepoNameRepoNameParserRuleCall_0_0()); 
                    	    
                    pushFollow(FOLLOW_ruleRepoName_in_ruleModel131);
                    lv_repoName_0_0=ruleRepoName();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getModelRule());
                    	        }
                           		set(
                           			current, 
                           			"repoName",
                            		lv_repoName_0_0, 
                            		"RepoName");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }

            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:98:3: ( (lv_associatedRepos_1_0= ruleReferencedRepo ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==12) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:99:1: (lv_associatedRepos_1_0= ruleReferencedRepo )
            	    {
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:99:1: (lv_associatedRepos_1_0= ruleReferencedRepo )
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:100:3: lv_associatedRepos_1_0= ruleReferencedRepo
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getModelAccess().getAssociatedReposReferencedRepoParserRuleCall_1_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleReferencedRepo_in_ruleModel153);
            	    lv_associatedRepos_1_0=ruleReferencedRepo();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"associatedRepos",
            	            		lv_associatedRepos_1_0, 
            	            		"ReferencedRepo");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:116:3: ( (lv_categories_2_0= ruleCategoryDef ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==25) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:117:1: (lv_categories_2_0= ruleCategoryDef )
            	    {
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:117:1: (lv_categories_2_0= ruleCategoryDef )
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:118:3: lv_categories_2_0= ruleCategoryDef
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getModelAccess().getCategoriesCategoryDefParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleCategoryDef_in_ruleModel175);
            	    lv_categories_2_0=ruleCategoryDef();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getModelRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"categories",
            	            		lv_categories_2_0, 
            	            		"CategoryDef");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:134:3: ( (lv_nonCategorized_3_0= ruleUncategorizedContent ) )?
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==18) ) {
                alt4=1;
            }
            switch (alt4) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:135:1: (lv_nonCategorized_3_0= ruleUncategorizedContent )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:135:1: (lv_nonCategorized_3_0= ruleUncategorizedContent )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:136:3: lv_nonCategorized_3_0= ruleUncategorizedContent
                    {
                     
                    	        newCompositeNode(grammarAccess.getModelAccess().getNonCategorizedUncategorizedContentParserRuleCall_3_0()); 
                    	    
                    pushFollow(FOLLOW_ruleUncategorizedContent_in_ruleModel197);
                    lv_nonCategorized_3_0=ruleUncategorizedContent();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getModelRule());
                    	        }
                           		set(
                           			current, 
                           			"nonCategorized",
                            		lv_nonCategorized_3_0, 
                            		"UncategorizedContent");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleRepoName"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:160:1: entryRuleRepoName returns [EObject current=null] : iv_ruleRepoName= ruleRepoName EOF ;
    public final EObject entryRuleRepoName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRepoName = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:161:2: (iv_ruleRepoName= ruleRepoName EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:162:2: iv_ruleRepoName= ruleRepoName EOF
            {
             newCompositeNode(grammarAccess.getRepoNameRule()); 
            pushFollow(FOLLOW_ruleRepoName_in_entryRuleRepoName234);
            iv_ruleRepoName=ruleRepoName();

            state._fsp--;

             current =iv_ruleRepoName; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepoName244); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRepoName"


    // $ANTLR start "ruleRepoName"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:169:1: ruleRepoName returns [EObject current=null] : (otherlv_0= 'repoName:' ( (lv_userReadableName_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleRepoName() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_userReadableName_1_0=null;

         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:172:28: ( (otherlv_0= 'repoName:' ( (lv_userReadableName_1_0= RULE_STRING ) ) ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:173:1: (otherlv_0= 'repoName:' ( (lv_userReadableName_1_0= RULE_STRING ) ) )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:173:1: (otherlv_0= 'repoName:' ( (lv_userReadableName_1_0= RULE_STRING ) ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:173:3: otherlv_0= 'repoName:' ( (lv_userReadableName_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleRepoName281); 

                	newLeafNode(otherlv_0, grammarAccess.getRepoNameAccess().getRepoNameKeyword_0());
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:177:1: ( (lv_userReadableName_1_0= RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:178:1: (lv_userReadableName_1_0= RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:178:1: (lv_userReadableName_1_0= RULE_STRING )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:179:3: lv_userReadableName_1_0= RULE_STRING
            {
            lv_userReadableName_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleRepoName298); 

            			newLeafNode(lv_userReadableName_1_0, grammarAccess.getRepoNameAccess().getUserReadableNameSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRepoNameRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"userReadableName",
                    		lv_userReadableName_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRepoName"


    // $ANTLR start "entryRuleReferencedRepo"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:203:1: entryRuleReferencedRepo returns [EObject current=null] : iv_ruleReferencedRepo= ruleReferencedRepo EOF ;
    public final EObject entryRuleReferencedRepo() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReferencedRepo = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:204:2: (iv_ruleReferencedRepo= ruleReferencedRepo EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:205:2: iv_ruleReferencedRepo= ruleReferencedRepo EOF
            {
             newCompositeNode(grammarAccess.getReferencedRepoRule()); 
            pushFollow(FOLLOW_ruleReferencedRepo_in_entryRuleReferencedRepo339);
            iv_ruleReferencedRepo=ruleReferencedRepo();

            state._fsp--;

             current =iv_ruleReferencedRepo; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReferencedRepo349); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReferencedRepo"


    // $ANTLR start "ruleReferencedRepo"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:212:1: ruleReferencedRepo returns [EObject current=null] : (otherlv_0= 'referenceRepo:' ( (lv_associatedRepoURL_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_nickName_3_0= RULE_STRING ) ) ( ( ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) ) ) ( ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) ) ) )? ) ;
    public final EObject ruleReferencedRepo() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_associatedRepoURL_1_0=null;
        Token otherlv_2=null;
        Token lv_nickName_3_0=null;
        Token lv_enablement_4_1=null;
        Token lv_enablement_4_2=null;
        Token lv_visibility_5_1=null;
        Token lv_visibility_5_2=null;

         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:215:28: ( (otherlv_0= 'referenceRepo:' ( (lv_associatedRepoURL_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_nickName_3_0= RULE_STRING ) ) ( ( ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) ) ) ( ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) ) ) )? ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:216:1: (otherlv_0= 'referenceRepo:' ( (lv_associatedRepoURL_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_nickName_3_0= RULE_STRING ) ) ( ( ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) ) ) ( ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) ) ) )? )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:216:1: (otherlv_0= 'referenceRepo:' ( (lv_associatedRepoURL_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_nickName_3_0= RULE_STRING ) ) ( ( ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) ) ) ( ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) ) ) )? )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:216:3: otherlv_0= 'referenceRepo:' ( (lv_associatedRepoURL_1_0= RULE_STRING ) ) otherlv_2= 'as' ( (lv_nickName_3_0= RULE_STRING ) ) ( ( ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) ) ) ( ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) ) ) )?
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12_in_ruleReferencedRepo386); 

                	newLeafNode(otherlv_0, grammarAccess.getReferencedRepoAccess().getReferenceRepoKeyword_0());
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:220:1: ( (lv_associatedRepoURL_1_0= RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:221:1: (lv_associatedRepoURL_1_0= RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:221:1: (lv_associatedRepoURL_1_0= RULE_STRING )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:222:3: lv_associatedRepoURL_1_0= RULE_STRING
            {
            lv_associatedRepoURL_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleReferencedRepo403); 

            			newLeafNode(lv_associatedRepoURL_1_0, grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReferencedRepoRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"associatedRepoURL",
                    		lv_associatedRepoURL_1_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleReferencedRepo420); 

                	newLeafNode(otherlv_2, grammarAccess.getReferencedRepoAccess().getAsKeyword_2());
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:242:1: ( (lv_nickName_3_0= RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:243:1: (lv_nickName_3_0= RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:243:1: (lv_nickName_3_0= RULE_STRING )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:244:3: lv_nickName_3_0= RULE_STRING
            {
            lv_nickName_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleReferencedRepo437); 

            			newLeafNode(lv_nickName_3_0, grammarAccess.getReferencedRepoAccess().getNickNameSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getReferencedRepoRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"nickName",
                    		lv_nickName_3_0, 
                    		"STRING");
            	    

            }


            }

            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:260:2: ( ( ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) ) ) ( ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) ) ) )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( ((LA7_0>=14 && LA7_0<=15)) ) {
                alt7=1;
            }
            switch (alt7) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:260:3: ( ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) ) ) ( ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) ) )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:260:3: ( ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) ) )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:261:1: ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:261:1: ( (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' ) )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:262:1: (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:262:1: (lv_enablement_4_1= 'enabled' | lv_enablement_4_2= 'disabled' )
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0==14) ) {
                        alt5=1;
                    }
                    else if ( (LA5_0==15) ) {
                        alt5=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 5, 0, input);

                        throw nvae;
                    }
                    switch (alt5) {
                        case 1 :
                            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:263:3: lv_enablement_4_1= 'enabled'
                            {
                            lv_enablement_4_1=(Token)match(input,14,FOLLOW_14_in_ruleReferencedRepo463); 

                                    newLeafNode(lv_enablement_4_1, grammarAccess.getReferencedRepoAccess().getEnablementEnabledKeyword_4_0_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getReferencedRepoRule());
                            	        }
                                   		setWithLastConsumed(current, "enablement", lv_enablement_4_1, null);
                            	    

                            }
                            break;
                        case 2 :
                            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:275:8: lv_enablement_4_2= 'disabled'
                            {
                            lv_enablement_4_2=(Token)match(input,15,FOLLOW_15_in_ruleReferencedRepo492); 

                                    newLeafNode(lv_enablement_4_2, grammarAccess.getReferencedRepoAccess().getEnablementDisabledKeyword_4_0_0_1());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getReferencedRepoRule());
                            	        }
                                   		setWithLastConsumed(current, "enablement", lv_enablement_4_2, null);
                            	    

                            }
                            break;

                    }


                    }


                    }

                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:290:2: ( ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) ) )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:291:1: ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:291:1: ( (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' ) )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:292:1: (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:292:1: (lv_visibility_5_1= 'hidden' | lv_visibility_5_2= 'visible' )
                    int alt6=2;
                    int LA6_0 = input.LA(1);

                    if ( (LA6_0==16) ) {
                        alt6=1;
                    }
                    else if ( (LA6_0==17) ) {
                        alt6=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 6, 0, input);

                        throw nvae;
                    }
                    switch (alt6) {
                        case 1 :
                            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:293:3: lv_visibility_5_1= 'hidden'
                            {
                            lv_visibility_5_1=(Token)match(input,16,FOLLOW_16_in_ruleReferencedRepo528); 

                                    newLeafNode(lv_visibility_5_1, grammarAccess.getReferencedRepoAccess().getVisibilityHiddenKeyword_4_1_0_0());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getReferencedRepoRule());
                            	        }
                                   		setWithLastConsumed(current, "visibility", lv_visibility_5_1, null);
                            	    

                            }
                            break;
                        case 2 :
                            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:305:8: lv_visibility_5_2= 'visible'
                            {
                            lv_visibility_5_2=(Token)match(input,17,FOLLOW_17_in_ruleReferencedRepo557); 

                                    newLeafNode(lv_visibility_5_2, grammarAccess.getReferencedRepoAccess().getVisibilityVisibleKeyword_4_1_0_1());
                                

                            	        if (current==null) {
                            	            current = createModelElement(grammarAccess.getReferencedRepoRule());
                            	        }
                                   		setWithLastConsumed(current, "visibility", lv_visibility_5_2, null);
                            	    

                            }
                            break;

                    }


                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReferencedRepo"


    // $ANTLR start "entryRuleUncategorizedContent"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:328:1: entryRuleUncategorizedContent returns [EObject current=null] : iv_ruleUncategorizedContent= ruleUncategorizedContent EOF ;
    public final EObject entryRuleUncategorizedContent() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUncategorizedContent = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:329:2: (iv_ruleUncategorizedContent= ruleUncategorizedContent EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:330:2: iv_ruleUncategorizedContent= ruleUncategorizedContent EOF
            {
             newCompositeNode(grammarAccess.getUncategorizedContentRule()); 
            pushFollow(FOLLOW_ruleUncategorizedContent_in_entryRuleUncategorizedContent611);
            iv_ruleUncategorizedContent=ruleUncategorizedContent();

            state._fsp--;

             current =iv_ruleUncategorizedContent; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUncategorizedContent621); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUncategorizedContent"


    // $ANTLR start "ruleUncategorizedContent"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:337:1: ruleUncategorizedContent returns [EObject current=null] : (otherlv_0= 'uncategorizedContent:' ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu ) ) )+ otherlv_2= 'end' ) ;
    public final EObject ruleUncategorizedContent() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_entries_1_1 = null;

        EObject lv_entries_1_2 = null;


         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:340:28: ( (otherlv_0= 'uncategorizedContent:' ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu ) ) )+ otherlv_2= 'end' ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:341:1: (otherlv_0= 'uncategorizedContent:' ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu ) ) )+ otherlv_2= 'end' )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:341:1: (otherlv_0= 'uncategorizedContent:' ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu ) ) )+ otherlv_2= 'end' )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:341:3: otherlv_0= 'uncategorizedContent:' ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu ) ) )+ otherlv_2= 'end'
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleUncategorizedContent658); 

                	newLeafNode(otherlv_0, grammarAccess.getUncategorizedContentAccess().getUncategorizedContentKeyword_0());
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:345:1: ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu ) ) )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==20||LA9_0==24) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:346:1: ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu ) )
            	    {
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:346:1: ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu ) )
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:347:1: (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu )
            	    {
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:347:1: (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu )
            	    int alt8=2;
            	    int LA8_0 = input.LA(1);

            	    if ( (LA8_0==24) ) {
            	        alt8=1;
            	    }
            	    else if ( (LA8_0==20) ) {
            	        alt8=2;
            	    }
            	    else {
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 8, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt8) {
            	        case 1 :
            	            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:348:3: lv_entries_1_1= ruleFeature
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getUncategorizedContentAccess().getEntriesFeatureParserRuleCall_1_0_0()); 
            	            	    
            	            pushFollow(FOLLOW_ruleFeature_in_ruleUncategorizedContent681);
            	            lv_entries_1_1=ruleFeature();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getUncategorizedContentRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"entries",
            	                    		lv_entries_1_1, 
            	                    		"Feature");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:363:8: lv_entries_1_2= ruleIu
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getUncategorizedContentAccess().getEntriesIuParserRuleCall_1_0_1()); 
            	            	    
            	            pushFollow(FOLLOW_ruleIu_in_ruleUncategorizedContent700);
            	            lv_entries_1_2=ruleIu();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getUncategorizedContentRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"entries",
            	                    		lv_entries_1_2, 
            	                    		"Iu");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);

            otherlv_2=(Token)match(input,19,FOLLOW_19_in_ruleUncategorizedContent716); 

                	newLeafNode(otherlv_2, grammarAccess.getUncategorizedContentAccess().getEndKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUncategorizedContent"


    // $ANTLR start "entryRuleCategoryDef"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:393:1: entryRuleCategoryDef returns [EObject current=null] : iv_ruleCategoryDef= ruleCategoryDef EOF ;
    public final EObject entryRuleCategoryDef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCategoryDef = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:394:2: (iv_ruleCategoryDef= ruleCategoryDef EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:395:2: iv_ruleCategoryDef= ruleCategoryDef EOF
            {
             newCompositeNode(grammarAccess.getCategoryDefRule()); 
            pushFollow(FOLLOW_ruleCategoryDef_in_entryRuleCategoryDef752);
            iv_ruleCategoryDef=ruleCategoryDef();

            state._fsp--;

             current =iv_ruleCategoryDef; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCategoryDef762); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCategoryDef"


    // $ANTLR start "ruleCategoryDef"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:402:1: ruleCategoryDef returns [EObject current=null] : (this_CategoryName_0= ruleCategoryName ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef ) ) )* otherlv_2= 'end' ) ;
    public final EObject ruleCategoryDef() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject this_CategoryName_0 = null;

        EObject lv_entries_1_1 = null;

        EObject lv_entries_1_2 = null;

        EObject lv_entries_1_3 = null;


         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:405:28: ( (this_CategoryName_0= ruleCategoryName ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef ) ) )* otherlv_2= 'end' ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:406:1: (this_CategoryName_0= ruleCategoryName ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef ) ) )* otherlv_2= 'end' )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:406:1: (this_CategoryName_0= ruleCategoryName ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef ) ) )* otherlv_2= 'end' )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:407:5: this_CategoryName_0= ruleCategoryName ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef ) ) )* otherlv_2= 'end'
            {
             
                    newCompositeNode(grammarAccess.getCategoryDefAccess().getCategoryNameParserRuleCall_0()); 
                
            pushFollow(FOLLOW_ruleCategoryName_in_ruleCategoryDef809);
            this_CategoryName_0=ruleCategoryName();

            state._fsp--;

             
                    current = this_CategoryName_0; 
                    afterParserOrEnumRuleCall();
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:415:1: ( ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==20||(LA11_0>=24 && LA11_0<=25)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:416:1: ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef ) )
            	    {
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:416:1: ( (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef ) )
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:417:1: (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef )
            	    {
            	    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:417:1: (lv_entries_1_1= ruleFeature | lv_entries_1_2= ruleIu | lv_entries_1_3= ruleCategoryDef )
            	    int alt10=3;
            	    switch ( input.LA(1) ) {
            	    case 24:
            	        {
            	        alt10=1;
            	        }
            	        break;
            	    case 20:
            	        {
            	        alt10=2;
            	        }
            	        break;
            	    case 25:
            	        {
            	        alt10=3;
            	        }
            	        break;
            	    default:
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 10, 0, input);

            	        throw nvae;
            	    }

            	    switch (alt10) {
            	        case 1 :
            	            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:418:3: lv_entries_1_1= ruleFeature
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getCategoryDefAccess().getEntriesFeatureParserRuleCall_1_0_0()); 
            	            	    
            	            pushFollow(FOLLOW_ruleFeature_in_ruleCategoryDef831);
            	            lv_entries_1_1=ruleFeature();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getCategoryDefRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"entries",
            	                    		lv_entries_1_1, 
            	                    		"Feature");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }
            	            break;
            	        case 2 :
            	            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:433:8: lv_entries_1_2= ruleIu
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getCategoryDefAccess().getEntriesIuParserRuleCall_1_0_1()); 
            	            	    
            	            pushFollow(FOLLOW_ruleIu_in_ruleCategoryDef850);
            	            lv_entries_1_2=ruleIu();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getCategoryDefRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"entries",
            	                    		lv_entries_1_2, 
            	                    		"Iu");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }
            	            break;
            	        case 3 :
            	            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:448:8: lv_entries_1_3= ruleCategoryDef
            	            {
            	             
            	            	        newCompositeNode(grammarAccess.getCategoryDefAccess().getEntriesCategoryDefParserRuleCall_1_0_2()); 
            	            	    
            	            pushFollow(FOLLOW_ruleCategoryDef_in_ruleCategoryDef869);
            	            lv_entries_1_3=ruleCategoryDef();

            	            state._fsp--;


            	            	        if (current==null) {
            	            	            current = createModelElementForParent(grammarAccess.getCategoryDefRule());
            	            	        }
            	                   		add(
            	                   			current, 
            	                   			"entries",
            	                    		lv_entries_1_3, 
            	                    		"CategoryDef");
            	            	        afterParserOrEnumRuleCall();
            	            	    

            	            }
            	            break;

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

            otherlv_2=(Token)match(input,19,FOLLOW_19_in_ruleCategoryDef885); 

                	newLeafNode(otherlv_2, grammarAccess.getCategoryDefAccess().getEndKeyword_2());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCategoryDef"


    // $ANTLR start "entryRuleIu"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:478:1: entryRuleIu returns [EObject current=null] : iv_ruleIu= ruleIu EOF ;
    public final EObject entryRuleIu() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleIu = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:479:2: (iv_ruleIu= ruleIu EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:480:2: iv_ruleIu= ruleIu EOF
            {
             newCompositeNode(grammarAccess.getIuRule()); 
            pushFollow(FOLLOW_ruleIu_in_entryRuleIu921);
            iv_ruleIu=ruleIu();

            state._fsp--;

             current =iv_ruleIu; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIu931); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIu"


    // $ANTLR start "ruleIu"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:487:1: ruleIu returns [EObject current=null] : (otherlv_0= 'iu:' ( (lv_iuId_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_range_3_0= ruleVersionRange ) ) )? ) ;
    public final EObject ruleIu() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_iuId_1_0=null;
        Token otherlv_2=null;
        EObject lv_range_3_0 = null;


         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:490:28: ( (otherlv_0= 'iu:' ( (lv_iuId_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_range_3_0= ruleVersionRange ) ) )? ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:491:1: (otherlv_0= 'iu:' ( (lv_iuId_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_range_3_0= ruleVersionRange ) ) )? )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:491:1: (otherlv_0= 'iu:' ( (lv_iuId_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_range_3_0= ruleVersionRange ) ) )? )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:491:3: otherlv_0= 'iu:' ( (lv_iuId_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_range_3_0= ruleVersionRange ) ) )?
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleIu968); 

                	newLeafNode(otherlv_0, grammarAccess.getIuAccess().getIuKeyword_0());
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:495:1: ( (lv_iuId_1_0= RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:496:1: (lv_iuId_1_0= RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:496:1: (lv_iuId_1_0= RULE_STRING )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:497:3: lv_iuId_1_0= RULE_STRING
            {
            lv_iuId_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleIu985); 

            			newLeafNode(lv_iuId_1_0, grammarAccess.getIuAccess().getIuIdSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getIuRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"iuId",
                    		lv_iuId_1_0, 
                    		"STRING");
            	    

            }


            }

            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:513:2: (otherlv_2= ',' ( (lv_range_3_0= ruleVersionRange ) ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==21) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:513:4: otherlv_2= ',' ( (lv_range_3_0= ruleVersionRange ) )
                    {
                    otherlv_2=(Token)match(input,21,FOLLOW_21_in_ruleIu1003); 

                        	newLeafNode(otherlv_2, grammarAccess.getIuAccess().getCommaKeyword_2_0());
                        
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:517:1: ( (lv_range_3_0= ruleVersionRange ) )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:518:1: (lv_range_3_0= ruleVersionRange )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:518:1: (lv_range_3_0= ruleVersionRange )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:519:3: lv_range_3_0= ruleVersionRange
                    {
                     
                    	        newCompositeNode(grammarAccess.getIuAccess().getRangeVersionRangeParserRuleCall_2_1_0()); 
                    	    
                    pushFollow(FOLLOW_ruleVersionRange_in_ruleIu1024);
                    lv_range_3_0=ruleVersionRange();

                    state._fsp--;


                    	        if (current==null) {
                    	            current = createModelElementForParent(grammarAccess.getIuRule());
                    	        }
                           		set(
                           			current, 
                           			"range",
                            		lv_range_3_0, 
                            		"VersionRange");
                    	        afterParserOrEnumRuleCall();
                    	    

                    }


                    }


                    }
                    break;

            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIu"


    // $ANTLR start "entryRuleVersionRange"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:543:1: entryRuleVersionRange returns [EObject current=null] : iv_ruleVersionRange= ruleVersionRange EOF ;
    public final EObject entryRuleVersionRange() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVersionRange = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:544:2: (iv_ruleVersionRange= ruleVersionRange EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:545:2: iv_ruleVersionRange= ruleVersionRange EOF
            {
             newCompositeNode(grammarAccess.getVersionRangeRule()); 
            pushFollow(FOLLOW_ruleVersionRange_in_entryRuleVersionRange1062);
            iv_ruleVersionRange=ruleVersionRange();

            state._fsp--;

             current =iv_ruleVersionRange; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVersionRange1072); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVersionRange"


    // $ANTLR start "ruleVersionRange"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:552:1: ruleVersionRange returns [EObject current=null] : (otherlv_0= '[' ( (lv_lowerRange_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_upperRange_3_0= RULE_STRING ) ) )? otherlv_4= ']' ) ;
    public final EObject ruleVersionRange() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_lowerRange_1_0=null;
        Token otherlv_2=null;
        Token lv_upperRange_3_0=null;
        Token otherlv_4=null;

         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:555:28: ( (otherlv_0= '[' ( (lv_lowerRange_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_upperRange_3_0= RULE_STRING ) ) )? otherlv_4= ']' ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:556:1: (otherlv_0= '[' ( (lv_lowerRange_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_upperRange_3_0= RULE_STRING ) ) )? otherlv_4= ']' )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:556:1: (otherlv_0= '[' ( (lv_lowerRange_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_upperRange_3_0= RULE_STRING ) ) )? otherlv_4= ']' )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:556:3: otherlv_0= '[' ( (lv_lowerRange_1_0= RULE_STRING ) ) (otherlv_2= ',' ( (lv_upperRange_3_0= RULE_STRING ) ) )? otherlv_4= ']'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleVersionRange1109); 

                	newLeafNode(otherlv_0, grammarAccess.getVersionRangeAccess().getLeftSquareBracketKeyword_0());
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:560:1: ( (lv_lowerRange_1_0= RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:561:1: (lv_lowerRange_1_0= RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:561:1: (lv_lowerRange_1_0= RULE_STRING )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:562:3: lv_lowerRange_1_0= RULE_STRING
            {
            lv_lowerRange_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleVersionRange1126); 

            			newLeafNode(lv_lowerRange_1_0, grammarAccess.getVersionRangeAccess().getLowerRangeSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getVersionRangeRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"lowerRange",
                    		lv_lowerRange_1_0, 
                    		"STRING");
            	    

            }


            }

            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:578:2: (otherlv_2= ',' ( (lv_upperRange_3_0= RULE_STRING ) ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==21) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:578:4: otherlv_2= ',' ( (lv_upperRange_3_0= RULE_STRING ) )
                    {
                    otherlv_2=(Token)match(input,21,FOLLOW_21_in_ruleVersionRange1144); 

                        	newLeafNode(otherlv_2, grammarAccess.getVersionRangeAccess().getCommaKeyword_2_0());
                        
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:582:1: ( (lv_upperRange_3_0= RULE_STRING ) )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:583:1: (lv_upperRange_3_0= RULE_STRING )
                    {
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:583:1: (lv_upperRange_3_0= RULE_STRING )
                    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:584:3: lv_upperRange_3_0= RULE_STRING
                    {
                    lv_upperRange_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleVersionRange1161); 

                    			newLeafNode(lv_upperRange_3_0, grammarAccess.getVersionRangeAccess().getUpperRangeSTRINGTerminalRuleCall_2_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getVersionRangeRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"upperRange",
                            		lv_upperRange_3_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;

            }

            otherlv_4=(Token)match(input,23,FOLLOW_23_in_ruleVersionRange1180); 

                	newLeafNode(otherlv_4, grammarAccess.getVersionRangeAccess().getRightSquareBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVersionRange"


    // $ANTLR start "entryRuleFeature"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:612:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:613:2: (iv_ruleFeature= ruleFeature EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:614:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_ruleFeature_in_entryRuleFeature1216);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFeature1226); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:621:1: ruleFeature returns [EObject current=null] : (otherlv_0= 'feature:' ( (lv_featureId_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_featureId_1_0=null;

         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:624:28: ( (otherlv_0= 'feature:' ( (lv_featureId_1_0= RULE_STRING ) ) ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:625:1: (otherlv_0= 'feature:' ( (lv_featureId_1_0= RULE_STRING ) ) )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:625:1: (otherlv_0= 'feature:' ( (lv_featureId_1_0= RULE_STRING ) ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:625:3: otherlv_0= 'feature:' ( (lv_featureId_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,24,FOLLOW_24_in_ruleFeature1263); 

                	newLeafNode(otherlv_0, grammarAccess.getFeatureAccess().getFeatureKeyword_0());
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:629:1: ( (lv_featureId_1_0= RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:630:1: (lv_featureId_1_0= RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:630:1: (lv_featureId_1_0= RULE_STRING )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:631:3: lv_featureId_1_0= RULE_STRING
            {
            lv_featureId_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleFeature1280); 

            			newLeafNode(lv_featureId_1_0, grammarAccess.getFeatureAccess().getFeatureIdSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFeatureRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"featureId",
                    		lv_featureId_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleCategoryName"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:655:1: entryRuleCategoryName returns [EObject current=null] : iv_ruleCategoryName= ruleCategoryName EOF ;
    public final EObject entryRuleCategoryName() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCategoryName = null;


        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:656:2: (iv_ruleCategoryName= ruleCategoryName EOF )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:657:2: iv_ruleCategoryName= ruleCategoryName EOF
            {
             newCompositeNode(grammarAccess.getCategoryNameRule()); 
            pushFollow(FOLLOW_ruleCategoryName_in_entryRuleCategoryName1321);
            iv_ruleCategoryName=ruleCategoryName();

            state._fsp--;

             current =iv_ruleCategoryName; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCategoryName1331); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCategoryName"


    // $ANTLR start "ruleCategoryName"
    // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:664:1: ruleCategoryName returns [EObject current=null] : (otherlv_0= 'category:' ( (lv_categoryName_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleCategoryName() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_categoryName_1_0=null;

         enterRule(); 
            
        try {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:667:28: ( (otherlv_0= 'category:' ( (lv_categoryName_1_0= RULE_STRING ) ) ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:668:1: (otherlv_0= 'category:' ( (lv_categoryName_1_0= RULE_STRING ) ) )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:668:1: (otherlv_0= 'category:' ( (lv_categoryName_1_0= RULE_STRING ) ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:668:3: otherlv_0= 'category:' ( (lv_categoryName_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,25,FOLLOW_25_in_ruleCategoryName1368); 

                	newLeafNode(otherlv_0, grammarAccess.getCategoryNameAccess().getCategoryKeyword_0());
                
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:672:1: ( (lv_categoryName_1_0= RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:673:1: (lv_categoryName_1_0= RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:673:1: (lv_categoryName_1_0= RULE_STRING )
            // ../com.rapicorp.p2.repodsl/src-gen/com/rapicorp/p2/parser/antlr/internal/InternalRepoDSL.g:674:3: lv_categoryName_1_0= RULE_STRING
            {
            lv_categoryName_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleCategoryName1385); 

            			newLeafNode(lv_categoryName_1_0, grammarAccess.getCategoryNameAccess().getCategoryNameSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCategoryNameRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"categoryName",
                    		lv_categoryName_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCategoryName"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepoName_in_ruleModel131 = new BitSet(new long[]{0x0000000002041002L});
    public static final BitSet FOLLOW_ruleReferencedRepo_in_ruleModel153 = new BitSet(new long[]{0x0000000002041002L});
    public static final BitSet FOLLOW_ruleCategoryDef_in_ruleModel175 = new BitSet(new long[]{0x0000000002040002L});
    public static final BitSet FOLLOW_ruleUncategorizedContent_in_ruleModel197 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepoName_in_entryRuleRepoName234 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepoName244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleRepoName281 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleRepoName298 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferencedRepo_in_entryRuleReferencedRepo339 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReferencedRepo349 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleReferencedRepo386 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleReferencedRepo403 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleReferencedRepo420 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleReferencedRepo437 = new BitSet(new long[]{0x000000000000C002L});
    public static final BitSet FOLLOW_14_in_ruleReferencedRepo463 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_15_in_ruleReferencedRepo492 = new BitSet(new long[]{0x0000000000030000L});
    public static final BitSet FOLLOW_16_in_ruleReferencedRepo528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleReferencedRepo557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUncategorizedContent_in_entryRuleUncategorizedContent611 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUncategorizedContent621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleUncategorizedContent658 = new BitSet(new long[]{0x0000000001100000L});
    public static final BitSet FOLLOW_ruleFeature_in_ruleUncategorizedContent681 = new BitSet(new long[]{0x0000000001180000L});
    public static final BitSet FOLLOW_ruleIu_in_ruleUncategorizedContent700 = new BitSet(new long[]{0x0000000001180000L});
    public static final BitSet FOLLOW_19_in_ruleUncategorizedContent716 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCategoryDef_in_entryRuleCategoryDef752 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCategoryDef762 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCategoryName_in_ruleCategoryDef809 = new BitSet(new long[]{0x0000000003180000L});
    public static final BitSet FOLLOW_ruleFeature_in_ruleCategoryDef831 = new BitSet(new long[]{0x0000000003180000L});
    public static final BitSet FOLLOW_ruleIu_in_ruleCategoryDef850 = new BitSet(new long[]{0x0000000003180000L});
    public static final BitSet FOLLOW_ruleCategoryDef_in_ruleCategoryDef869 = new BitSet(new long[]{0x0000000003180000L});
    public static final BitSet FOLLOW_19_in_ruleCategoryDef885 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIu_in_entryRuleIu921 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIu931 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleIu968 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleIu985 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_21_in_ruleIu1003 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_ruleVersionRange_in_ruleIu1024 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVersionRange_in_entryRuleVersionRange1062 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVersionRange1072 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleVersionRange1109 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleVersionRange1126 = new BitSet(new long[]{0x0000000000A00000L});
    public static final BitSet FOLLOW_21_in_ruleVersionRange1144 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleVersionRange1161 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_23_in_ruleVersionRange1180 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature1216 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFeature1226 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_ruleFeature1263 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleFeature1280 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCategoryName_in_entryRuleCategoryName1321 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCategoryName1331 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_ruleCategoryName1368 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleCategoryName1385 = new BitSet(new long[]{0x0000000000000002L});

}