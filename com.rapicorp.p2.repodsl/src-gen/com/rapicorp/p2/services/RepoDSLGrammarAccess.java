/*
* generated by Xtext
*/
package com.rapicorp.p2.services;

import com.google.inject.Singleton;
import com.google.inject.Inject;

import java.util.List;

import org.eclipse.xtext.*;
import org.eclipse.xtext.service.GrammarProvider;
import org.eclipse.xtext.service.AbstractElementFinder.*;

import org.eclipse.xtext.common.services.TerminalsGrammarAccess;

@Singleton
public class RepoDSLGrammarAccess extends AbstractGrammarElementFinder {
	
	
	public class ModelElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Model");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Assignment cRepoNameAssignment_0 = (Assignment)cGroup.eContents().get(0);
		private final RuleCall cRepoNameRepoNameParserRuleCall_0_0 = (RuleCall)cRepoNameAssignment_0.eContents().get(0);
		private final Assignment cAssociatedReposAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cAssociatedReposReferencedRepoParserRuleCall_1_0 = (RuleCall)cAssociatedReposAssignment_1.eContents().get(0);
		private final Assignment cCategoriesAssignment_2 = (Assignment)cGroup.eContents().get(2);
		private final RuleCall cCategoriesCategoryDefParserRuleCall_2_0 = (RuleCall)cCategoriesAssignment_2.eContents().get(0);
		private final Assignment cNonCategorizedAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cNonCategorizedUncategorizedContentParserRuleCall_3_0 = (RuleCall)cNonCategorizedAssignment_3.eContents().get(0);
		
		////To add
		//
		////referencedRepos vs associatedRepos
		//
		////inclusion control (only IU, sliced, resolved)
		//
		////stats tracker URL
		//
		////mirror list URL
		//
		////better p2 QL
		//
		//Model:
		//
		//	repoName=RepoName? associatedRepos+=ReferencedRepo* categories+=CategoryDef* nonCategorized=UncategorizedContent?;
		public ParserRule getRule() { return rule; }

		//repoName=RepoName? associatedRepos+=ReferencedRepo* categories+=CategoryDef* nonCategorized=UncategorizedContent?
		public Group getGroup() { return cGroup; }

		//repoName=RepoName?
		public Assignment getRepoNameAssignment_0() { return cRepoNameAssignment_0; }

		//RepoName
		public RuleCall getRepoNameRepoNameParserRuleCall_0_0() { return cRepoNameRepoNameParserRuleCall_0_0; }

		//associatedRepos+=ReferencedRepo*
		public Assignment getAssociatedReposAssignment_1() { return cAssociatedReposAssignment_1; }

		//ReferencedRepo
		public RuleCall getAssociatedReposReferencedRepoParserRuleCall_1_0() { return cAssociatedReposReferencedRepoParserRuleCall_1_0; }

		//categories+=CategoryDef*
		public Assignment getCategoriesAssignment_2() { return cCategoriesAssignment_2; }

		//CategoryDef
		public RuleCall getCategoriesCategoryDefParserRuleCall_2_0() { return cCategoriesCategoryDefParserRuleCall_2_0; }

		//nonCategorized=UncategorizedContent?
		public Assignment getNonCategorizedAssignment_3() { return cNonCategorizedAssignment_3; }

		//UncategorizedContent
		public RuleCall getNonCategorizedUncategorizedContentParserRuleCall_3_0() { return cNonCategorizedUncategorizedContentParserRuleCall_3_0; }
	}

	public class RepoNameElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "RepoName");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cRepoNameKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cUserReadableNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cUserReadableNameSTRINGTerminalRuleCall_1_0 = (RuleCall)cUserReadableNameAssignment_1.eContents().get(0);
		
		//RepoName:
		//
		//	"repoName:" userReadableName=STRING;
		public ParserRule getRule() { return rule; }

		//"repoName:" userReadableName=STRING
		public Group getGroup() { return cGroup; }

		//"repoName:"
		public Keyword getRepoNameKeyword_0() { return cRepoNameKeyword_0; }

		//userReadableName=STRING
		public Assignment getUserReadableNameAssignment_1() { return cUserReadableNameAssignment_1; }

		//STRING
		public RuleCall getUserReadableNameSTRINGTerminalRuleCall_1_0() { return cUserReadableNameSTRINGTerminalRuleCall_1_0; }
	}

	public class ReferencedRepoElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "ReferencedRepo");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cReferenceRepoKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cAssociatedRepoURLAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cAssociatedRepoURLSTRINGTerminalRuleCall_1_0 = (RuleCall)cAssociatedRepoURLAssignment_1.eContents().get(0);
		private final Keyword cAsKeyword_2 = (Keyword)cGroup.eContents().get(2);
		private final Assignment cNickNameAssignment_3 = (Assignment)cGroup.eContents().get(3);
		private final RuleCall cNickNameSTRINGTerminalRuleCall_3_0 = (RuleCall)cNickNameAssignment_3.eContents().get(0);
		private final Group cGroup_4 = (Group)cGroup.eContents().get(4);
		private final Assignment cEnablementAssignment_4_0 = (Assignment)cGroup_4.eContents().get(0);
		private final Alternatives cEnablementAlternatives_4_0_0 = (Alternatives)cEnablementAssignment_4_0.eContents().get(0);
		private final Keyword cEnablementEnabledKeyword_4_0_0_0 = (Keyword)cEnablementAlternatives_4_0_0.eContents().get(0);
		private final Keyword cEnablementDisabledKeyword_4_0_0_1 = (Keyword)cEnablementAlternatives_4_0_0.eContents().get(1);
		private final Assignment cVisibilityAssignment_4_1 = (Assignment)cGroup_4.eContents().get(1);
		private final Alternatives cVisibilityAlternatives_4_1_0 = (Alternatives)cVisibilityAssignment_4_1.eContents().get(0);
		private final Keyword cVisibilityHiddenKeyword_4_1_0_0 = (Keyword)cVisibilityAlternatives_4_1_0.eContents().get(0);
		private final Keyword cVisibilityVisibleKeyword_4_1_0_1 = (Keyword)cVisibilityAlternatives_4_1_0.eContents().get(1);
		
		//ReferencedRepo:
		//
		//	"referenceRepo:" associatedRepoURL=STRING "as" nickName=STRING (enablement=("enabled" | "disabled")
		//
		//	visibility=("hidden" | "visible"))?;
		public ParserRule getRule() { return rule; }

		//"referenceRepo:" associatedRepoURL=STRING "as" nickName=STRING (enablement=("enabled" | "disabled") visibility=("hidden"
		//
		//| "visible"))?
		public Group getGroup() { return cGroup; }

		//"referenceRepo:"
		public Keyword getReferenceRepoKeyword_0() { return cReferenceRepoKeyword_0; }

		//associatedRepoURL=STRING
		public Assignment getAssociatedRepoURLAssignment_1() { return cAssociatedRepoURLAssignment_1; }

		//STRING
		public RuleCall getAssociatedRepoURLSTRINGTerminalRuleCall_1_0() { return cAssociatedRepoURLSTRINGTerminalRuleCall_1_0; }

		//"as"
		public Keyword getAsKeyword_2() { return cAsKeyword_2; }

		//nickName=STRING
		public Assignment getNickNameAssignment_3() { return cNickNameAssignment_3; }

		//STRING
		public RuleCall getNickNameSTRINGTerminalRuleCall_3_0() { return cNickNameSTRINGTerminalRuleCall_3_0; }

		//(enablement=("enabled" | "disabled") visibility=("hidden" | "visible"))?
		public Group getGroup_4() { return cGroup_4; }

		//enablement=("enabled" | "disabled")
		public Assignment getEnablementAssignment_4_0() { return cEnablementAssignment_4_0; }

		//"enabled" | "disabled"
		public Alternatives getEnablementAlternatives_4_0_0() { return cEnablementAlternatives_4_0_0; }

		//"enabled"
		public Keyword getEnablementEnabledKeyword_4_0_0_0() { return cEnablementEnabledKeyword_4_0_0_0; }

		//"disabled"
		public Keyword getEnablementDisabledKeyword_4_0_0_1() { return cEnablementDisabledKeyword_4_0_0_1; }

		//visibility=("hidden" | "visible")
		public Assignment getVisibilityAssignment_4_1() { return cVisibilityAssignment_4_1; }

		//"hidden" | "visible"
		public Alternatives getVisibilityAlternatives_4_1_0() { return cVisibilityAlternatives_4_1_0; }

		//"hidden"
		public Keyword getVisibilityHiddenKeyword_4_1_0_0() { return cVisibilityHiddenKeyword_4_1_0_0; }

		//"visible"
		public Keyword getVisibilityVisibleKeyword_4_1_0_1() { return cVisibilityVisibleKeyword_4_1_0_1; }
	}

	public class UncategorizedContentElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "UncategorizedContent");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cUncategorizedContentKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cEntriesAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final Alternatives cEntriesAlternatives_1_0 = (Alternatives)cEntriesAssignment_1.eContents().get(0);
		private final RuleCall cEntriesFeatureParserRuleCall_1_0_0 = (RuleCall)cEntriesAlternatives_1_0.eContents().get(0);
		private final RuleCall cEntriesIuParserRuleCall_1_0_1 = (RuleCall)cEntriesAlternatives_1_0.eContents().get(1);
		private final Keyword cEndKeyword_2 = (Keyword)cGroup.eContents().get(2);
		
		//UncategorizedContent:
		//
		//	"uncategorizedContent:" entries+=(Feature | Iu)+ "end";
		public ParserRule getRule() { return rule; }

		//"uncategorizedContent:" entries+=(Feature | Iu)+ "end"
		public Group getGroup() { return cGroup; }

		//"uncategorizedContent:"
		public Keyword getUncategorizedContentKeyword_0() { return cUncategorizedContentKeyword_0; }

		//entries+=(Feature | Iu)+
		public Assignment getEntriesAssignment_1() { return cEntriesAssignment_1; }

		//Feature | Iu
		public Alternatives getEntriesAlternatives_1_0() { return cEntriesAlternatives_1_0; }

		//Feature
		public RuleCall getEntriesFeatureParserRuleCall_1_0_0() { return cEntriesFeatureParserRuleCall_1_0_0; }

		//Iu
		public RuleCall getEntriesIuParserRuleCall_1_0_1() { return cEntriesIuParserRuleCall_1_0_1; }

		//"end"
		public Keyword getEndKeyword_2() { return cEndKeyword_2; }
	}

	public class CategoryDefElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "CategoryDef");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final RuleCall cCategoryNameParserRuleCall_0 = (RuleCall)cGroup.eContents().get(0);
		private final Assignment cEntriesAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final Alternatives cEntriesAlternatives_1_0 = (Alternatives)cEntriesAssignment_1.eContents().get(0);
		private final RuleCall cEntriesFeatureParserRuleCall_1_0_0 = (RuleCall)cEntriesAlternatives_1_0.eContents().get(0);
		private final RuleCall cEntriesIuParserRuleCall_1_0_1 = (RuleCall)cEntriesAlternatives_1_0.eContents().get(1);
		private final RuleCall cEntriesCategoryDefParserRuleCall_1_0_2 = (RuleCall)cEntriesAlternatives_1_0.eContents().get(2);
		private final Keyword cEndKeyword_2 = (Keyword)cGroup.eContents().get(2);
		
		//CategoryDef:
		//
		//	CategoryName entries+=(Feature | Iu | CategoryDef)* "end";
		public ParserRule getRule() { return rule; }

		//CategoryName entries+=(Feature | Iu | CategoryDef)* "end"
		public Group getGroup() { return cGroup; }

		//CategoryName
		public RuleCall getCategoryNameParserRuleCall_0() { return cCategoryNameParserRuleCall_0; }

		//entries+=(Feature | Iu | CategoryDef)*
		public Assignment getEntriesAssignment_1() { return cEntriesAssignment_1; }

		//Feature | Iu | CategoryDef
		public Alternatives getEntriesAlternatives_1_0() { return cEntriesAlternatives_1_0; }

		//Feature
		public RuleCall getEntriesFeatureParserRuleCall_1_0_0() { return cEntriesFeatureParserRuleCall_1_0_0; }

		//Iu
		public RuleCall getEntriesIuParserRuleCall_1_0_1() { return cEntriesIuParserRuleCall_1_0_1; }

		//CategoryDef
		public RuleCall getEntriesCategoryDefParserRuleCall_1_0_2() { return cEntriesCategoryDefParserRuleCall_1_0_2; }

		//"end"
		public Keyword getEndKeyword_2() { return cEndKeyword_2; }
	}

	public class IuElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Iu");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cIuKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cIuIdAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cIuIdSTRINGTerminalRuleCall_1_0 = (RuleCall)cIuIdAssignment_1.eContents().get(0);
		private final Group cGroup_2 = (Group)cGroup.eContents().get(2);
		private final Keyword cCommaKeyword_2_0 = (Keyword)cGroup_2.eContents().get(0);
		private final Assignment cRangeAssignment_2_1 = (Assignment)cGroup_2.eContents().get(1);
		private final RuleCall cRangeVersionRangeParserRuleCall_2_1_0 = (RuleCall)cRangeAssignment_2_1.eContents().get(0);
		
		//Iu:
		//
		//	"iu:" iuId=STRING ("," range=VersionRange)?;
		public ParserRule getRule() { return rule; }

		//"iu:" iuId=STRING ("," range=VersionRange)?
		public Group getGroup() { return cGroup; }

		//"iu:"
		public Keyword getIuKeyword_0() { return cIuKeyword_0; }

		//iuId=STRING
		public Assignment getIuIdAssignment_1() { return cIuIdAssignment_1; }

		//STRING
		public RuleCall getIuIdSTRINGTerminalRuleCall_1_0() { return cIuIdSTRINGTerminalRuleCall_1_0; }

		//("," range=VersionRange)?
		public Group getGroup_2() { return cGroup_2; }

		//","
		public Keyword getCommaKeyword_2_0() { return cCommaKeyword_2_0; }

		//range=VersionRange
		public Assignment getRangeAssignment_2_1() { return cRangeAssignment_2_1; }

		//VersionRange
		public RuleCall getRangeVersionRangeParserRuleCall_2_1_0() { return cRangeVersionRangeParserRuleCall_2_1_0; }
	}

	public class VersionRangeElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "VersionRange");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cLeftSquareBracketKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cLowerRangeAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cLowerRangeSTRINGTerminalRuleCall_1_0 = (RuleCall)cLowerRangeAssignment_1.eContents().get(0);
		private final Group cGroup_2 = (Group)cGroup.eContents().get(2);
		private final Keyword cCommaKeyword_2_0 = (Keyword)cGroup_2.eContents().get(0);
		private final Assignment cUpperRangeAssignment_2_1 = (Assignment)cGroup_2.eContents().get(1);
		private final RuleCall cUpperRangeSTRINGTerminalRuleCall_2_1_0 = (RuleCall)cUpperRangeAssignment_2_1.eContents().get(0);
		private final Keyword cRightSquareBracketKeyword_3 = (Keyword)cGroup.eContents().get(3);
		
		//VersionRange:
		//
		//	"[" lowerRange=STRING ("," upperRange=STRING)? "]";
		public ParserRule getRule() { return rule; }

		//"[" lowerRange=STRING ("," upperRange=STRING)? "]"
		public Group getGroup() { return cGroup; }

		//"["
		public Keyword getLeftSquareBracketKeyword_0() { return cLeftSquareBracketKeyword_0; }

		//lowerRange=STRING
		public Assignment getLowerRangeAssignment_1() { return cLowerRangeAssignment_1; }

		//STRING
		public RuleCall getLowerRangeSTRINGTerminalRuleCall_1_0() { return cLowerRangeSTRINGTerminalRuleCall_1_0; }

		//("," upperRange=STRING)?
		public Group getGroup_2() { return cGroup_2; }

		//","
		public Keyword getCommaKeyword_2_0() { return cCommaKeyword_2_0; }

		//upperRange=STRING
		public Assignment getUpperRangeAssignment_2_1() { return cUpperRangeAssignment_2_1; }

		//STRING
		public RuleCall getUpperRangeSTRINGTerminalRuleCall_2_1_0() { return cUpperRangeSTRINGTerminalRuleCall_2_1_0; }

		//"]"
		public Keyword getRightSquareBracketKeyword_3() { return cRightSquareBracketKeyword_3; }
	}

	public class FeatureElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "Feature");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cFeatureKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cFeatureIdAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cFeatureIdSTRINGTerminalRuleCall_1_0 = (RuleCall)cFeatureIdAssignment_1.eContents().get(0);
		
		//Feature:
		//
		//	"feature:" featureId=STRING;
		public ParserRule getRule() { return rule; }

		//"feature:" featureId=STRING
		public Group getGroup() { return cGroup; }

		//"feature:"
		public Keyword getFeatureKeyword_0() { return cFeatureKeyword_0; }

		//featureId=STRING
		public Assignment getFeatureIdAssignment_1() { return cFeatureIdAssignment_1; }

		//STRING
		public RuleCall getFeatureIdSTRINGTerminalRuleCall_1_0() { return cFeatureIdSTRINGTerminalRuleCall_1_0; }
	}

	public class CategoryNameElements extends AbstractParserRuleElementFinder {
		private final ParserRule rule = (ParserRule) GrammarUtil.findRuleForName(getGrammar(), "CategoryName");
		private final Group cGroup = (Group)rule.eContents().get(1);
		private final Keyword cCategoryKeyword_0 = (Keyword)cGroup.eContents().get(0);
		private final Assignment cCategoryNameAssignment_1 = (Assignment)cGroup.eContents().get(1);
		private final RuleCall cCategoryNameSTRINGTerminalRuleCall_1_0 = (RuleCall)cCategoryNameAssignment_1.eContents().get(0);
		
		//CategoryName:
		//
		//	"category:" categoryName=STRING;
		public ParserRule getRule() { return rule; }

		//"category:" categoryName=STRING
		public Group getGroup() { return cGroup; }

		//"category:"
		public Keyword getCategoryKeyword_0() { return cCategoryKeyword_0; }

		//categoryName=STRING
		public Assignment getCategoryNameAssignment_1() { return cCategoryNameAssignment_1; }

		//STRING
		public RuleCall getCategoryNameSTRINGTerminalRuleCall_1_0() { return cCategoryNameSTRINGTerminalRuleCall_1_0; }
	}
	
	
	private ModelElements pModel;
	private RepoNameElements pRepoName;
	private ReferencedRepoElements pReferencedRepo;
	private UncategorizedContentElements pUncategorizedContent;
	private CategoryDefElements pCategoryDef;
	private IuElements pIu;
	private VersionRangeElements pVersionRange;
	private FeatureElements pFeature;
	private CategoryNameElements pCategoryName;
	
	private final Grammar grammar;

	private TerminalsGrammarAccess gaTerminals;

	@Inject
	public RepoDSLGrammarAccess(GrammarProvider grammarProvider,
		TerminalsGrammarAccess gaTerminals) {
		this.grammar = internalFindGrammar(grammarProvider);
		this.gaTerminals = gaTerminals;
	}
	
	protected Grammar internalFindGrammar(GrammarProvider grammarProvider) {
		Grammar grammar = grammarProvider.getGrammar(this);
		while (grammar != null) {
			if ("com.rapicorp.p2.RepoDSL".equals(grammar.getName())) {
				return grammar;
			}
			List<Grammar> grammars = grammar.getUsedGrammars();
			if (!grammars.isEmpty()) {
				grammar = grammars.iterator().next();
			} else {
				return null;
			}
		}
		return grammar;
	}
	
	
	public Grammar getGrammar() {
		return grammar;
	}
	

	public TerminalsGrammarAccess getTerminalsGrammarAccess() {
		return gaTerminals;
	}

	
	////To add
	//
	////referencedRepos vs associatedRepos
	//
	////inclusion control (only IU, sliced, resolved)
	//
	////stats tracker URL
	//
	////mirror list URL
	//
	////better p2 QL
	//
	//Model:
	//
	//	repoName=RepoName? associatedRepos+=ReferencedRepo* categories+=CategoryDef* nonCategorized=UncategorizedContent?;
	public ModelElements getModelAccess() {
		return (pModel != null) ? pModel : (pModel = new ModelElements());
	}
	
	public ParserRule getModelRule() {
		return getModelAccess().getRule();
	}

	//RepoName:
	//
	//	"repoName:" userReadableName=STRING;
	public RepoNameElements getRepoNameAccess() {
		return (pRepoName != null) ? pRepoName : (pRepoName = new RepoNameElements());
	}
	
	public ParserRule getRepoNameRule() {
		return getRepoNameAccess().getRule();
	}

	//ReferencedRepo:
	//
	//	"referenceRepo:" associatedRepoURL=STRING "as" nickName=STRING (enablement=("enabled" | "disabled")
	//
	//	visibility=("hidden" | "visible"))?;
	public ReferencedRepoElements getReferencedRepoAccess() {
		return (pReferencedRepo != null) ? pReferencedRepo : (pReferencedRepo = new ReferencedRepoElements());
	}
	
	public ParserRule getReferencedRepoRule() {
		return getReferencedRepoAccess().getRule();
	}

	//UncategorizedContent:
	//
	//	"uncategorizedContent:" entries+=(Feature | Iu)+ "end";
	public UncategorizedContentElements getUncategorizedContentAccess() {
		return (pUncategorizedContent != null) ? pUncategorizedContent : (pUncategorizedContent = new UncategorizedContentElements());
	}
	
	public ParserRule getUncategorizedContentRule() {
		return getUncategorizedContentAccess().getRule();
	}

	//CategoryDef:
	//
	//	CategoryName entries+=(Feature | Iu | CategoryDef)* "end";
	public CategoryDefElements getCategoryDefAccess() {
		return (pCategoryDef != null) ? pCategoryDef : (pCategoryDef = new CategoryDefElements());
	}
	
	public ParserRule getCategoryDefRule() {
		return getCategoryDefAccess().getRule();
	}

	//Iu:
	//
	//	"iu:" iuId=STRING ("," range=VersionRange)?;
	public IuElements getIuAccess() {
		return (pIu != null) ? pIu : (pIu = new IuElements());
	}
	
	public ParserRule getIuRule() {
		return getIuAccess().getRule();
	}

	//VersionRange:
	//
	//	"[" lowerRange=STRING ("," upperRange=STRING)? "]";
	public VersionRangeElements getVersionRangeAccess() {
		return (pVersionRange != null) ? pVersionRange : (pVersionRange = new VersionRangeElements());
	}
	
	public ParserRule getVersionRangeRule() {
		return getVersionRangeAccess().getRule();
	}

	//Feature:
	//
	//	"feature:" featureId=STRING;
	public FeatureElements getFeatureAccess() {
		return (pFeature != null) ? pFeature : (pFeature = new FeatureElements());
	}
	
	public ParserRule getFeatureRule() {
		return getFeatureAccess().getRule();
	}

	//CategoryName:
	//
	//	"category:" categoryName=STRING;
	public CategoryNameElements getCategoryNameAccess() {
		return (pCategoryName != null) ? pCategoryName : (pCategoryName = new CategoryNameElements());
	}
	
	public ParserRule getCategoryNameRule() {
		return getCategoryNameAccess().getRule();
	}

	//terminal ID:
	//
	//	"^"? ("a".."z" | "A".."Z" | "_") ("a".."z" | "A".."Z" | "_" | "0".."9")*;
	public TerminalRule getIDRule() {
		return gaTerminals.getIDRule();
	} 

	//terminal INT returns ecore::EInt:
	//
	//	"0".."9"+;
	public TerminalRule getINTRule() {
		return gaTerminals.getINTRule();
	} 

	//terminal STRING:
	//
	//	"\"" ("\\" ("b" | "t" | "n" | "f" | "r" | "u" | "\"" | "\'" | "\\") | !("\\" | "\""))* "\"" | "\'" ("\\" ("b" | "t" |
	//
	//	"n" | "f" | "r" | "u" | "\"" | "\'" | "\\") | !("\\" | "\'"))* "\'";
	public TerminalRule getSTRINGRule() {
		return gaTerminals.getSTRINGRule();
	} 

	//terminal ML_COMMENT:
	//
	//	"/ *"->"* /";
	public TerminalRule getML_COMMENTRule() {
		return gaTerminals.getML_COMMENTRule();
	} 

	//terminal SL_COMMENT:
	//
	//	"//" !("\n" | "\r")* ("\r"? "\n")?;
	public TerminalRule getSL_COMMENTRule() {
		return gaTerminals.getSL_COMMENTRule();
	} 

	//terminal WS:
	//
	//	(" " | "\t" | "\r" | "\n")+;
	public TerminalRule getWSRule() {
		return gaTerminals.getWSRule();
	} 

	//terminal ANY_OTHER:
	//
	//	.;
	public TerminalRule getANY_OTHERRule() {
		return gaTerminals.getANY_OTHERRule();
	} 
}
