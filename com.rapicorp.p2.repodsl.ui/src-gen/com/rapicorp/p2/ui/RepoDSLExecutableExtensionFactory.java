/*
 * generated by Xtext
 */
package com.rapicorp.p2.ui;

import org.eclipse.xtext.ui.guice.AbstractGuiceAwareExecutableExtensionFactory;
import org.osgi.framework.Bundle;

import com.google.inject.Injector;

import com.rapicorp.p2.ui.internal.RepoDSLActivator;

/**
 * This class was generated. Customizations should only happen in a newly
 * introduced subclass. 
 */
public class RepoDSLExecutableExtensionFactory extends AbstractGuiceAwareExecutableExtensionFactory {

	@Override
	protected Bundle getBundle() {
		return RepoDSLActivator.getInstance().getBundle();
	}
	
	@Override
	protected Injector getInjector() {
		return RepoDSLActivator.getInstance().getInjector(RepoDSLActivator.COM_RAPICORP_P2_REPODSL);
	}
	
}
