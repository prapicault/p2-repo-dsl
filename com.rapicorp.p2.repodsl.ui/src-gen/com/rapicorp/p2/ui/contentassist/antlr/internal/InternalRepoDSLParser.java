package com.rapicorp.p2.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import com.rapicorp.p2.services.RepoDSLGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalRepoDSLParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_STRING", "RULE_ID", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'enabled'", "'disabled'", "'hidden'", "'visible'", "'repoName:'", "'referenceRepo:'", "'as'", "'uncategorizedContent:'", "'end'", "'iu:'", "','", "'['", "']'", "'feature:'", "'category:'"
    };
    public static final int RULE_ID=5;
    public static final int T__25=25;
    public static final int T__24=24;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=4;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalRepoDSLParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalRepoDSLParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalRepoDSLParser.tokenNames; }
    public String getGrammarFileName() { return "../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g"; }


     
     	private RepoDSLGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(RepoDSLGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleModel"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:60:1: entryRuleModel : ruleModel EOF ;
    public final void entryRuleModel() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:61:1: ( ruleModel EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:62:1: ruleModel EOF
            {
             before(grammarAccess.getModelRule()); 
            pushFollow(FOLLOW_ruleModel_in_entryRuleModel61);
            ruleModel();

            state._fsp--;

             after(grammarAccess.getModelRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleModel68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:69:1: ruleModel : ( ( rule__Model__Group__0 ) ) ;
    public final void ruleModel() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:73:2: ( ( ( rule__Model__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:74:1: ( ( rule__Model__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:74:1: ( ( rule__Model__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:75:1: ( rule__Model__Group__0 )
            {
             before(grammarAccess.getModelAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:76:1: ( rule__Model__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:76:2: rule__Model__Group__0
            {
            pushFollow(FOLLOW_rule__Model__Group__0_in_ruleModel94);
            rule__Model__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleRepoName"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:88:1: entryRuleRepoName : ruleRepoName EOF ;
    public final void entryRuleRepoName() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:89:1: ( ruleRepoName EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:90:1: ruleRepoName EOF
            {
             before(grammarAccess.getRepoNameRule()); 
            pushFollow(FOLLOW_ruleRepoName_in_entryRuleRepoName121);
            ruleRepoName();

            state._fsp--;

             after(grammarAccess.getRepoNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRepoName128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRepoName"


    // $ANTLR start "ruleRepoName"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:97:1: ruleRepoName : ( ( rule__RepoName__Group__0 ) ) ;
    public final void ruleRepoName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:101:2: ( ( ( rule__RepoName__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:102:1: ( ( rule__RepoName__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:102:1: ( ( rule__RepoName__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:103:1: ( rule__RepoName__Group__0 )
            {
             before(grammarAccess.getRepoNameAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:104:1: ( rule__RepoName__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:104:2: rule__RepoName__Group__0
            {
            pushFollow(FOLLOW_rule__RepoName__Group__0_in_ruleRepoName154);
            rule__RepoName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRepoNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRepoName"


    // $ANTLR start "entryRuleReferencedRepo"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:116:1: entryRuleReferencedRepo : ruleReferencedRepo EOF ;
    public final void entryRuleReferencedRepo() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:117:1: ( ruleReferencedRepo EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:118:1: ruleReferencedRepo EOF
            {
             before(grammarAccess.getReferencedRepoRule()); 
            pushFollow(FOLLOW_ruleReferencedRepo_in_entryRuleReferencedRepo181);
            ruleReferencedRepo();

            state._fsp--;

             after(grammarAccess.getReferencedRepoRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleReferencedRepo188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleReferencedRepo"


    // $ANTLR start "ruleReferencedRepo"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:125:1: ruleReferencedRepo : ( ( rule__ReferencedRepo__Group__0 ) ) ;
    public final void ruleReferencedRepo() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:129:2: ( ( ( rule__ReferencedRepo__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:130:1: ( ( rule__ReferencedRepo__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:130:1: ( ( rule__ReferencedRepo__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:131:1: ( rule__ReferencedRepo__Group__0 )
            {
             before(grammarAccess.getReferencedRepoAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:132:1: ( rule__ReferencedRepo__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:132:2: rule__ReferencedRepo__Group__0
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__Group__0_in_ruleReferencedRepo214);
            rule__ReferencedRepo__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getReferencedRepoAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleReferencedRepo"


    // $ANTLR start "entryRuleUncategorizedContent"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:144:1: entryRuleUncategorizedContent : ruleUncategorizedContent EOF ;
    public final void entryRuleUncategorizedContent() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:145:1: ( ruleUncategorizedContent EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:146:1: ruleUncategorizedContent EOF
            {
             before(grammarAccess.getUncategorizedContentRule()); 
            pushFollow(FOLLOW_ruleUncategorizedContent_in_entryRuleUncategorizedContent241);
            ruleUncategorizedContent();

            state._fsp--;

             after(grammarAccess.getUncategorizedContentRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUncategorizedContent248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUncategorizedContent"


    // $ANTLR start "ruleUncategorizedContent"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:153:1: ruleUncategorizedContent : ( ( rule__UncategorizedContent__Group__0 ) ) ;
    public final void ruleUncategorizedContent() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:157:2: ( ( ( rule__UncategorizedContent__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:158:1: ( ( rule__UncategorizedContent__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:158:1: ( ( rule__UncategorizedContent__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:159:1: ( rule__UncategorizedContent__Group__0 )
            {
             before(grammarAccess.getUncategorizedContentAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:160:1: ( rule__UncategorizedContent__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:160:2: rule__UncategorizedContent__Group__0
            {
            pushFollow(FOLLOW_rule__UncategorizedContent__Group__0_in_ruleUncategorizedContent274);
            rule__UncategorizedContent__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUncategorizedContentAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUncategorizedContent"


    // $ANTLR start "entryRuleCategoryDef"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:172:1: entryRuleCategoryDef : ruleCategoryDef EOF ;
    public final void entryRuleCategoryDef() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:173:1: ( ruleCategoryDef EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:174:1: ruleCategoryDef EOF
            {
             before(grammarAccess.getCategoryDefRule()); 
            pushFollow(FOLLOW_ruleCategoryDef_in_entryRuleCategoryDef301);
            ruleCategoryDef();

            state._fsp--;

             after(grammarAccess.getCategoryDefRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCategoryDef308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCategoryDef"


    // $ANTLR start "ruleCategoryDef"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:181:1: ruleCategoryDef : ( ( rule__CategoryDef__Group__0 ) ) ;
    public final void ruleCategoryDef() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:185:2: ( ( ( rule__CategoryDef__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:186:1: ( ( rule__CategoryDef__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:186:1: ( ( rule__CategoryDef__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:187:1: ( rule__CategoryDef__Group__0 )
            {
             before(grammarAccess.getCategoryDefAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:188:1: ( rule__CategoryDef__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:188:2: rule__CategoryDef__Group__0
            {
            pushFollow(FOLLOW_rule__CategoryDef__Group__0_in_ruleCategoryDef334);
            rule__CategoryDef__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryDefAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCategoryDef"


    // $ANTLR start "entryRuleIu"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:200:1: entryRuleIu : ruleIu EOF ;
    public final void entryRuleIu() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:201:1: ( ruleIu EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:202:1: ruleIu EOF
            {
             before(grammarAccess.getIuRule()); 
            pushFollow(FOLLOW_ruleIu_in_entryRuleIu361);
            ruleIu();

            state._fsp--;

             after(grammarAccess.getIuRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleIu368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleIu"


    // $ANTLR start "ruleIu"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:209:1: ruleIu : ( ( rule__Iu__Group__0 ) ) ;
    public final void ruleIu() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:213:2: ( ( ( rule__Iu__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:214:1: ( ( rule__Iu__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:214:1: ( ( rule__Iu__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:215:1: ( rule__Iu__Group__0 )
            {
             before(grammarAccess.getIuAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:216:1: ( rule__Iu__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:216:2: rule__Iu__Group__0
            {
            pushFollow(FOLLOW_rule__Iu__Group__0_in_ruleIu394);
            rule__Iu__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getIuAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleIu"


    // $ANTLR start "entryRuleVersionRange"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:228:1: entryRuleVersionRange : ruleVersionRange EOF ;
    public final void entryRuleVersionRange() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:229:1: ( ruleVersionRange EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:230:1: ruleVersionRange EOF
            {
             before(grammarAccess.getVersionRangeRule()); 
            pushFollow(FOLLOW_ruleVersionRange_in_entryRuleVersionRange421);
            ruleVersionRange();

            state._fsp--;

             after(grammarAccess.getVersionRangeRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleVersionRange428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleVersionRange"


    // $ANTLR start "ruleVersionRange"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:237:1: ruleVersionRange : ( ( rule__VersionRange__Group__0 ) ) ;
    public final void ruleVersionRange() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:241:2: ( ( ( rule__VersionRange__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:242:1: ( ( rule__VersionRange__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:242:1: ( ( rule__VersionRange__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:243:1: ( rule__VersionRange__Group__0 )
            {
             before(grammarAccess.getVersionRangeAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:244:1: ( rule__VersionRange__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:244:2: rule__VersionRange__Group__0
            {
            pushFollow(FOLLOW_rule__VersionRange__Group__0_in_ruleVersionRange454);
            rule__VersionRange__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getVersionRangeAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleVersionRange"


    // $ANTLR start "entryRuleFeature"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:256:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:257:1: ( ruleFeature EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:258:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_ruleFeature_in_entryRuleFeature481);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFeature488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:265:1: ruleFeature : ( ( rule__Feature__Group__0 ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:269:2: ( ( ( rule__Feature__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:270:1: ( ( rule__Feature__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:270:1: ( ( rule__Feature__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:271:1: ( rule__Feature__Group__0 )
            {
             before(grammarAccess.getFeatureAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:272:1: ( rule__Feature__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:272:2: rule__Feature__Group__0
            {
            pushFollow(FOLLOW_rule__Feature__Group__0_in_ruleFeature514);
            rule__Feature__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleCategoryName"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:284:1: entryRuleCategoryName : ruleCategoryName EOF ;
    public final void entryRuleCategoryName() throws RecognitionException {
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:285:1: ( ruleCategoryName EOF )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:286:1: ruleCategoryName EOF
            {
             before(grammarAccess.getCategoryNameRule()); 
            pushFollow(FOLLOW_ruleCategoryName_in_entryRuleCategoryName541);
            ruleCategoryName();

            state._fsp--;

             after(grammarAccess.getCategoryNameRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCategoryName548); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCategoryName"


    // $ANTLR start "ruleCategoryName"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:293:1: ruleCategoryName : ( ( rule__CategoryName__Group__0 ) ) ;
    public final void ruleCategoryName() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:297:2: ( ( ( rule__CategoryName__Group__0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:298:1: ( ( rule__CategoryName__Group__0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:298:1: ( ( rule__CategoryName__Group__0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:299:1: ( rule__CategoryName__Group__0 )
            {
             before(grammarAccess.getCategoryNameAccess().getGroup()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:300:1: ( rule__CategoryName__Group__0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:300:2: rule__CategoryName__Group__0
            {
            pushFollow(FOLLOW_rule__CategoryName__Group__0_in_ruleCategoryName574);
            rule__CategoryName__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryNameAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCategoryName"


    // $ANTLR start "rule__ReferencedRepo__EnablementAlternatives_4_0_0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:312:1: rule__ReferencedRepo__EnablementAlternatives_4_0_0 : ( ( 'enabled' ) | ( 'disabled' ) );
    public final void rule__ReferencedRepo__EnablementAlternatives_4_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:316:1: ( ( 'enabled' ) | ( 'disabled' ) )
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==11) ) {
                alt1=1;
            }
            else if ( (LA1_0==12) ) {
                alt1=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }
            switch (alt1) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:317:1: ( 'enabled' )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:317:1: ( 'enabled' )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:318:1: 'enabled'
                    {
                     before(grammarAccess.getReferencedRepoAccess().getEnablementEnabledKeyword_4_0_0_0()); 
                    match(input,11,FOLLOW_11_in_rule__ReferencedRepo__EnablementAlternatives_4_0_0611); 
                     after(grammarAccess.getReferencedRepoAccess().getEnablementEnabledKeyword_4_0_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:325:6: ( 'disabled' )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:325:6: ( 'disabled' )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:326:1: 'disabled'
                    {
                     before(grammarAccess.getReferencedRepoAccess().getEnablementDisabledKeyword_4_0_0_1()); 
                    match(input,12,FOLLOW_12_in_rule__ReferencedRepo__EnablementAlternatives_4_0_0631); 
                     after(grammarAccess.getReferencedRepoAccess().getEnablementDisabledKeyword_4_0_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__EnablementAlternatives_4_0_0"


    // $ANTLR start "rule__ReferencedRepo__VisibilityAlternatives_4_1_0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:338:1: rule__ReferencedRepo__VisibilityAlternatives_4_1_0 : ( ( 'hidden' ) | ( 'visible' ) );
    public final void rule__ReferencedRepo__VisibilityAlternatives_4_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:342:1: ( ( 'hidden' ) | ( 'visible' ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==13) ) {
                alt2=1;
            }
            else if ( (LA2_0==14) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:343:1: ( 'hidden' )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:343:1: ( 'hidden' )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:344:1: 'hidden'
                    {
                     before(grammarAccess.getReferencedRepoAccess().getVisibilityHiddenKeyword_4_1_0_0()); 
                    match(input,13,FOLLOW_13_in_rule__ReferencedRepo__VisibilityAlternatives_4_1_0666); 
                     after(grammarAccess.getReferencedRepoAccess().getVisibilityHiddenKeyword_4_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:351:6: ( 'visible' )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:351:6: ( 'visible' )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:352:1: 'visible'
                    {
                     before(grammarAccess.getReferencedRepoAccess().getVisibilityVisibleKeyword_4_1_0_1()); 
                    match(input,14,FOLLOW_14_in_rule__ReferencedRepo__VisibilityAlternatives_4_1_0686); 
                     after(grammarAccess.getReferencedRepoAccess().getVisibilityVisibleKeyword_4_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__VisibilityAlternatives_4_1_0"


    // $ANTLR start "rule__UncategorizedContent__EntriesAlternatives_1_0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:364:1: rule__UncategorizedContent__EntriesAlternatives_1_0 : ( ( ruleFeature ) | ( ruleIu ) );
    public final void rule__UncategorizedContent__EntriesAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:368:1: ( ( ruleFeature ) | ( ruleIu ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==24) ) {
                alt3=1;
            }
            else if ( (LA3_0==20) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:369:1: ( ruleFeature )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:369:1: ( ruleFeature )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:370:1: ruleFeature
                    {
                     before(grammarAccess.getUncategorizedContentAccess().getEntriesFeatureParserRuleCall_1_0_0()); 
                    pushFollow(FOLLOW_ruleFeature_in_rule__UncategorizedContent__EntriesAlternatives_1_0720);
                    ruleFeature();

                    state._fsp--;

                     after(grammarAccess.getUncategorizedContentAccess().getEntriesFeatureParserRuleCall_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:375:6: ( ruleIu )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:375:6: ( ruleIu )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:376:1: ruleIu
                    {
                     before(grammarAccess.getUncategorizedContentAccess().getEntriesIuParserRuleCall_1_0_1()); 
                    pushFollow(FOLLOW_ruleIu_in_rule__UncategorizedContent__EntriesAlternatives_1_0737);
                    ruleIu();

                    state._fsp--;

                     after(grammarAccess.getUncategorizedContentAccess().getEntriesIuParserRuleCall_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UncategorizedContent__EntriesAlternatives_1_0"


    // $ANTLR start "rule__CategoryDef__EntriesAlternatives_1_0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:386:1: rule__CategoryDef__EntriesAlternatives_1_0 : ( ( ruleFeature ) | ( ruleIu ) | ( ruleCategoryDef ) );
    public final void rule__CategoryDef__EntriesAlternatives_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:390:1: ( ( ruleFeature ) | ( ruleIu ) | ( ruleCategoryDef ) )
            int alt4=3;
            switch ( input.LA(1) ) {
            case 24:
                {
                alt4=1;
                }
                break;
            case 20:
                {
                alt4=2;
                }
                break;
            case 25:
                {
                alt4=3;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }

            switch (alt4) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:391:1: ( ruleFeature )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:391:1: ( ruleFeature )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:392:1: ruleFeature
                    {
                     before(grammarAccess.getCategoryDefAccess().getEntriesFeatureParserRuleCall_1_0_0()); 
                    pushFollow(FOLLOW_ruleFeature_in_rule__CategoryDef__EntriesAlternatives_1_0769);
                    ruleFeature();

                    state._fsp--;

                     after(grammarAccess.getCategoryDefAccess().getEntriesFeatureParserRuleCall_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:397:6: ( ruleIu )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:397:6: ( ruleIu )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:398:1: ruleIu
                    {
                     before(grammarAccess.getCategoryDefAccess().getEntriesIuParserRuleCall_1_0_1()); 
                    pushFollow(FOLLOW_ruleIu_in_rule__CategoryDef__EntriesAlternatives_1_0786);
                    ruleIu();

                    state._fsp--;

                     after(grammarAccess.getCategoryDefAccess().getEntriesIuParserRuleCall_1_0_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:403:6: ( ruleCategoryDef )
                    {
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:403:6: ( ruleCategoryDef )
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:404:1: ruleCategoryDef
                    {
                     before(grammarAccess.getCategoryDefAccess().getEntriesCategoryDefParserRuleCall_1_0_2()); 
                    pushFollow(FOLLOW_ruleCategoryDef_in_rule__CategoryDef__EntriesAlternatives_1_0803);
                    ruleCategoryDef();

                    state._fsp--;

                     after(grammarAccess.getCategoryDefAccess().getEntriesCategoryDefParserRuleCall_1_0_2()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryDef__EntriesAlternatives_1_0"


    // $ANTLR start "rule__Model__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:416:1: rule__Model__Group__0 : rule__Model__Group__0__Impl rule__Model__Group__1 ;
    public final void rule__Model__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:420:1: ( rule__Model__Group__0__Impl rule__Model__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:421:2: rule__Model__Group__0__Impl rule__Model__Group__1
            {
            pushFollow(FOLLOW_rule__Model__Group__0__Impl_in_rule__Model__Group__0833);
            rule__Model__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Model__Group__1_in_rule__Model__Group__0836);
            rule__Model__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0"


    // $ANTLR start "rule__Model__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:428:1: rule__Model__Group__0__Impl : ( ( rule__Model__RepoNameAssignment_0 )? ) ;
    public final void rule__Model__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:432:1: ( ( ( rule__Model__RepoNameAssignment_0 )? ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:433:1: ( ( rule__Model__RepoNameAssignment_0 )? )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:433:1: ( ( rule__Model__RepoNameAssignment_0 )? )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:434:1: ( rule__Model__RepoNameAssignment_0 )?
            {
             before(grammarAccess.getModelAccess().getRepoNameAssignment_0()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:435:1: ( rule__Model__RepoNameAssignment_0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==15) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:435:2: rule__Model__RepoNameAssignment_0
                    {
                    pushFollow(FOLLOW_rule__Model__RepoNameAssignment_0_in_rule__Model__Group__0__Impl863);
                    rule__Model__RepoNameAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getModelAccess().getRepoNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__0__Impl"


    // $ANTLR start "rule__Model__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:445:1: rule__Model__Group__1 : rule__Model__Group__1__Impl rule__Model__Group__2 ;
    public final void rule__Model__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:449:1: ( rule__Model__Group__1__Impl rule__Model__Group__2 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:450:2: rule__Model__Group__1__Impl rule__Model__Group__2
            {
            pushFollow(FOLLOW_rule__Model__Group__1__Impl_in_rule__Model__Group__1894);
            rule__Model__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Model__Group__2_in_rule__Model__Group__1897);
            rule__Model__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1"


    // $ANTLR start "rule__Model__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:457:1: rule__Model__Group__1__Impl : ( ( rule__Model__AssociatedReposAssignment_1 )* ) ;
    public final void rule__Model__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:461:1: ( ( ( rule__Model__AssociatedReposAssignment_1 )* ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:462:1: ( ( rule__Model__AssociatedReposAssignment_1 )* )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:462:1: ( ( rule__Model__AssociatedReposAssignment_1 )* )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:463:1: ( rule__Model__AssociatedReposAssignment_1 )*
            {
             before(grammarAccess.getModelAccess().getAssociatedReposAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:464:1: ( rule__Model__AssociatedReposAssignment_1 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==16) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:464:2: rule__Model__AssociatedReposAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__Model__AssociatedReposAssignment_1_in_rule__Model__Group__1__Impl924);
            	    rule__Model__AssociatedReposAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getAssociatedReposAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__1__Impl"


    // $ANTLR start "rule__Model__Group__2"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:474:1: rule__Model__Group__2 : rule__Model__Group__2__Impl rule__Model__Group__3 ;
    public final void rule__Model__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:478:1: ( rule__Model__Group__2__Impl rule__Model__Group__3 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:479:2: rule__Model__Group__2__Impl rule__Model__Group__3
            {
            pushFollow(FOLLOW_rule__Model__Group__2__Impl_in_rule__Model__Group__2955);
            rule__Model__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Model__Group__3_in_rule__Model__Group__2958);
            rule__Model__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2"


    // $ANTLR start "rule__Model__Group__2__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:486:1: rule__Model__Group__2__Impl : ( ( rule__Model__CategoriesAssignment_2 )* ) ;
    public final void rule__Model__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:490:1: ( ( ( rule__Model__CategoriesAssignment_2 )* ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:491:1: ( ( rule__Model__CategoriesAssignment_2 )* )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:491:1: ( ( rule__Model__CategoriesAssignment_2 )* )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:492:1: ( rule__Model__CategoriesAssignment_2 )*
            {
             before(grammarAccess.getModelAccess().getCategoriesAssignment_2()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:493:1: ( rule__Model__CategoriesAssignment_2 )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==25) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:493:2: rule__Model__CategoriesAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__Model__CategoriesAssignment_2_in_rule__Model__Group__2__Impl985);
            	    rule__Model__CategoriesAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

             after(grammarAccess.getModelAccess().getCategoriesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__2__Impl"


    // $ANTLR start "rule__Model__Group__3"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:503:1: rule__Model__Group__3 : rule__Model__Group__3__Impl ;
    public final void rule__Model__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:507:1: ( rule__Model__Group__3__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:508:2: rule__Model__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__Model__Group__3__Impl_in_rule__Model__Group__31016);
            rule__Model__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3"


    // $ANTLR start "rule__Model__Group__3__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:514:1: rule__Model__Group__3__Impl : ( ( rule__Model__NonCategorizedAssignment_3 )? ) ;
    public final void rule__Model__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:518:1: ( ( ( rule__Model__NonCategorizedAssignment_3 )? ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:519:1: ( ( rule__Model__NonCategorizedAssignment_3 )? )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:519:1: ( ( rule__Model__NonCategorizedAssignment_3 )? )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:520:1: ( rule__Model__NonCategorizedAssignment_3 )?
            {
             before(grammarAccess.getModelAccess().getNonCategorizedAssignment_3()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:521:1: ( rule__Model__NonCategorizedAssignment_3 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==18) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:521:2: rule__Model__NonCategorizedAssignment_3
                    {
                    pushFollow(FOLLOW_rule__Model__NonCategorizedAssignment_3_in_rule__Model__Group__3__Impl1043);
                    rule__Model__NonCategorizedAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getModelAccess().getNonCategorizedAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__Group__3__Impl"


    // $ANTLR start "rule__RepoName__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:539:1: rule__RepoName__Group__0 : rule__RepoName__Group__0__Impl rule__RepoName__Group__1 ;
    public final void rule__RepoName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:543:1: ( rule__RepoName__Group__0__Impl rule__RepoName__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:544:2: rule__RepoName__Group__0__Impl rule__RepoName__Group__1
            {
            pushFollow(FOLLOW_rule__RepoName__Group__0__Impl_in_rule__RepoName__Group__01082);
            rule__RepoName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RepoName__Group__1_in_rule__RepoName__Group__01085);
            rule__RepoName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepoName__Group__0"


    // $ANTLR start "rule__RepoName__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:551:1: rule__RepoName__Group__0__Impl : ( 'repoName:' ) ;
    public final void rule__RepoName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:555:1: ( ( 'repoName:' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:556:1: ( 'repoName:' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:556:1: ( 'repoName:' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:557:1: 'repoName:'
            {
             before(grammarAccess.getRepoNameAccess().getRepoNameKeyword_0()); 
            match(input,15,FOLLOW_15_in_rule__RepoName__Group__0__Impl1113); 
             after(grammarAccess.getRepoNameAccess().getRepoNameKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepoName__Group__0__Impl"


    // $ANTLR start "rule__RepoName__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:570:1: rule__RepoName__Group__1 : rule__RepoName__Group__1__Impl ;
    public final void rule__RepoName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:574:1: ( rule__RepoName__Group__1__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:575:2: rule__RepoName__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__RepoName__Group__1__Impl_in_rule__RepoName__Group__11144);
            rule__RepoName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepoName__Group__1"


    // $ANTLR start "rule__RepoName__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:581:1: rule__RepoName__Group__1__Impl : ( ( rule__RepoName__UserReadableNameAssignment_1 ) ) ;
    public final void rule__RepoName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:585:1: ( ( ( rule__RepoName__UserReadableNameAssignment_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:586:1: ( ( rule__RepoName__UserReadableNameAssignment_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:586:1: ( ( rule__RepoName__UserReadableNameAssignment_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:587:1: ( rule__RepoName__UserReadableNameAssignment_1 )
            {
             before(grammarAccess.getRepoNameAccess().getUserReadableNameAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:588:1: ( rule__RepoName__UserReadableNameAssignment_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:588:2: rule__RepoName__UserReadableNameAssignment_1
            {
            pushFollow(FOLLOW_rule__RepoName__UserReadableNameAssignment_1_in_rule__RepoName__Group__1__Impl1171);
            rule__RepoName__UserReadableNameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRepoNameAccess().getUserReadableNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepoName__Group__1__Impl"


    // $ANTLR start "rule__ReferencedRepo__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:602:1: rule__ReferencedRepo__Group__0 : rule__ReferencedRepo__Group__0__Impl rule__ReferencedRepo__Group__1 ;
    public final void rule__ReferencedRepo__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:606:1: ( rule__ReferencedRepo__Group__0__Impl rule__ReferencedRepo__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:607:2: rule__ReferencedRepo__Group__0__Impl rule__ReferencedRepo__Group__1
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__Group__0__Impl_in_rule__ReferencedRepo__Group__01205);
            rule__ReferencedRepo__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReferencedRepo__Group__1_in_rule__ReferencedRepo__Group__01208);
            rule__ReferencedRepo__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__0"


    // $ANTLR start "rule__ReferencedRepo__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:614:1: rule__ReferencedRepo__Group__0__Impl : ( 'referenceRepo:' ) ;
    public final void rule__ReferencedRepo__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:618:1: ( ( 'referenceRepo:' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:619:1: ( 'referenceRepo:' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:619:1: ( 'referenceRepo:' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:620:1: 'referenceRepo:'
            {
             before(grammarAccess.getReferencedRepoAccess().getReferenceRepoKeyword_0()); 
            match(input,16,FOLLOW_16_in_rule__ReferencedRepo__Group__0__Impl1236); 
             after(grammarAccess.getReferencedRepoAccess().getReferenceRepoKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__0__Impl"


    // $ANTLR start "rule__ReferencedRepo__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:633:1: rule__ReferencedRepo__Group__1 : rule__ReferencedRepo__Group__1__Impl rule__ReferencedRepo__Group__2 ;
    public final void rule__ReferencedRepo__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:637:1: ( rule__ReferencedRepo__Group__1__Impl rule__ReferencedRepo__Group__2 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:638:2: rule__ReferencedRepo__Group__1__Impl rule__ReferencedRepo__Group__2
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__Group__1__Impl_in_rule__ReferencedRepo__Group__11267);
            rule__ReferencedRepo__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReferencedRepo__Group__2_in_rule__ReferencedRepo__Group__11270);
            rule__ReferencedRepo__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__1"


    // $ANTLR start "rule__ReferencedRepo__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:645:1: rule__ReferencedRepo__Group__1__Impl : ( ( rule__ReferencedRepo__AssociatedRepoURLAssignment_1 ) ) ;
    public final void rule__ReferencedRepo__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:649:1: ( ( ( rule__ReferencedRepo__AssociatedRepoURLAssignment_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:650:1: ( ( rule__ReferencedRepo__AssociatedRepoURLAssignment_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:650:1: ( ( rule__ReferencedRepo__AssociatedRepoURLAssignment_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:651:1: ( rule__ReferencedRepo__AssociatedRepoURLAssignment_1 )
            {
             before(grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:652:1: ( rule__ReferencedRepo__AssociatedRepoURLAssignment_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:652:2: rule__ReferencedRepo__AssociatedRepoURLAssignment_1
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__AssociatedRepoURLAssignment_1_in_rule__ReferencedRepo__Group__1__Impl1297);
            rule__ReferencedRepo__AssociatedRepoURLAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__1__Impl"


    // $ANTLR start "rule__ReferencedRepo__Group__2"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:662:1: rule__ReferencedRepo__Group__2 : rule__ReferencedRepo__Group__2__Impl rule__ReferencedRepo__Group__3 ;
    public final void rule__ReferencedRepo__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:666:1: ( rule__ReferencedRepo__Group__2__Impl rule__ReferencedRepo__Group__3 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:667:2: rule__ReferencedRepo__Group__2__Impl rule__ReferencedRepo__Group__3
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__Group__2__Impl_in_rule__ReferencedRepo__Group__21327);
            rule__ReferencedRepo__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReferencedRepo__Group__3_in_rule__ReferencedRepo__Group__21330);
            rule__ReferencedRepo__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__2"


    // $ANTLR start "rule__ReferencedRepo__Group__2__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:674:1: rule__ReferencedRepo__Group__2__Impl : ( 'as' ) ;
    public final void rule__ReferencedRepo__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:678:1: ( ( 'as' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:679:1: ( 'as' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:679:1: ( 'as' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:680:1: 'as'
            {
             before(grammarAccess.getReferencedRepoAccess().getAsKeyword_2()); 
            match(input,17,FOLLOW_17_in_rule__ReferencedRepo__Group__2__Impl1358); 
             after(grammarAccess.getReferencedRepoAccess().getAsKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__2__Impl"


    // $ANTLR start "rule__ReferencedRepo__Group__3"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:693:1: rule__ReferencedRepo__Group__3 : rule__ReferencedRepo__Group__3__Impl rule__ReferencedRepo__Group__4 ;
    public final void rule__ReferencedRepo__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:697:1: ( rule__ReferencedRepo__Group__3__Impl rule__ReferencedRepo__Group__4 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:698:2: rule__ReferencedRepo__Group__3__Impl rule__ReferencedRepo__Group__4
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__Group__3__Impl_in_rule__ReferencedRepo__Group__31389);
            rule__ReferencedRepo__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReferencedRepo__Group__4_in_rule__ReferencedRepo__Group__31392);
            rule__ReferencedRepo__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__3"


    // $ANTLR start "rule__ReferencedRepo__Group__3__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:705:1: rule__ReferencedRepo__Group__3__Impl : ( ( rule__ReferencedRepo__NickNameAssignment_3 ) ) ;
    public final void rule__ReferencedRepo__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:709:1: ( ( ( rule__ReferencedRepo__NickNameAssignment_3 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:710:1: ( ( rule__ReferencedRepo__NickNameAssignment_3 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:710:1: ( ( rule__ReferencedRepo__NickNameAssignment_3 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:711:1: ( rule__ReferencedRepo__NickNameAssignment_3 )
            {
             before(grammarAccess.getReferencedRepoAccess().getNickNameAssignment_3()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:712:1: ( rule__ReferencedRepo__NickNameAssignment_3 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:712:2: rule__ReferencedRepo__NickNameAssignment_3
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__NickNameAssignment_3_in_rule__ReferencedRepo__Group__3__Impl1419);
            rule__ReferencedRepo__NickNameAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getReferencedRepoAccess().getNickNameAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__3__Impl"


    // $ANTLR start "rule__ReferencedRepo__Group__4"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:722:1: rule__ReferencedRepo__Group__4 : rule__ReferencedRepo__Group__4__Impl ;
    public final void rule__ReferencedRepo__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:726:1: ( rule__ReferencedRepo__Group__4__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:727:2: rule__ReferencedRepo__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__Group__4__Impl_in_rule__ReferencedRepo__Group__41449);
            rule__ReferencedRepo__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__4"


    // $ANTLR start "rule__ReferencedRepo__Group__4__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:733:1: rule__ReferencedRepo__Group__4__Impl : ( ( rule__ReferencedRepo__Group_4__0 )? ) ;
    public final void rule__ReferencedRepo__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:737:1: ( ( ( rule__ReferencedRepo__Group_4__0 )? ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:738:1: ( ( rule__ReferencedRepo__Group_4__0 )? )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:738:1: ( ( rule__ReferencedRepo__Group_4__0 )? )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:739:1: ( rule__ReferencedRepo__Group_4__0 )?
            {
             before(grammarAccess.getReferencedRepoAccess().getGroup_4()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:740:1: ( rule__ReferencedRepo__Group_4__0 )?
            int alt9=2;
            int LA9_0 = input.LA(1);

            if ( ((LA9_0>=11 && LA9_0<=12)) ) {
                alt9=1;
            }
            switch (alt9) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:740:2: rule__ReferencedRepo__Group_4__0
                    {
                    pushFollow(FOLLOW_rule__ReferencedRepo__Group_4__0_in_rule__ReferencedRepo__Group__4__Impl1476);
                    rule__ReferencedRepo__Group_4__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getReferencedRepoAccess().getGroup_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group__4__Impl"


    // $ANTLR start "rule__ReferencedRepo__Group_4__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:760:1: rule__ReferencedRepo__Group_4__0 : rule__ReferencedRepo__Group_4__0__Impl rule__ReferencedRepo__Group_4__1 ;
    public final void rule__ReferencedRepo__Group_4__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:764:1: ( rule__ReferencedRepo__Group_4__0__Impl rule__ReferencedRepo__Group_4__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:765:2: rule__ReferencedRepo__Group_4__0__Impl rule__ReferencedRepo__Group_4__1
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__Group_4__0__Impl_in_rule__ReferencedRepo__Group_4__01517);
            rule__ReferencedRepo__Group_4__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__ReferencedRepo__Group_4__1_in_rule__ReferencedRepo__Group_4__01520);
            rule__ReferencedRepo__Group_4__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group_4__0"


    // $ANTLR start "rule__ReferencedRepo__Group_4__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:772:1: rule__ReferencedRepo__Group_4__0__Impl : ( ( rule__ReferencedRepo__EnablementAssignment_4_0 ) ) ;
    public final void rule__ReferencedRepo__Group_4__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:776:1: ( ( ( rule__ReferencedRepo__EnablementAssignment_4_0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:777:1: ( ( rule__ReferencedRepo__EnablementAssignment_4_0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:777:1: ( ( rule__ReferencedRepo__EnablementAssignment_4_0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:778:1: ( rule__ReferencedRepo__EnablementAssignment_4_0 )
            {
             before(grammarAccess.getReferencedRepoAccess().getEnablementAssignment_4_0()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:779:1: ( rule__ReferencedRepo__EnablementAssignment_4_0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:779:2: rule__ReferencedRepo__EnablementAssignment_4_0
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__EnablementAssignment_4_0_in_rule__ReferencedRepo__Group_4__0__Impl1547);
            rule__ReferencedRepo__EnablementAssignment_4_0();

            state._fsp--;


            }

             after(grammarAccess.getReferencedRepoAccess().getEnablementAssignment_4_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group_4__0__Impl"


    // $ANTLR start "rule__ReferencedRepo__Group_4__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:789:1: rule__ReferencedRepo__Group_4__1 : rule__ReferencedRepo__Group_4__1__Impl ;
    public final void rule__ReferencedRepo__Group_4__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:793:1: ( rule__ReferencedRepo__Group_4__1__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:794:2: rule__ReferencedRepo__Group_4__1__Impl
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__Group_4__1__Impl_in_rule__ReferencedRepo__Group_4__11577);
            rule__ReferencedRepo__Group_4__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group_4__1"


    // $ANTLR start "rule__ReferencedRepo__Group_4__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:800:1: rule__ReferencedRepo__Group_4__1__Impl : ( ( rule__ReferencedRepo__VisibilityAssignment_4_1 ) ) ;
    public final void rule__ReferencedRepo__Group_4__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:804:1: ( ( ( rule__ReferencedRepo__VisibilityAssignment_4_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:805:1: ( ( rule__ReferencedRepo__VisibilityAssignment_4_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:805:1: ( ( rule__ReferencedRepo__VisibilityAssignment_4_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:806:1: ( rule__ReferencedRepo__VisibilityAssignment_4_1 )
            {
             before(grammarAccess.getReferencedRepoAccess().getVisibilityAssignment_4_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:807:1: ( rule__ReferencedRepo__VisibilityAssignment_4_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:807:2: rule__ReferencedRepo__VisibilityAssignment_4_1
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__VisibilityAssignment_4_1_in_rule__ReferencedRepo__Group_4__1__Impl1604);
            rule__ReferencedRepo__VisibilityAssignment_4_1();

            state._fsp--;


            }

             after(grammarAccess.getReferencedRepoAccess().getVisibilityAssignment_4_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__Group_4__1__Impl"


    // $ANTLR start "rule__UncategorizedContent__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:821:1: rule__UncategorizedContent__Group__0 : rule__UncategorizedContent__Group__0__Impl rule__UncategorizedContent__Group__1 ;
    public final void rule__UncategorizedContent__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:825:1: ( rule__UncategorizedContent__Group__0__Impl rule__UncategorizedContent__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:826:2: rule__UncategorizedContent__Group__0__Impl rule__UncategorizedContent__Group__1
            {
            pushFollow(FOLLOW_rule__UncategorizedContent__Group__0__Impl_in_rule__UncategorizedContent__Group__01638);
            rule__UncategorizedContent__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UncategorizedContent__Group__1_in_rule__UncategorizedContent__Group__01641);
            rule__UncategorizedContent__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UncategorizedContent__Group__0"


    // $ANTLR start "rule__UncategorizedContent__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:833:1: rule__UncategorizedContent__Group__0__Impl : ( 'uncategorizedContent:' ) ;
    public final void rule__UncategorizedContent__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:837:1: ( ( 'uncategorizedContent:' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:838:1: ( 'uncategorizedContent:' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:838:1: ( 'uncategorizedContent:' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:839:1: 'uncategorizedContent:'
            {
             before(grammarAccess.getUncategorizedContentAccess().getUncategorizedContentKeyword_0()); 
            match(input,18,FOLLOW_18_in_rule__UncategorizedContent__Group__0__Impl1669); 
             after(grammarAccess.getUncategorizedContentAccess().getUncategorizedContentKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UncategorizedContent__Group__0__Impl"


    // $ANTLR start "rule__UncategorizedContent__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:852:1: rule__UncategorizedContent__Group__1 : rule__UncategorizedContent__Group__1__Impl rule__UncategorizedContent__Group__2 ;
    public final void rule__UncategorizedContent__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:856:1: ( rule__UncategorizedContent__Group__1__Impl rule__UncategorizedContent__Group__2 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:857:2: rule__UncategorizedContent__Group__1__Impl rule__UncategorizedContent__Group__2
            {
            pushFollow(FOLLOW_rule__UncategorizedContent__Group__1__Impl_in_rule__UncategorizedContent__Group__11700);
            rule__UncategorizedContent__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__UncategorizedContent__Group__2_in_rule__UncategorizedContent__Group__11703);
            rule__UncategorizedContent__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UncategorizedContent__Group__1"


    // $ANTLR start "rule__UncategorizedContent__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:864:1: rule__UncategorizedContent__Group__1__Impl : ( ( ( rule__UncategorizedContent__EntriesAssignment_1 ) ) ( ( rule__UncategorizedContent__EntriesAssignment_1 )* ) ) ;
    public final void rule__UncategorizedContent__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:868:1: ( ( ( ( rule__UncategorizedContent__EntriesAssignment_1 ) ) ( ( rule__UncategorizedContent__EntriesAssignment_1 )* ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:869:1: ( ( ( rule__UncategorizedContent__EntriesAssignment_1 ) ) ( ( rule__UncategorizedContent__EntriesAssignment_1 )* ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:869:1: ( ( ( rule__UncategorizedContent__EntriesAssignment_1 ) ) ( ( rule__UncategorizedContent__EntriesAssignment_1 )* ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:870:1: ( ( rule__UncategorizedContent__EntriesAssignment_1 ) ) ( ( rule__UncategorizedContent__EntriesAssignment_1 )* )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:870:1: ( ( rule__UncategorizedContent__EntriesAssignment_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:871:1: ( rule__UncategorizedContent__EntriesAssignment_1 )
            {
             before(grammarAccess.getUncategorizedContentAccess().getEntriesAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:872:1: ( rule__UncategorizedContent__EntriesAssignment_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:872:2: rule__UncategorizedContent__EntriesAssignment_1
            {
            pushFollow(FOLLOW_rule__UncategorizedContent__EntriesAssignment_1_in_rule__UncategorizedContent__Group__1__Impl1732);
            rule__UncategorizedContent__EntriesAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUncategorizedContentAccess().getEntriesAssignment_1()); 

            }

            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:875:1: ( ( rule__UncategorizedContent__EntriesAssignment_1 )* )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:876:1: ( rule__UncategorizedContent__EntriesAssignment_1 )*
            {
             before(grammarAccess.getUncategorizedContentAccess().getEntriesAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:877:1: ( rule__UncategorizedContent__EntriesAssignment_1 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==20||LA10_0==24) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:877:2: rule__UncategorizedContent__EntriesAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__UncategorizedContent__EntriesAssignment_1_in_rule__UncategorizedContent__Group__1__Impl1744);
            	    rule__UncategorizedContent__EntriesAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getUncategorizedContentAccess().getEntriesAssignment_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UncategorizedContent__Group__1__Impl"


    // $ANTLR start "rule__UncategorizedContent__Group__2"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:888:1: rule__UncategorizedContent__Group__2 : rule__UncategorizedContent__Group__2__Impl ;
    public final void rule__UncategorizedContent__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:892:1: ( rule__UncategorizedContent__Group__2__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:893:2: rule__UncategorizedContent__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__UncategorizedContent__Group__2__Impl_in_rule__UncategorizedContent__Group__21777);
            rule__UncategorizedContent__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UncategorizedContent__Group__2"


    // $ANTLR start "rule__UncategorizedContent__Group__2__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:899:1: rule__UncategorizedContent__Group__2__Impl : ( 'end' ) ;
    public final void rule__UncategorizedContent__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:903:1: ( ( 'end' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:904:1: ( 'end' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:904:1: ( 'end' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:905:1: 'end'
            {
             before(grammarAccess.getUncategorizedContentAccess().getEndKeyword_2()); 
            match(input,19,FOLLOW_19_in_rule__UncategorizedContent__Group__2__Impl1805); 
             after(grammarAccess.getUncategorizedContentAccess().getEndKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UncategorizedContent__Group__2__Impl"


    // $ANTLR start "rule__CategoryDef__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:924:1: rule__CategoryDef__Group__0 : rule__CategoryDef__Group__0__Impl rule__CategoryDef__Group__1 ;
    public final void rule__CategoryDef__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:928:1: ( rule__CategoryDef__Group__0__Impl rule__CategoryDef__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:929:2: rule__CategoryDef__Group__0__Impl rule__CategoryDef__Group__1
            {
            pushFollow(FOLLOW_rule__CategoryDef__Group__0__Impl_in_rule__CategoryDef__Group__01842);
            rule__CategoryDef__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CategoryDef__Group__1_in_rule__CategoryDef__Group__01845);
            rule__CategoryDef__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryDef__Group__0"


    // $ANTLR start "rule__CategoryDef__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:936:1: rule__CategoryDef__Group__0__Impl : ( ruleCategoryName ) ;
    public final void rule__CategoryDef__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:940:1: ( ( ruleCategoryName ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:941:1: ( ruleCategoryName )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:941:1: ( ruleCategoryName )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:942:1: ruleCategoryName
            {
             before(grammarAccess.getCategoryDefAccess().getCategoryNameParserRuleCall_0()); 
            pushFollow(FOLLOW_ruleCategoryName_in_rule__CategoryDef__Group__0__Impl1872);
            ruleCategoryName();

            state._fsp--;

             after(grammarAccess.getCategoryDefAccess().getCategoryNameParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryDef__Group__0__Impl"


    // $ANTLR start "rule__CategoryDef__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:953:1: rule__CategoryDef__Group__1 : rule__CategoryDef__Group__1__Impl rule__CategoryDef__Group__2 ;
    public final void rule__CategoryDef__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:957:1: ( rule__CategoryDef__Group__1__Impl rule__CategoryDef__Group__2 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:958:2: rule__CategoryDef__Group__1__Impl rule__CategoryDef__Group__2
            {
            pushFollow(FOLLOW_rule__CategoryDef__Group__1__Impl_in_rule__CategoryDef__Group__11901);
            rule__CategoryDef__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CategoryDef__Group__2_in_rule__CategoryDef__Group__11904);
            rule__CategoryDef__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryDef__Group__1"


    // $ANTLR start "rule__CategoryDef__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:965:1: rule__CategoryDef__Group__1__Impl : ( ( rule__CategoryDef__EntriesAssignment_1 )* ) ;
    public final void rule__CategoryDef__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:969:1: ( ( ( rule__CategoryDef__EntriesAssignment_1 )* ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:970:1: ( ( rule__CategoryDef__EntriesAssignment_1 )* )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:970:1: ( ( rule__CategoryDef__EntriesAssignment_1 )* )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:971:1: ( rule__CategoryDef__EntriesAssignment_1 )*
            {
             before(grammarAccess.getCategoryDefAccess().getEntriesAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:972:1: ( rule__CategoryDef__EntriesAssignment_1 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==20||(LA11_0>=24 && LA11_0<=25)) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:972:2: rule__CategoryDef__EntriesAssignment_1
            	    {
            	    pushFollow(FOLLOW_rule__CategoryDef__EntriesAssignment_1_in_rule__CategoryDef__Group__1__Impl1931);
            	    rule__CategoryDef__EntriesAssignment_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getCategoryDefAccess().getEntriesAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryDef__Group__1__Impl"


    // $ANTLR start "rule__CategoryDef__Group__2"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:982:1: rule__CategoryDef__Group__2 : rule__CategoryDef__Group__2__Impl ;
    public final void rule__CategoryDef__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:986:1: ( rule__CategoryDef__Group__2__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:987:2: rule__CategoryDef__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__CategoryDef__Group__2__Impl_in_rule__CategoryDef__Group__21962);
            rule__CategoryDef__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryDef__Group__2"


    // $ANTLR start "rule__CategoryDef__Group__2__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:993:1: rule__CategoryDef__Group__2__Impl : ( 'end' ) ;
    public final void rule__CategoryDef__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:997:1: ( ( 'end' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:998:1: ( 'end' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:998:1: ( 'end' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:999:1: 'end'
            {
             before(grammarAccess.getCategoryDefAccess().getEndKeyword_2()); 
            match(input,19,FOLLOW_19_in_rule__CategoryDef__Group__2__Impl1990); 
             after(grammarAccess.getCategoryDefAccess().getEndKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryDef__Group__2__Impl"


    // $ANTLR start "rule__Iu__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1018:1: rule__Iu__Group__0 : rule__Iu__Group__0__Impl rule__Iu__Group__1 ;
    public final void rule__Iu__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1022:1: ( rule__Iu__Group__0__Impl rule__Iu__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1023:2: rule__Iu__Group__0__Impl rule__Iu__Group__1
            {
            pushFollow(FOLLOW_rule__Iu__Group__0__Impl_in_rule__Iu__Group__02027);
            rule__Iu__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Iu__Group__1_in_rule__Iu__Group__02030);
            rule__Iu__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group__0"


    // $ANTLR start "rule__Iu__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1030:1: rule__Iu__Group__0__Impl : ( 'iu:' ) ;
    public final void rule__Iu__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1034:1: ( ( 'iu:' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1035:1: ( 'iu:' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1035:1: ( 'iu:' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1036:1: 'iu:'
            {
             before(grammarAccess.getIuAccess().getIuKeyword_0()); 
            match(input,20,FOLLOW_20_in_rule__Iu__Group__0__Impl2058); 
             after(grammarAccess.getIuAccess().getIuKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group__0__Impl"


    // $ANTLR start "rule__Iu__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1049:1: rule__Iu__Group__1 : rule__Iu__Group__1__Impl rule__Iu__Group__2 ;
    public final void rule__Iu__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1053:1: ( rule__Iu__Group__1__Impl rule__Iu__Group__2 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1054:2: rule__Iu__Group__1__Impl rule__Iu__Group__2
            {
            pushFollow(FOLLOW_rule__Iu__Group__1__Impl_in_rule__Iu__Group__12089);
            rule__Iu__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Iu__Group__2_in_rule__Iu__Group__12092);
            rule__Iu__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group__1"


    // $ANTLR start "rule__Iu__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1061:1: rule__Iu__Group__1__Impl : ( ( rule__Iu__IuIdAssignment_1 ) ) ;
    public final void rule__Iu__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1065:1: ( ( ( rule__Iu__IuIdAssignment_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1066:1: ( ( rule__Iu__IuIdAssignment_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1066:1: ( ( rule__Iu__IuIdAssignment_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1067:1: ( rule__Iu__IuIdAssignment_1 )
            {
             before(grammarAccess.getIuAccess().getIuIdAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1068:1: ( rule__Iu__IuIdAssignment_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1068:2: rule__Iu__IuIdAssignment_1
            {
            pushFollow(FOLLOW_rule__Iu__IuIdAssignment_1_in_rule__Iu__Group__1__Impl2119);
            rule__Iu__IuIdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getIuAccess().getIuIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group__1__Impl"


    // $ANTLR start "rule__Iu__Group__2"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1078:1: rule__Iu__Group__2 : rule__Iu__Group__2__Impl ;
    public final void rule__Iu__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1082:1: ( rule__Iu__Group__2__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1083:2: rule__Iu__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Iu__Group__2__Impl_in_rule__Iu__Group__22149);
            rule__Iu__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group__2"


    // $ANTLR start "rule__Iu__Group__2__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1089:1: rule__Iu__Group__2__Impl : ( ( rule__Iu__Group_2__0 )? ) ;
    public final void rule__Iu__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1093:1: ( ( ( rule__Iu__Group_2__0 )? ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1094:1: ( ( rule__Iu__Group_2__0 )? )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1094:1: ( ( rule__Iu__Group_2__0 )? )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1095:1: ( rule__Iu__Group_2__0 )?
            {
             before(grammarAccess.getIuAccess().getGroup_2()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1096:1: ( rule__Iu__Group_2__0 )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==21) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1096:2: rule__Iu__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__Iu__Group_2__0_in_rule__Iu__Group__2__Impl2176);
                    rule__Iu__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getIuAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group__2__Impl"


    // $ANTLR start "rule__Iu__Group_2__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1112:1: rule__Iu__Group_2__0 : rule__Iu__Group_2__0__Impl rule__Iu__Group_2__1 ;
    public final void rule__Iu__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1116:1: ( rule__Iu__Group_2__0__Impl rule__Iu__Group_2__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1117:2: rule__Iu__Group_2__0__Impl rule__Iu__Group_2__1
            {
            pushFollow(FOLLOW_rule__Iu__Group_2__0__Impl_in_rule__Iu__Group_2__02213);
            rule__Iu__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Iu__Group_2__1_in_rule__Iu__Group_2__02216);
            rule__Iu__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group_2__0"


    // $ANTLR start "rule__Iu__Group_2__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1124:1: rule__Iu__Group_2__0__Impl : ( ',' ) ;
    public final void rule__Iu__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1128:1: ( ( ',' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1129:1: ( ',' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1129:1: ( ',' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1130:1: ','
            {
             before(grammarAccess.getIuAccess().getCommaKeyword_2_0()); 
            match(input,21,FOLLOW_21_in_rule__Iu__Group_2__0__Impl2244); 
             after(grammarAccess.getIuAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group_2__0__Impl"


    // $ANTLR start "rule__Iu__Group_2__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1143:1: rule__Iu__Group_2__1 : rule__Iu__Group_2__1__Impl ;
    public final void rule__Iu__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1147:1: ( rule__Iu__Group_2__1__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1148:2: rule__Iu__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__Iu__Group_2__1__Impl_in_rule__Iu__Group_2__12275);
            rule__Iu__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group_2__1"


    // $ANTLR start "rule__Iu__Group_2__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1154:1: rule__Iu__Group_2__1__Impl : ( ( rule__Iu__RangeAssignment_2_1 ) ) ;
    public final void rule__Iu__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1158:1: ( ( ( rule__Iu__RangeAssignment_2_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1159:1: ( ( rule__Iu__RangeAssignment_2_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1159:1: ( ( rule__Iu__RangeAssignment_2_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1160:1: ( rule__Iu__RangeAssignment_2_1 )
            {
             before(grammarAccess.getIuAccess().getRangeAssignment_2_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1161:1: ( rule__Iu__RangeAssignment_2_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1161:2: rule__Iu__RangeAssignment_2_1
            {
            pushFollow(FOLLOW_rule__Iu__RangeAssignment_2_1_in_rule__Iu__Group_2__1__Impl2302);
            rule__Iu__RangeAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getIuAccess().getRangeAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__Group_2__1__Impl"


    // $ANTLR start "rule__VersionRange__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1175:1: rule__VersionRange__Group__0 : rule__VersionRange__Group__0__Impl rule__VersionRange__Group__1 ;
    public final void rule__VersionRange__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1179:1: ( rule__VersionRange__Group__0__Impl rule__VersionRange__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1180:2: rule__VersionRange__Group__0__Impl rule__VersionRange__Group__1
            {
            pushFollow(FOLLOW_rule__VersionRange__Group__0__Impl_in_rule__VersionRange__Group__02336);
            rule__VersionRange__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VersionRange__Group__1_in_rule__VersionRange__Group__02339);
            rule__VersionRange__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group__0"


    // $ANTLR start "rule__VersionRange__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1187:1: rule__VersionRange__Group__0__Impl : ( '[' ) ;
    public final void rule__VersionRange__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1191:1: ( ( '[' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1192:1: ( '[' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1192:1: ( '[' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1193:1: '['
            {
             before(grammarAccess.getVersionRangeAccess().getLeftSquareBracketKeyword_0()); 
            match(input,22,FOLLOW_22_in_rule__VersionRange__Group__0__Impl2367); 
             after(grammarAccess.getVersionRangeAccess().getLeftSquareBracketKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group__0__Impl"


    // $ANTLR start "rule__VersionRange__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1206:1: rule__VersionRange__Group__1 : rule__VersionRange__Group__1__Impl rule__VersionRange__Group__2 ;
    public final void rule__VersionRange__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1210:1: ( rule__VersionRange__Group__1__Impl rule__VersionRange__Group__2 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1211:2: rule__VersionRange__Group__1__Impl rule__VersionRange__Group__2
            {
            pushFollow(FOLLOW_rule__VersionRange__Group__1__Impl_in_rule__VersionRange__Group__12398);
            rule__VersionRange__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VersionRange__Group__2_in_rule__VersionRange__Group__12401);
            rule__VersionRange__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group__1"


    // $ANTLR start "rule__VersionRange__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1218:1: rule__VersionRange__Group__1__Impl : ( ( rule__VersionRange__LowerRangeAssignment_1 ) ) ;
    public final void rule__VersionRange__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1222:1: ( ( ( rule__VersionRange__LowerRangeAssignment_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1223:1: ( ( rule__VersionRange__LowerRangeAssignment_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1223:1: ( ( rule__VersionRange__LowerRangeAssignment_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1224:1: ( rule__VersionRange__LowerRangeAssignment_1 )
            {
             before(grammarAccess.getVersionRangeAccess().getLowerRangeAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1225:1: ( rule__VersionRange__LowerRangeAssignment_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1225:2: rule__VersionRange__LowerRangeAssignment_1
            {
            pushFollow(FOLLOW_rule__VersionRange__LowerRangeAssignment_1_in_rule__VersionRange__Group__1__Impl2428);
            rule__VersionRange__LowerRangeAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getVersionRangeAccess().getLowerRangeAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group__1__Impl"


    // $ANTLR start "rule__VersionRange__Group__2"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1235:1: rule__VersionRange__Group__2 : rule__VersionRange__Group__2__Impl rule__VersionRange__Group__3 ;
    public final void rule__VersionRange__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1239:1: ( rule__VersionRange__Group__2__Impl rule__VersionRange__Group__3 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1240:2: rule__VersionRange__Group__2__Impl rule__VersionRange__Group__3
            {
            pushFollow(FOLLOW_rule__VersionRange__Group__2__Impl_in_rule__VersionRange__Group__22458);
            rule__VersionRange__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VersionRange__Group__3_in_rule__VersionRange__Group__22461);
            rule__VersionRange__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group__2"


    // $ANTLR start "rule__VersionRange__Group__2__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1247:1: rule__VersionRange__Group__2__Impl : ( ( rule__VersionRange__Group_2__0 )? ) ;
    public final void rule__VersionRange__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1251:1: ( ( ( rule__VersionRange__Group_2__0 )? ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1252:1: ( ( rule__VersionRange__Group_2__0 )? )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1252:1: ( ( rule__VersionRange__Group_2__0 )? )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1253:1: ( rule__VersionRange__Group_2__0 )?
            {
             before(grammarAccess.getVersionRangeAccess().getGroup_2()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1254:1: ( rule__VersionRange__Group_2__0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==21) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1254:2: rule__VersionRange__Group_2__0
                    {
                    pushFollow(FOLLOW_rule__VersionRange__Group_2__0_in_rule__VersionRange__Group__2__Impl2488);
                    rule__VersionRange__Group_2__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getVersionRangeAccess().getGroup_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group__2__Impl"


    // $ANTLR start "rule__VersionRange__Group__3"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1264:1: rule__VersionRange__Group__3 : rule__VersionRange__Group__3__Impl ;
    public final void rule__VersionRange__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1268:1: ( rule__VersionRange__Group__3__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1269:2: rule__VersionRange__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__VersionRange__Group__3__Impl_in_rule__VersionRange__Group__32519);
            rule__VersionRange__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group__3"


    // $ANTLR start "rule__VersionRange__Group__3__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1275:1: rule__VersionRange__Group__3__Impl : ( ']' ) ;
    public final void rule__VersionRange__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1279:1: ( ( ']' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1280:1: ( ']' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1280:1: ( ']' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1281:1: ']'
            {
             before(grammarAccess.getVersionRangeAccess().getRightSquareBracketKeyword_3()); 
            match(input,23,FOLLOW_23_in_rule__VersionRange__Group__3__Impl2547); 
             after(grammarAccess.getVersionRangeAccess().getRightSquareBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group__3__Impl"


    // $ANTLR start "rule__VersionRange__Group_2__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1302:1: rule__VersionRange__Group_2__0 : rule__VersionRange__Group_2__0__Impl rule__VersionRange__Group_2__1 ;
    public final void rule__VersionRange__Group_2__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1306:1: ( rule__VersionRange__Group_2__0__Impl rule__VersionRange__Group_2__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1307:2: rule__VersionRange__Group_2__0__Impl rule__VersionRange__Group_2__1
            {
            pushFollow(FOLLOW_rule__VersionRange__Group_2__0__Impl_in_rule__VersionRange__Group_2__02586);
            rule__VersionRange__Group_2__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__VersionRange__Group_2__1_in_rule__VersionRange__Group_2__02589);
            rule__VersionRange__Group_2__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group_2__0"


    // $ANTLR start "rule__VersionRange__Group_2__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1314:1: rule__VersionRange__Group_2__0__Impl : ( ',' ) ;
    public final void rule__VersionRange__Group_2__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1318:1: ( ( ',' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1319:1: ( ',' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1319:1: ( ',' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1320:1: ','
            {
             before(grammarAccess.getVersionRangeAccess().getCommaKeyword_2_0()); 
            match(input,21,FOLLOW_21_in_rule__VersionRange__Group_2__0__Impl2617); 
             after(grammarAccess.getVersionRangeAccess().getCommaKeyword_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group_2__0__Impl"


    // $ANTLR start "rule__VersionRange__Group_2__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1333:1: rule__VersionRange__Group_2__1 : rule__VersionRange__Group_2__1__Impl ;
    public final void rule__VersionRange__Group_2__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1337:1: ( rule__VersionRange__Group_2__1__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1338:2: rule__VersionRange__Group_2__1__Impl
            {
            pushFollow(FOLLOW_rule__VersionRange__Group_2__1__Impl_in_rule__VersionRange__Group_2__12648);
            rule__VersionRange__Group_2__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group_2__1"


    // $ANTLR start "rule__VersionRange__Group_2__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1344:1: rule__VersionRange__Group_2__1__Impl : ( ( rule__VersionRange__UpperRangeAssignment_2_1 ) ) ;
    public final void rule__VersionRange__Group_2__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1348:1: ( ( ( rule__VersionRange__UpperRangeAssignment_2_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1349:1: ( ( rule__VersionRange__UpperRangeAssignment_2_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1349:1: ( ( rule__VersionRange__UpperRangeAssignment_2_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1350:1: ( rule__VersionRange__UpperRangeAssignment_2_1 )
            {
             before(grammarAccess.getVersionRangeAccess().getUpperRangeAssignment_2_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1351:1: ( rule__VersionRange__UpperRangeAssignment_2_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1351:2: rule__VersionRange__UpperRangeAssignment_2_1
            {
            pushFollow(FOLLOW_rule__VersionRange__UpperRangeAssignment_2_1_in_rule__VersionRange__Group_2__1__Impl2675);
            rule__VersionRange__UpperRangeAssignment_2_1();

            state._fsp--;


            }

             after(grammarAccess.getVersionRangeAccess().getUpperRangeAssignment_2_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__Group_2__1__Impl"


    // $ANTLR start "rule__Feature__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1365:1: rule__Feature__Group__0 : rule__Feature__Group__0__Impl rule__Feature__Group__1 ;
    public final void rule__Feature__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1369:1: ( rule__Feature__Group__0__Impl rule__Feature__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1370:2: rule__Feature__Group__0__Impl rule__Feature__Group__1
            {
            pushFollow(FOLLOW_rule__Feature__Group__0__Impl_in_rule__Feature__Group__02709);
            rule__Feature__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Feature__Group__1_in_rule__Feature__Group__02712);
            rule__Feature__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0"


    // $ANTLR start "rule__Feature__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1377:1: rule__Feature__Group__0__Impl : ( 'feature:' ) ;
    public final void rule__Feature__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1381:1: ( ( 'feature:' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1382:1: ( 'feature:' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1382:1: ( 'feature:' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1383:1: 'feature:'
            {
             before(grammarAccess.getFeatureAccess().getFeatureKeyword_0()); 
            match(input,24,FOLLOW_24_in_rule__Feature__Group__0__Impl2740); 
             after(grammarAccess.getFeatureAccess().getFeatureKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0__Impl"


    // $ANTLR start "rule__Feature__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1396:1: rule__Feature__Group__1 : rule__Feature__Group__1__Impl ;
    public final void rule__Feature__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1400:1: ( rule__Feature__Group__1__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1401:2: rule__Feature__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Feature__Group__1__Impl_in_rule__Feature__Group__12771);
            rule__Feature__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1"


    // $ANTLR start "rule__Feature__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1407:1: rule__Feature__Group__1__Impl : ( ( rule__Feature__FeatureIdAssignment_1 ) ) ;
    public final void rule__Feature__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1411:1: ( ( ( rule__Feature__FeatureIdAssignment_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1412:1: ( ( rule__Feature__FeatureIdAssignment_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1412:1: ( ( rule__Feature__FeatureIdAssignment_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1413:1: ( rule__Feature__FeatureIdAssignment_1 )
            {
             before(grammarAccess.getFeatureAccess().getFeatureIdAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1414:1: ( rule__Feature__FeatureIdAssignment_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1414:2: rule__Feature__FeatureIdAssignment_1
            {
            pushFollow(FOLLOW_rule__Feature__FeatureIdAssignment_1_in_rule__Feature__Group__1__Impl2798);
            rule__Feature__FeatureIdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getFeatureIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1__Impl"


    // $ANTLR start "rule__CategoryName__Group__0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1428:1: rule__CategoryName__Group__0 : rule__CategoryName__Group__0__Impl rule__CategoryName__Group__1 ;
    public final void rule__CategoryName__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1432:1: ( rule__CategoryName__Group__0__Impl rule__CategoryName__Group__1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1433:2: rule__CategoryName__Group__0__Impl rule__CategoryName__Group__1
            {
            pushFollow(FOLLOW_rule__CategoryName__Group__0__Impl_in_rule__CategoryName__Group__02832);
            rule__CategoryName__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CategoryName__Group__1_in_rule__CategoryName__Group__02835);
            rule__CategoryName__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryName__Group__0"


    // $ANTLR start "rule__CategoryName__Group__0__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1440:1: rule__CategoryName__Group__0__Impl : ( 'category:' ) ;
    public final void rule__CategoryName__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1444:1: ( ( 'category:' ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1445:1: ( 'category:' )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1445:1: ( 'category:' )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1446:1: 'category:'
            {
             before(grammarAccess.getCategoryNameAccess().getCategoryKeyword_0()); 
            match(input,25,FOLLOW_25_in_rule__CategoryName__Group__0__Impl2863); 
             after(grammarAccess.getCategoryNameAccess().getCategoryKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryName__Group__0__Impl"


    // $ANTLR start "rule__CategoryName__Group__1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1459:1: rule__CategoryName__Group__1 : rule__CategoryName__Group__1__Impl ;
    public final void rule__CategoryName__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1463:1: ( rule__CategoryName__Group__1__Impl )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1464:2: rule__CategoryName__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__CategoryName__Group__1__Impl_in_rule__CategoryName__Group__12894);
            rule__CategoryName__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryName__Group__1"


    // $ANTLR start "rule__CategoryName__Group__1__Impl"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1470:1: rule__CategoryName__Group__1__Impl : ( ( rule__CategoryName__CategoryNameAssignment_1 ) ) ;
    public final void rule__CategoryName__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1474:1: ( ( ( rule__CategoryName__CategoryNameAssignment_1 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1475:1: ( ( rule__CategoryName__CategoryNameAssignment_1 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1475:1: ( ( rule__CategoryName__CategoryNameAssignment_1 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1476:1: ( rule__CategoryName__CategoryNameAssignment_1 )
            {
             before(grammarAccess.getCategoryNameAccess().getCategoryNameAssignment_1()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1477:1: ( rule__CategoryName__CategoryNameAssignment_1 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1477:2: rule__CategoryName__CategoryNameAssignment_1
            {
            pushFollow(FOLLOW_rule__CategoryName__CategoryNameAssignment_1_in_rule__CategoryName__Group__1__Impl2921);
            rule__CategoryName__CategoryNameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCategoryNameAccess().getCategoryNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryName__Group__1__Impl"


    // $ANTLR start "rule__Model__RepoNameAssignment_0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1492:1: rule__Model__RepoNameAssignment_0 : ( ruleRepoName ) ;
    public final void rule__Model__RepoNameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1496:1: ( ( ruleRepoName ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1497:1: ( ruleRepoName )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1497:1: ( ruleRepoName )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1498:1: ruleRepoName
            {
             before(grammarAccess.getModelAccess().getRepoNameRepoNameParserRuleCall_0_0()); 
            pushFollow(FOLLOW_ruleRepoName_in_rule__Model__RepoNameAssignment_02960);
            ruleRepoName();

            state._fsp--;

             after(grammarAccess.getModelAccess().getRepoNameRepoNameParserRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__RepoNameAssignment_0"


    // $ANTLR start "rule__Model__AssociatedReposAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1507:1: rule__Model__AssociatedReposAssignment_1 : ( ruleReferencedRepo ) ;
    public final void rule__Model__AssociatedReposAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1511:1: ( ( ruleReferencedRepo ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1512:1: ( ruleReferencedRepo )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1512:1: ( ruleReferencedRepo )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1513:1: ruleReferencedRepo
            {
             before(grammarAccess.getModelAccess().getAssociatedReposReferencedRepoParserRuleCall_1_0()); 
            pushFollow(FOLLOW_ruleReferencedRepo_in_rule__Model__AssociatedReposAssignment_12991);
            ruleReferencedRepo();

            state._fsp--;

             after(grammarAccess.getModelAccess().getAssociatedReposReferencedRepoParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__AssociatedReposAssignment_1"


    // $ANTLR start "rule__Model__CategoriesAssignment_2"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1522:1: rule__Model__CategoriesAssignment_2 : ( ruleCategoryDef ) ;
    public final void rule__Model__CategoriesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1526:1: ( ( ruleCategoryDef ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1527:1: ( ruleCategoryDef )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1527:1: ( ruleCategoryDef )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1528:1: ruleCategoryDef
            {
             before(grammarAccess.getModelAccess().getCategoriesCategoryDefParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleCategoryDef_in_rule__Model__CategoriesAssignment_23022);
            ruleCategoryDef();

            state._fsp--;

             after(grammarAccess.getModelAccess().getCategoriesCategoryDefParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__CategoriesAssignment_2"


    // $ANTLR start "rule__Model__NonCategorizedAssignment_3"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1537:1: rule__Model__NonCategorizedAssignment_3 : ( ruleUncategorizedContent ) ;
    public final void rule__Model__NonCategorizedAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1541:1: ( ( ruleUncategorizedContent ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1542:1: ( ruleUncategorizedContent )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1542:1: ( ruleUncategorizedContent )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1543:1: ruleUncategorizedContent
            {
             before(grammarAccess.getModelAccess().getNonCategorizedUncategorizedContentParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleUncategorizedContent_in_rule__Model__NonCategorizedAssignment_33053);
            ruleUncategorizedContent();

            state._fsp--;

             after(grammarAccess.getModelAccess().getNonCategorizedUncategorizedContentParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Model__NonCategorizedAssignment_3"


    // $ANTLR start "rule__RepoName__UserReadableNameAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1552:1: rule__RepoName__UserReadableNameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__RepoName__UserReadableNameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1556:1: ( ( RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1557:1: ( RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1557:1: ( RULE_STRING )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1558:1: RULE_STRING
            {
             before(grammarAccess.getRepoNameAccess().getUserReadableNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__RepoName__UserReadableNameAssignment_13084); 
             after(grammarAccess.getRepoNameAccess().getUserReadableNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RepoName__UserReadableNameAssignment_1"


    // $ANTLR start "rule__ReferencedRepo__AssociatedRepoURLAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1567:1: rule__ReferencedRepo__AssociatedRepoURLAssignment_1 : ( RULE_STRING ) ;
    public final void rule__ReferencedRepo__AssociatedRepoURLAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1571:1: ( ( RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1572:1: ( RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1572:1: ( RULE_STRING )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1573:1: RULE_STRING
            {
             before(grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__ReferencedRepo__AssociatedRepoURLAssignment_13115); 
             after(grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__AssociatedRepoURLAssignment_1"


    // $ANTLR start "rule__ReferencedRepo__NickNameAssignment_3"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1582:1: rule__ReferencedRepo__NickNameAssignment_3 : ( RULE_STRING ) ;
    public final void rule__ReferencedRepo__NickNameAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1586:1: ( ( RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1587:1: ( RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1587:1: ( RULE_STRING )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1588:1: RULE_STRING
            {
             before(grammarAccess.getReferencedRepoAccess().getNickNameSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__ReferencedRepo__NickNameAssignment_33146); 
             after(grammarAccess.getReferencedRepoAccess().getNickNameSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__NickNameAssignment_3"


    // $ANTLR start "rule__ReferencedRepo__EnablementAssignment_4_0"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1597:1: rule__ReferencedRepo__EnablementAssignment_4_0 : ( ( rule__ReferencedRepo__EnablementAlternatives_4_0_0 ) ) ;
    public final void rule__ReferencedRepo__EnablementAssignment_4_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1601:1: ( ( ( rule__ReferencedRepo__EnablementAlternatives_4_0_0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1602:1: ( ( rule__ReferencedRepo__EnablementAlternatives_4_0_0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1602:1: ( ( rule__ReferencedRepo__EnablementAlternatives_4_0_0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1603:1: ( rule__ReferencedRepo__EnablementAlternatives_4_0_0 )
            {
             before(grammarAccess.getReferencedRepoAccess().getEnablementAlternatives_4_0_0()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1604:1: ( rule__ReferencedRepo__EnablementAlternatives_4_0_0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1604:2: rule__ReferencedRepo__EnablementAlternatives_4_0_0
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__EnablementAlternatives_4_0_0_in_rule__ReferencedRepo__EnablementAssignment_4_03177);
            rule__ReferencedRepo__EnablementAlternatives_4_0_0();

            state._fsp--;


            }

             after(grammarAccess.getReferencedRepoAccess().getEnablementAlternatives_4_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__EnablementAssignment_4_0"


    // $ANTLR start "rule__ReferencedRepo__VisibilityAssignment_4_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1613:1: rule__ReferencedRepo__VisibilityAssignment_4_1 : ( ( rule__ReferencedRepo__VisibilityAlternatives_4_1_0 ) ) ;
    public final void rule__ReferencedRepo__VisibilityAssignment_4_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1617:1: ( ( ( rule__ReferencedRepo__VisibilityAlternatives_4_1_0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1618:1: ( ( rule__ReferencedRepo__VisibilityAlternatives_4_1_0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1618:1: ( ( rule__ReferencedRepo__VisibilityAlternatives_4_1_0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1619:1: ( rule__ReferencedRepo__VisibilityAlternatives_4_1_0 )
            {
             before(grammarAccess.getReferencedRepoAccess().getVisibilityAlternatives_4_1_0()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1620:1: ( rule__ReferencedRepo__VisibilityAlternatives_4_1_0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1620:2: rule__ReferencedRepo__VisibilityAlternatives_4_1_0
            {
            pushFollow(FOLLOW_rule__ReferencedRepo__VisibilityAlternatives_4_1_0_in_rule__ReferencedRepo__VisibilityAssignment_4_13210);
            rule__ReferencedRepo__VisibilityAlternatives_4_1_0();

            state._fsp--;


            }

             after(grammarAccess.getReferencedRepoAccess().getVisibilityAlternatives_4_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__ReferencedRepo__VisibilityAssignment_4_1"


    // $ANTLR start "rule__UncategorizedContent__EntriesAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1629:1: rule__UncategorizedContent__EntriesAssignment_1 : ( ( rule__UncategorizedContent__EntriesAlternatives_1_0 ) ) ;
    public final void rule__UncategorizedContent__EntriesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1633:1: ( ( ( rule__UncategorizedContent__EntriesAlternatives_1_0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1634:1: ( ( rule__UncategorizedContent__EntriesAlternatives_1_0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1634:1: ( ( rule__UncategorizedContent__EntriesAlternatives_1_0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1635:1: ( rule__UncategorizedContent__EntriesAlternatives_1_0 )
            {
             before(grammarAccess.getUncategorizedContentAccess().getEntriesAlternatives_1_0()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1636:1: ( rule__UncategorizedContent__EntriesAlternatives_1_0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1636:2: rule__UncategorizedContent__EntriesAlternatives_1_0
            {
            pushFollow(FOLLOW_rule__UncategorizedContent__EntriesAlternatives_1_0_in_rule__UncategorizedContent__EntriesAssignment_13243);
            rule__UncategorizedContent__EntriesAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getUncategorizedContentAccess().getEntriesAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__UncategorizedContent__EntriesAssignment_1"


    // $ANTLR start "rule__CategoryDef__EntriesAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1645:1: rule__CategoryDef__EntriesAssignment_1 : ( ( rule__CategoryDef__EntriesAlternatives_1_0 ) ) ;
    public final void rule__CategoryDef__EntriesAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1649:1: ( ( ( rule__CategoryDef__EntriesAlternatives_1_0 ) ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1650:1: ( ( rule__CategoryDef__EntriesAlternatives_1_0 ) )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1650:1: ( ( rule__CategoryDef__EntriesAlternatives_1_0 ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1651:1: ( rule__CategoryDef__EntriesAlternatives_1_0 )
            {
             before(grammarAccess.getCategoryDefAccess().getEntriesAlternatives_1_0()); 
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1652:1: ( rule__CategoryDef__EntriesAlternatives_1_0 )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1652:2: rule__CategoryDef__EntriesAlternatives_1_0
            {
            pushFollow(FOLLOW_rule__CategoryDef__EntriesAlternatives_1_0_in_rule__CategoryDef__EntriesAssignment_13276);
            rule__CategoryDef__EntriesAlternatives_1_0();

            state._fsp--;


            }

             after(grammarAccess.getCategoryDefAccess().getEntriesAlternatives_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryDef__EntriesAssignment_1"


    // $ANTLR start "rule__Iu__IuIdAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1661:1: rule__Iu__IuIdAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Iu__IuIdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1665:1: ( ( RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1666:1: ( RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1666:1: ( RULE_STRING )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1667:1: RULE_STRING
            {
             before(grammarAccess.getIuAccess().getIuIdSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Iu__IuIdAssignment_13309); 
             after(grammarAccess.getIuAccess().getIuIdSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__IuIdAssignment_1"


    // $ANTLR start "rule__Iu__RangeAssignment_2_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1676:1: rule__Iu__RangeAssignment_2_1 : ( ruleVersionRange ) ;
    public final void rule__Iu__RangeAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1680:1: ( ( ruleVersionRange ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1681:1: ( ruleVersionRange )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1681:1: ( ruleVersionRange )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1682:1: ruleVersionRange
            {
             before(grammarAccess.getIuAccess().getRangeVersionRangeParserRuleCall_2_1_0()); 
            pushFollow(FOLLOW_ruleVersionRange_in_rule__Iu__RangeAssignment_2_13340);
            ruleVersionRange();

            state._fsp--;

             after(grammarAccess.getIuAccess().getRangeVersionRangeParserRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Iu__RangeAssignment_2_1"


    // $ANTLR start "rule__VersionRange__LowerRangeAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1691:1: rule__VersionRange__LowerRangeAssignment_1 : ( RULE_STRING ) ;
    public final void rule__VersionRange__LowerRangeAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1695:1: ( ( RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1696:1: ( RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1696:1: ( RULE_STRING )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1697:1: RULE_STRING
            {
             before(grammarAccess.getVersionRangeAccess().getLowerRangeSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__VersionRange__LowerRangeAssignment_13371); 
             after(grammarAccess.getVersionRangeAccess().getLowerRangeSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__LowerRangeAssignment_1"


    // $ANTLR start "rule__VersionRange__UpperRangeAssignment_2_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1706:1: rule__VersionRange__UpperRangeAssignment_2_1 : ( RULE_STRING ) ;
    public final void rule__VersionRange__UpperRangeAssignment_2_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1710:1: ( ( RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1711:1: ( RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1711:1: ( RULE_STRING )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1712:1: RULE_STRING
            {
             before(grammarAccess.getVersionRangeAccess().getUpperRangeSTRINGTerminalRuleCall_2_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__VersionRange__UpperRangeAssignment_2_13402); 
             after(grammarAccess.getVersionRangeAccess().getUpperRangeSTRINGTerminalRuleCall_2_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__VersionRange__UpperRangeAssignment_2_1"


    // $ANTLR start "rule__Feature__FeatureIdAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1721:1: rule__Feature__FeatureIdAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Feature__FeatureIdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1725:1: ( ( RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1726:1: ( RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1726:1: ( RULE_STRING )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1727:1: RULE_STRING
            {
             before(grammarAccess.getFeatureAccess().getFeatureIdSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Feature__FeatureIdAssignment_13433); 
             after(grammarAccess.getFeatureAccess().getFeatureIdSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__FeatureIdAssignment_1"


    // $ANTLR start "rule__CategoryName__CategoryNameAssignment_1"
    // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1736:1: rule__CategoryName__CategoryNameAssignment_1 : ( RULE_STRING ) ;
    public final void rule__CategoryName__CategoryNameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1740:1: ( ( RULE_STRING ) )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1741:1: ( RULE_STRING )
            {
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1741:1: ( RULE_STRING )
            // ../com.rapicorp.p2.repodsl.ui/src-gen/com/rapicorp/p2/ui/contentassist/antlr/internal/InternalRepoDSL.g:1742:1: RULE_STRING
            {
             before(grammarAccess.getCategoryNameAccess().getCategoryNameSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__CategoryName__CategoryNameAssignment_13464); 
             after(grammarAccess.getCategoryNameAccess().getCategoryNameSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CategoryName__CategoryNameAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleModel_in_entryRuleModel61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleModel68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__Group__0_in_ruleModel94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepoName_in_entryRuleRepoName121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRepoName128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepoName__Group__0_in_ruleRepoName154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferencedRepo_in_entryRuleReferencedRepo181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleReferencedRepo188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__0_in_ruleReferencedRepo214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUncategorizedContent_in_entryRuleUncategorizedContent241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUncategorizedContent248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__Group__0_in_ruleUncategorizedContent274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCategoryDef_in_entryRuleCategoryDef301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCategoryDef308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryDef__Group__0_in_ruleCategoryDef334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIu_in_entryRuleIu361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleIu368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__Group__0_in_ruleIu394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVersionRange_in_entryRuleVersionRange421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleVersionRange428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__Group__0_in_ruleVersionRange454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeature_in_entryRuleFeature481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFeature488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__Group__0_in_ruleFeature514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCategoryName_in_entryRuleCategoryName541 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCategoryName548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryName__Group__0_in_ruleCategoryName574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__ReferencedRepo__EnablementAlternatives_4_0_0611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__ReferencedRepo__EnablementAlternatives_4_0_0631 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__ReferencedRepo__VisibilityAlternatives_4_1_0666 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__ReferencedRepo__VisibilityAlternatives_4_1_0686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeature_in_rule__UncategorizedContent__EntriesAlternatives_1_0720 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIu_in_rule__UncategorizedContent__EntriesAlternatives_1_0737 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleFeature_in_rule__CategoryDef__EntriesAlternatives_1_0769 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleIu_in_rule__CategoryDef__EntriesAlternatives_1_0786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCategoryDef_in_rule__CategoryDef__EntriesAlternatives_1_0803 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__Group__0__Impl_in_rule__Model__Group__0833 = new BitSet(new long[]{0x0000000002050000L});
    public static final BitSet FOLLOW_rule__Model__Group__1_in_rule__Model__Group__0836 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__RepoNameAssignment_0_in_rule__Model__Group__0__Impl863 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__Group__1__Impl_in_rule__Model__Group__1894 = new BitSet(new long[]{0x0000000002050000L});
    public static final BitSet FOLLOW_rule__Model__Group__2_in_rule__Model__Group__1897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__AssociatedReposAssignment_1_in_rule__Model__Group__1__Impl924 = new BitSet(new long[]{0x0000000000010002L});
    public static final BitSet FOLLOW_rule__Model__Group__2__Impl_in_rule__Model__Group__2955 = new BitSet(new long[]{0x0000000002050000L});
    public static final BitSet FOLLOW_rule__Model__Group__3_in_rule__Model__Group__2958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__CategoriesAssignment_2_in_rule__Model__Group__2__Impl985 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_rule__Model__Group__3__Impl_in_rule__Model__Group__31016 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Model__NonCategorizedAssignment_3_in_rule__Model__Group__3__Impl1043 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepoName__Group__0__Impl_in_rule__RepoName__Group__01082 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RepoName__Group__1_in_rule__RepoName__Group__01085 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__RepoName__Group__0__Impl1113 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepoName__Group__1__Impl_in_rule__RepoName__Group__11144 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RepoName__UserReadableNameAssignment_1_in_rule__RepoName__Group__1__Impl1171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__0__Impl_in_rule__ReferencedRepo__Group__01205 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__1_in_rule__ReferencedRepo__Group__01208 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__ReferencedRepo__Group__0__Impl1236 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__1__Impl_in_rule__ReferencedRepo__Group__11267 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__2_in_rule__ReferencedRepo__Group__11270 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__AssociatedRepoURLAssignment_1_in_rule__ReferencedRepo__Group__1__Impl1297 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__2__Impl_in_rule__ReferencedRepo__Group__21327 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__3_in_rule__ReferencedRepo__Group__21330 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__ReferencedRepo__Group__2__Impl1358 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__3__Impl_in_rule__ReferencedRepo__Group__31389 = new BitSet(new long[]{0x0000000000001800L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__4_in_rule__ReferencedRepo__Group__31392 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__NickNameAssignment_3_in_rule__ReferencedRepo__Group__3__Impl1419 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group__4__Impl_in_rule__ReferencedRepo__Group__41449 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group_4__0_in_rule__ReferencedRepo__Group__4__Impl1476 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group_4__0__Impl_in_rule__ReferencedRepo__Group_4__01517 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group_4__1_in_rule__ReferencedRepo__Group_4__01520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__EnablementAssignment_4_0_in_rule__ReferencedRepo__Group_4__0__Impl1547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__Group_4__1__Impl_in_rule__ReferencedRepo__Group_4__11577 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__VisibilityAssignment_4_1_in_rule__ReferencedRepo__Group_4__1__Impl1604 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__Group__0__Impl_in_rule__UncategorizedContent__Group__01638 = new BitSet(new long[]{0x0000000001100000L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__Group__1_in_rule__UncategorizedContent__Group__01641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__UncategorizedContent__Group__0__Impl1669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__Group__1__Impl_in_rule__UncategorizedContent__Group__11700 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__Group__2_in_rule__UncategorizedContent__Group__11703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__EntriesAssignment_1_in_rule__UncategorizedContent__Group__1__Impl1732 = new BitSet(new long[]{0x0000000001100002L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__EntriesAssignment_1_in_rule__UncategorizedContent__Group__1__Impl1744 = new BitSet(new long[]{0x0000000001100002L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__Group__2__Impl_in_rule__UncategorizedContent__Group__21777 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__UncategorizedContent__Group__2__Impl1805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryDef__Group__0__Impl_in_rule__CategoryDef__Group__01842 = new BitSet(new long[]{0x0000000003180000L});
    public static final BitSet FOLLOW_rule__CategoryDef__Group__1_in_rule__CategoryDef__Group__01845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCategoryName_in_rule__CategoryDef__Group__0__Impl1872 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryDef__Group__1__Impl_in_rule__CategoryDef__Group__11901 = new BitSet(new long[]{0x0000000003180000L});
    public static final BitSet FOLLOW_rule__CategoryDef__Group__2_in_rule__CategoryDef__Group__11904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryDef__EntriesAssignment_1_in_rule__CategoryDef__Group__1__Impl1931 = new BitSet(new long[]{0x0000000003100002L});
    public static final BitSet FOLLOW_rule__CategoryDef__Group__2__Impl_in_rule__CategoryDef__Group__21962 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__CategoryDef__Group__2__Impl1990 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__Group__0__Impl_in_rule__Iu__Group__02027 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Iu__Group__1_in_rule__Iu__Group__02030 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__Iu__Group__0__Impl2058 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__Group__1__Impl_in_rule__Iu__Group__12089 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_rule__Iu__Group__2_in_rule__Iu__Group__12092 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__IuIdAssignment_1_in_rule__Iu__Group__1__Impl2119 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__Group__2__Impl_in_rule__Iu__Group__22149 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__Group_2__0_in_rule__Iu__Group__2__Impl2176 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__Group_2__0__Impl_in_rule__Iu__Group_2__02213 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__Iu__Group_2__1_in_rule__Iu__Group_2__02216 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__Iu__Group_2__0__Impl2244 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__Group_2__1__Impl_in_rule__Iu__Group_2__12275 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Iu__RangeAssignment_2_1_in_rule__Iu__Group_2__1__Impl2302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__Group__0__Impl_in_rule__VersionRange__Group__02336 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__VersionRange__Group__1_in_rule__VersionRange__Group__02339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__VersionRange__Group__0__Impl2367 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__Group__1__Impl_in_rule__VersionRange__Group__12398 = new BitSet(new long[]{0x0000000000A00000L});
    public static final BitSet FOLLOW_rule__VersionRange__Group__2_in_rule__VersionRange__Group__12401 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__LowerRangeAssignment_1_in_rule__VersionRange__Group__1__Impl2428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__Group__2__Impl_in_rule__VersionRange__Group__22458 = new BitSet(new long[]{0x0000000000A00000L});
    public static final BitSet FOLLOW_rule__VersionRange__Group__3_in_rule__VersionRange__Group__22461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__Group_2__0_in_rule__VersionRange__Group__2__Impl2488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__Group__3__Impl_in_rule__VersionRange__Group__32519 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__VersionRange__Group__3__Impl2547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__Group_2__0__Impl_in_rule__VersionRange__Group_2__02586 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__VersionRange__Group_2__1_in_rule__VersionRange__Group_2__02589 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__VersionRange__Group_2__0__Impl2617 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__Group_2__1__Impl_in_rule__VersionRange__Group_2__12648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__VersionRange__UpperRangeAssignment_2_1_in_rule__VersionRange__Group_2__1__Impl2675 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__Group__0__Impl_in_rule__Feature__Group__02709 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Feature__Group__1_in_rule__Feature__Group__02712 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_24_in_rule__Feature__Group__0__Impl2740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__Group__1__Impl_in_rule__Feature__Group__12771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Feature__FeatureIdAssignment_1_in_rule__Feature__Group__1__Impl2798 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryName__Group__0__Impl_in_rule__CategoryName__Group__02832 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__CategoryName__Group__1_in_rule__CategoryName__Group__02835 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_25_in_rule__CategoryName__Group__0__Impl2863 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryName__Group__1__Impl_in_rule__CategoryName__Group__12894 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryName__CategoryNameAssignment_1_in_rule__CategoryName__Group__1__Impl2921 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRepoName_in_rule__Model__RepoNameAssignment_02960 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleReferencedRepo_in_rule__Model__AssociatedReposAssignment_12991 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCategoryDef_in_rule__Model__CategoriesAssignment_23022 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUncategorizedContent_in_rule__Model__NonCategorizedAssignment_33053 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__RepoName__UserReadableNameAssignment_13084 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__ReferencedRepo__AssociatedRepoURLAssignment_13115 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__ReferencedRepo__NickNameAssignment_33146 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__EnablementAlternatives_4_0_0_in_rule__ReferencedRepo__EnablementAssignment_4_03177 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__ReferencedRepo__VisibilityAlternatives_4_1_0_in_rule__ReferencedRepo__VisibilityAssignment_4_13210 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__UncategorizedContent__EntriesAlternatives_1_0_in_rule__UncategorizedContent__EntriesAssignment_13243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CategoryDef__EntriesAlternatives_1_0_in_rule__CategoryDef__EntriesAssignment_13276 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Iu__IuIdAssignment_13309 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleVersionRange_in_rule__Iu__RangeAssignment_2_13340 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__VersionRange__LowerRangeAssignment_13371 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__VersionRange__UpperRangeAssignment_2_13402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Feature__FeatureIdAssignment_13433 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__CategoryName__CategoryNameAssignment_13464 = new BitSet(new long[]{0x0000000000000002L});

}