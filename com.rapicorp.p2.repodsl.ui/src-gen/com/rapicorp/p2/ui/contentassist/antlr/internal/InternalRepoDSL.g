/*
* generated by Xtext
*/
grammar InternalRepoDSL;

options {
	superClass=AbstractInternalContentAssistParser;
	
}

@lexer::header {
package com.rapicorp.p2.ui.contentassist.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.Lexer;
}

@parser::header {
package com.rapicorp.p2.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import com.rapicorp.p2.services.RepoDSLGrammarAccess;

}

@parser::members {
 
 	private RepoDSLGrammarAccess grammarAccess;
 	
    public void setGrammarAccess(RepoDSLGrammarAccess grammarAccess) {
    	this.grammarAccess = grammarAccess;
    }
    
    @Override
    protected Grammar getGrammar() {
    	return grammarAccess.getGrammar();
    }
    
    @Override
    protected String getValueForTokenName(String tokenName) {
    	return tokenName;
    }

}




// Entry rule entryRuleModel
entryRuleModel 
:
{ before(grammarAccess.getModelRule()); }
	 ruleModel
{ after(grammarAccess.getModelRule()); } 
	 EOF 
;

// Rule Model
ruleModel
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getModelAccess().getGroup()); }
(rule__Model__Group__0)
{ after(grammarAccess.getModelAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleRepoName
entryRuleRepoName 
:
{ before(grammarAccess.getRepoNameRule()); }
	 ruleRepoName
{ after(grammarAccess.getRepoNameRule()); } 
	 EOF 
;

// Rule RepoName
ruleRepoName
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getRepoNameAccess().getGroup()); }
(rule__RepoName__Group__0)
{ after(grammarAccess.getRepoNameAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleReferencedRepo
entryRuleReferencedRepo 
:
{ before(grammarAccess.getReferencedRepoRule()); }
	 ruleReferencedRepo
{ after(grammarAccess.getReferencedRepoRule()); } 
	 EOF 
;

// Rule ReferencedRepo
ruleReferencedRepo
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getReferencedRepoAccess().getGroup()); }
(rule__ReferencedRepo__Group__0)
{ after(grammarAccess.getReferencedRepoAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleUncategorizedContent
entryRuleUncategorizedContent 
:
{ before(grammarAccess.getUncategorizedContentRule()); }
	 ruleUncategorizedContent
{ after(grammarAccess.getUncategorizedContentRule()); } 
	 EOF 
;

// Rule UncategorizedContent
ruleUncategorizedContent
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getUncategorizedContentAccess().getGroup()); }
(rule__UncategorizedContent__Group__0)
{ after(grammarAccess.getUncategorizedContentAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleCategoryDef
entryRuleCategoryDef 
:
{ before(grammarAccess.getCategoryDefRule()); }
	 ruleCategoryDef
{ after(grammarAccess.getCategoryDefRule()); } 
	 EOF 
;

// Rule CategoryDef
ruleCategoryDef
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getCategoryDefAccess().getGroup()); }
(rule__CategoryDef__Group__0)
{ after(grammarAccess.getCategoryDefAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleIu
entryRuleIu 
:
{ before(grammarAccess.getIuRule()); }
	 ruleIu
{ after(grammarAccess.getIuRule()); } 
	 EOF 
;

// Rule Iu
ruleIu
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getIuAccess().getGroup()); }
(rule__Iu__Group__0)
{ after(grammarAccess.getIuAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleVersionRange
entryRuleVersionRange 
:
{ before(grammarAccess.getVersionRangeRule()); }
	 ruleVersionRange
{ after(grammarAccess.getVersionRangeRule()); } 
	 EOF 
;

// Rule VersionRange
ruleVersionRange
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getVersionRangeAccess().getGroup()); }
(rule__VersionRange__Group__0)
{ after(grammarAccess.getVersionRangeAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleFeature
entryRuleFeature 
:
{ before(grammarAccess.getFeatureRule()); }
	 ruleFeature
{ after(grammarAccess.getFeatureRule()); } 
	 EOF 
;

// Rule Feature
ruleFeature
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getFeatureAccess().getGroup()); }
(rule__Feature__Group__0)
{ after(grammarAccess.getFeatureAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}



// Entry rule entryRuleCategoryName
entryRuleCategoryName 
:
{ before(grammarAccess.getCategoryNameRule()); }
	 ruleCategoryName
{ after(grammarAccess.getCategoryNameRule()); } 
	 EOF 
;

// Rule CategoryName
ruleCategoryName
    @init {
		int stackSize = keepStackSize();
    }
	:
(
{ before(grammarAccess.getCategoryNameAccess().getGroup()); }
(rule__CategoryName__Group__0)
{ after(grammarAccess.getCategoryNameAccess().getGroup()); }
)

;
finally {
	restoreStackSize(stackSize);
}




rule__ReferencedRepo__EnablementAlternatives_4_0_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getEnablementEnabledKeyword_4_0_0_0()); }

	'enabled' 

{ after(grammarAccess.getReferencedRepoAccess().getEnablementEnabledKeyword_4_0_0_0()); }
)

    |(
{ before(grammarAccess.getReferencedRepoAccess().getEnablementDisabledKeyword_4_0_0_1()); }

	'disabled' 

{ after(grammarAccess.getReferencedRepoAccess().getEnablementDisabledKeyword_4_0_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__VisibilityAlternatives_4_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getVisibilityHiddenKeyword_4_1_0_0()); }

	'hidden' 

{ after(grammarAccess.getReferencedRepoAccess().getVisibilityHiddenKeyword_4_1_0_0()); }
)

    |(
{ before(grammarAccess.getReferencedRepoAccess().getVisibilityVisibleKeyword_4_1_0_1()); }

	'visible' 

{ after(grammarAccess.getReferencedRepoAccess().getVisibilityVisibleKeyword_4_1_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__UncategorizedContent__EntriesAlternatives_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getUncategorizedContentAccess().getEntriesFeatureParserRuleCall_1_0_0()); }
	ruleFeature
{ after(grammarAccess.getUncategorizedContentAccess().getEntriesFeatureParserRuleCall_1_0_0()); }
)

    |(
{ before(grammarAccess.getUncategorizedContentAccess().getEntriesIuParserRuleCall_1_0_1()); }
	ruleIu
{ after(grammarAccess.getUncategorizedContentAccess().getEntriesIuParserRuleCall_1_0_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CategoryDef__EntriesAlternatives_1_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCategoryDefAccess().getEntriesFeatureParserRuleCall_1_0_0()); }
	ruleFeature
{ after(grammarAccess.getCategoryDefAccess().getEntriesFeatureParserRuleCall_1_0_0()); }
)

    |(
{ before(grammarAccess.getCategoryDefAccess().getEntriesIuParserRuleCall_1_0_1()); }
	ruleIu
{ after(grammarAccess.getCategoryDefAccess().getEntriesIuParserRuleCall_1_0_1()); }
)

    |(
{ before(grammarAccess.getCategoryDefAccess().getEntriesCategoryDefParserRuleCall_1_0_2()); }
	ruleCategoryDef
{ after(grammarAccess.getCategoryDefAccess().getEntriesCategoryDefParserRuleCall_1_0_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}



rule__Model__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Model__Group__0__Impl
	rule__Model__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Model__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getRepoNameAssignment_0()); }
(rule__Model__RepoNameAssignment_0)?
{ after(grammarAccess.getModelAccess().getRepoNameAssignment_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Model__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Model__Group__1__Impl
	rule__Model__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Model__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getAssociatedReposAssignment_1()); }
(rule__Model__AssociatedReposAssignment_1)*
{ after(grammarAccess.getModelAccess().getAssociatedReposAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Model__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Model__Group__2__Impl
	rule__Model__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__Model__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getCategoriesAssignment_2()); }
(rule__Model__CategoriesAssignment_2)*
{ after(grammarAccess.getModelAccess().getCategoriesAssignment_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Model__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Model__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Model__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getNonCategorizedAssignment_3()); }
(rule__Model__NonCategorizedAssignment_3)?
{ after(grammarAccess.getModelAccess().getNonCategorizedAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__RepoName__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RepoName__Group__0__Impl
	rule__RepoName__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__RepoName__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRepoNameAccess().getRepoNameKeyword_0()); }

	'repoName:' 

{ after(grammarAccess.getRepoNameAccess().getRepoNameKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__RepoName__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__RepoName__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__RepoName__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRepoNameAccess().getUserReadableNameAssignment_1()); }
(rule__RepoName__UserReadableNameAssignment_1)
{ after(grammarAccess.getRepoNameAccess().getUserReadableNameAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__ReferencedRepo__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ReferencedRepo__Group__0__Impl
	rule__ReferencedRepo__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getReferenceRepoKeyword_0()); }

	'referenceRepo:' 

{ after(grammarAccess.getReferencedRepoAccess().getReferenceRepoKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ReferencedRepo__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ReferencedRepo__Group__1__Impl
	rule__ReferencedRepo__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLAssignment_1()); }
(rule__ReferencedRepo__AssociatedRepoURLAssignment_1)
{ after(grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ReferencedRepo__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ReferencedRepo__Group__2__Impl
	rule__ReferencedRepo__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getAsKeyword_2()); }

	'as' 

{ after(grammarAccess.getReferencedRepoAccess().getAsKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ReferencedRepo__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ReferencedRepo__Group__3__Impl
	rule__ReferencedRepo__Group__4
;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getNickNameAssignment_3()); }
(rule__ReferencedRepo__NickNameAssignment_3)
{ after(grammarAccess.getReferencedRepoAccess().getNickNameAssignment_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ReferencedRepo__Group__4
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ReferencedRepo__Group__4__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__Group__4__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getGroup_4()); }
(rule__ReferencedRepo__Group_4__0)?
{ after(grammarAccess.getReferencedRepoAccess().getGroup_4()); }
)

;
finally {
	restoreStackSize(stackSize);
}












rule__ReferencedRepo__Group_4__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ReferencedRepo__Group_4__0__Impl
	rule__ReferencedRepo__Group_4__1
;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__Group_4__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getEnablementAssignment_4_0()); }
(rule__ReferencedRepo__EnablementAssignment_4_0)
{ after(grammarAccess.getReferencedRepoAccess().getEnablementAssignment_4_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__ReferencedRepo__Group_4__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__ReferencedRepo__Group_4__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__Group_4__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getVisibilityAssignment_4_1()); }
(rule__ReferencedRepo__VisibilityAssignment_4_1)
{ after(grammarAccess.getReferencedRepoAccess().getVisibilityAssignment_4_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__UncategorizedContent__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__UncategorizedContent__Group__0__Impl
	rule__UncategorizedContent__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__UncategorizedContent__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getUncategorizedContentAccess().getUncategorizedContentKeyword_0()); }

	'uncategorizedContent:' 

{ after(grammarAccess.getUncategorizedContentAccess().getUncategorizedContentKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__UncategorizedContent__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__UncategorizedContent__Group__1__Impl
	rule__UncategorizedContent__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__UncategorizedContent__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
(
{ before(grammarAccess.getUncategorizedContentAccess().getEntriesAssignment_1()); }
(rule__UncategorizedContent__EntriesAssignment_1)
{ after(grammarAccess.getUncategorizedContentAccess().getEntriesAssignment_1()); }
)
(
{ before(grammarAccess.getUncategorizedContentAccess().getEntriesAssignment_1()); }
(rule__UncategorizedContent__EntriesAssignment_1)*
{ after(grammarAccess.getUncategorizedContentAccess().getEntriesAssignment_1()); }
)
)

;
finally {
	restoreStackSize(stackSize);
}


rule__UncategorizedContent__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__UncategorizedContent__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__UncategorizedContent__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getUncategorizedContentAccess().getEndKeyword_2()); }

	'end' 

{ after(grammarAccess.getUncategorizedContentAccess().getEndKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__CategoryDef__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CategoryDef__Group__0__Impl
	rule__CategoryDef__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__CategoryDef__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCategoryDefAccess().getCategoryNameParserRuleCall_0()); }
	ruleCategoryName
{ after(grammarAccess.getCategoryDefAccess().getCategoryNameParserRuleCall_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CategoryDef__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CategoryDef__Group__1__Impl
	rule__CategoryDef__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__CategoryDef__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCategoryDefAccess().getEntriesAssignment_1()); }
(rule__CategoryDef__EntriesAssignment_1)*
{ after(grammarAccess.getCategoryDefAccess().getEntriesAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CategoryDef__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CategoryDef__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__CategoryDef__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCategoryDefAccess().getEndKeyword_2()); }

	'end' 

{ after(grammarAccess.getCategoryDefAccess().getEndKeyword_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Iu__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Iu__Group__0__Impl
	rule__Iu__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Iu__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIuAccess().getIuKeyword_0()); }

	'iu:' 

{ after(grammarAccess.getIuAccess().getIuKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Iu__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Iu__Group__1__Impl
	rule__Iu__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__Iu__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIuAccess().getIuIdAssignment_1()); }
(rule__Iu__IuIdAssignment_1)
{ after(grammarAccess.getIuAccess().getIuIdAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Iu__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Iu__Group__2__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Iu__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIuAccess().getGroup_2()); }
(rule__Iu__Group_2__0)?
{ after(grammarAccess.getIuAccess().getGroup_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}








rule__Iu__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Iu__Group_2__0__Impl
	rule__Iu__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Iu__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIuAccess().getCommaKeyword_2_0()); }

	',' 

{ after(grammarAccess.getIuAccess().getCommaKeyword_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Iu__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Iu__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Iu__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIuAccess().getRangeAssignment_2_1()); }
(rule__Iu__RangeAssignment_2_1)
{ after(grammarAccess.getIuAccess().getRangeAssignment_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__VersionRange__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VersionRange__Group__0__Impl
	rule__VersionRange__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__VersionRange__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVersionRangeAccess().getLeftSquareBracketKeyword_0()); }

	'[' 

{ after(grammarAccess.getVersionRangeAccess().getLeftSquareBracketKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VersionRange__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VersionRange__Group__1__Impl
	rule__VersionRange__Group__2
;
finally {
	restoreStackSize(stackSize);
}

rule__VersionRange__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVersionRangeAccess().getLowerRangeAssignment_1()); }
(rule__VersionRange__LowerRangeAssignment_1)
{ after(grammarAccess.getVersionRangeAccess().getLowerRangeAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VersionRange__Group__2
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VersionRange__Group__2__Impl
	rule__VersionRange__Group__3
;
finally {
	restoreStackSize(stackSize);
}

rule__VersionRange__Group__2__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVersionRangeAccess().getGroup_2()); }
(rule__VersionRange__Group_2__0)?
{ after(grammarAccess.getVersionRangeAccess().getGroup_2()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VersionRange__Group__3
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VersionRange__Group__3__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__VersionRange__Group__3__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVersionRangeAccess().getRightSquareBracketKeyword_3()); }

	']' 

{ after(grammarAccess.getVersionRangeAccess().getRightSquareBracketKeyword_3()); }
)

;
finally {
	restoreStackSize(stackSize);
}










rule__VersionRange__Group_2__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VersionRange__Group_2__0__Impl
	rule__VersionRange__Group_2__1
;
finally {
	restoreStackSize(stackSize);
}

rule__VersionRange__Group_2__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVersionRangeAccess().getCommaKeyword_2_0()); }

	',' 

{ after(grammarAccess.getVersionRangeAccess().getCommaKeyword_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__VersionRange__Group_2__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__VersionRange__Group_2__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__VersionRange__Group_2__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVersionRangeAccess().getUpperRangeAssignment_2_1()); }
(rule__VersionRange__UpperRangeAssignment_2_1)
{ after(grammarAccess.getVersionRangeAccess().getUpperRangeAssignment_2_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__Feature__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Feature__Group__0__Impl
	rule__Feature__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__Feature__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccess().getFeatureKeyword_0()); }

	'feature:' 

{ after(grammarAccess.getFeatureAccess().getFeatureKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__Feature__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__Feature__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__Feature__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccess().getFeatureIdAssignment_1()); }
(rule__Feature__FeatureIdAssignment_1)
{ after(grammarAccess.getFeatureAccess().getFeatureIdAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}






rule__CategoryName__Group__0
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CategoryName__Group__0__Impl
	rule__CategoryName__Group__1
;
finally {
	restoreStackSize(stackSize);
}

rule__CategoryName__Group__0__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCategoryNameAccess().getCategoryKeyword_0()); }

	'category:' 

{ after(grammarAccess.getCategoryNameAccess().getCategoryKeyword_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


rule__CategoryName__Group__1
    @init {
		int stackSize = keepStackSize();
    }
:
	rule__CategoryName__Group__1__Impl
;
finally {
	restoreStackSize(stackSize);
}

rule__CategoryName__Group__1__Impl
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCategoryNameAccess().getCategoryNameAssignment_1()); }
(rule__CategoryName__CategoryNameAssignment_1)
{ after(grammarAccess.getCategoryNameAccess().getCategoryNameAssignment_1()); }
)

;
finally {
	restoreStackSize(stackSize);
}







rule__Model__RepoNameAssignment_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getRepoNameRepoNameParserRuleCall_0_0()); }
	ruleRepoName{ after(grammarAccess.getModelAccess().getRepoNameRepoNameParserRuleCall_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Model__AssociatedReposAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getAssociatedReposReferencedRepoParserRuleCall_1_0()); }
	ruleReferencedRepo{ after(grammarAccess.getModelAccess().getAssociatedReposReferencedRepoParserRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Model__CategoriesAssignment_2
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getCategoriesCategoryDefParserRuleCall_2_0()); }
	ruleCategoryDef{ after(grammarAccess.getModelAccess().getCategoriesCategoryDefParserRuleCall_2_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Model__NonCategorizedAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getModelAccess().getNonCategorizedUncategorizedContentParserRuleCall_3_0()); }
	ruleUncategorizedContent{ after(grammarAccess.getModelAccess().getNonCategorizedUncategorizedContentParserRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__RepoName__UserReadableNameAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getRepoNameAccess().getUserReadableNameSTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getRepoNameAccess().getUserReadableNameSTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__AssociatedRepoURLAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLSTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getReferencedRepoAccess().getAssociatedRepoURLSTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__NickNameAssignment_3
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getNickNameSTRINGTerminalRuleCall_3_0()); }
	RULE_STRING{ after(grammarAccess.getReferencedRepoAccess().getNickNameSTRINGTerminalRuleCall_3_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__EnablementAssignment_4_0
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getEnablementAlternatives_4_0_0()); }
(rule__ReferencedRepo__EnablementAlternatives_4_0_0)
{ after(grammarAccess.getReferencedRepoAccess().getEnablementAlternatives_4_0_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__ReferencedRepo__VisibilityAssignment_4_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getReferencedRepoAccess().getVisibilityAlternatives_4_1_0()); }
(rule__ReferencedRepo__VisibilityAlternatives_4_1_0)
{ after(grammarAccess.getReferencedRepoAccess().getVisibilityAlternatives_4_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__UncategorizedContent__EntriesAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getUncategorizedContentAccess().getEntriesAlternatives_1_0()); }
(rule__UncategorizedContent__EntriesAlternatives_1_0)
{ after(grammarAccess.getUncategorizedContentAccess().getEntriesAlternatives_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CategoryDef__EntriesAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCategoryDefAccess().getEntriesAlternatives_1_0()); }
(rule__CategoryDef__EntriesAlternatives_1_0)
{ after(grammarAccess.getCategoryDefAccess().getEntriesAlternatives_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Iu__IuIdAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIuAccess().getIuIdSTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getIuAccess().getIuIdSTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Iu__RangeAssignment_2_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getIuAccess().getRangeVersionRangeParserRuleCall_2_1_0()); }
	ruleVersionRange{ after(grammarAccess.getIuAccess().getRangeVersionRangeParserRuleCall_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VersionRange__LowerRangeAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVersionRangeAccess().getLowerRangeSTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getVersionRangeAccess().getLowerRangeSTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__VersionRange__UpperRangeAssignment_2_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getVersionRangeAccess().getUpperRangeSTRINGTerminalRuleCall_2_1_0()); }
	RULE_STRING{ after(grammarAccess.getVersionRangeAccess().getUpperRangeSTRINGTerminalRuleCall_2_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__Feature__FeatureIdAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getFeatureAccess().getFeatureIdSTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getFeatureAccess().getFeatureIdSTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}

rule__CategoryName__CategoryNameAssignment_1
    @init {
		int stackSize = keepStackSize();
    }
:
(
{ before(grammarAccess.getCategoryNameAccess().getCategoryNameSTRINGTerminalRuleCall_1_0()); }
	RULE_STRING{ after(grammarAccess.getCategoryNameAccess().getCategoryNameSTRINGTerminalRuleCall_1_0()); }
)

;
finally {
	restoreStackSize(stackSize);
}


RULE_ID : '^'? ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'_'|'0'..'9')*;

RULE_INT : ('0'..'9')+;

RULE_STRING : ('"' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'"')))* '"'|'\'' ('\\' ('b'|'t'|'n'|'f'|'r'|'u'|'"'|'\''|'\\')|~(('\\'|'\'')))* '\'');

RULE_ML_COMMENT : '/*' ( options {greedy=false;} : . )*'*/';

RULE_SL_COMMENT : '//' ~(('\n'|'\r'))* ('\r'? '\n')?;

RULE_WS : (' '|'\t'|'\r'|'\n')+;

RULE_ANY_OTHER : .;


